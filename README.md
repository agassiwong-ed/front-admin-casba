# Front Admin Casba par Agassi WONG

Pour voir le travail du Front Admin Casba il est nécessaire d'installer un logiciel d'hébergement de site internet en Local sur votre appareil.

Pour cela il vous fait installer : XAMPP

Lien : [Téléchargement de XAMPP](https://www.apachefriends.org/fr/index.html)

Une fois l'installation terminée, il vous faudra **coller** le dossier **wordpress** dans le dossier htdocs de votre XAMPP.

En général il se trouve dans le chemin suivant : 

>  C:\xampp\htdocs

Une fois le dossier collé dans votre dossier htdocs il vous faudra par la suite activer le serveur local sur votre appareil.

Lancez XAMPP ou bien par l'éxecutable se trouvant dans le chemin suivant : 

> C:\xampp\xampp-control (Application)

Une fois l'application XAMPP lancée, vous lancer Apache en cliquant sur son bouton **start**.

Il vous faudra ensuite aller à l'adresse où le site est hébergé sur votre appareil :

> http://localhost/wordpress/accueil.html