'use strict'
var utils = require("./utils/express-utils");

var compression = require('compression');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var http = require('http');

// Load current configuration
var cfg = require('./config/index');


//For Documenting API
var swaggerJSDoc = require('swagger-jsdoc');
var swaggerUi = require('swagger-ui-express');
var swaggerDefinition = {
   info : {
       title : "Admin Casba API",
       version : "1.0.0",
       description : ''
   },
    host: cfg.url,
    basePath:'/'
}
var swaggerSpec= swaggerJSDoc({ swaggerDefinition:swaggerDefinition, apis:['./routes/*.js']});





// Create App Instance
var app = express();
// compress all responses if big enough
app.use(compression());
// Set options to avoid the 304 response on dynamic content.
app.disable('etag');

app.use(logger('dev'));

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true})); // false ???

app.use(bodyParser.json());

//app.use(bodyParser.urlencoded({ extended: true }));
//app.use(cookieParser());



// Add CORS Header
app.use(function(req, res, next) {
    //res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Auth-Token, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Expose-Headers", "X-Auth-Token");

    next();
});

// Configure global database access
var Promise = require('promise');
var options = {
    promiseLib: Promise // overriding the default (ES6 Promise);
};
var pgp = require('pg-promise')(options);
global.db = pgp(cfg.database); // define global database instance;


// Register Routes
var routes = require('./routes/index');
app.use('/', routes);


//For API DOcumentation
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
//app.use('/api/v1', routes);

app.get('/swagger.json', function(req,res){
    res.setHeader('Content-Type','application/json');
    res.send(swaggerSpec);
});

// catch last routes, return code=404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Resource not Found');
    err.status = 404;
    next(err);
});


//#######################################################################################################################
// error handlers

app.use(utils.safeErrorHandler);

if (app.get('env') === 'development') {
    // for Dev only!!
    process.on('uncaughtException', function (err) {
        console.log("Unexpected exception: ", err);
    });
}


//#######################################################################################################################
module.exports = app;
