"use strict";

var Errors = require("../utils/custom-errors");
var Promise = require('promise');
var jwt = require('jsonwebtoken');
var utils = require("../utils/express-utils");
var bcrypt=require('bcrypt');

var auth = {};
module.exports = auth;

var TokenKey = "1w5Hf$/5&r645gjYms!gHb?/5&r645g45gjYms!gH28ja5Hf$/5&r645gjYms!0oo!";


// =========================================================================
// GENERATION DE TOKEN POUR DECONNECTER L'UTILISATEUR AU BOUT DE 2:00 HEURES
// =========================================================================
auth.generateToken = function(identifiant) {

    var tokenOption = {
        algorithm: 'HS512',
        expiresIn : '2h',
        issuer : 'admin_Casba'
        //audience : '',
        //jwtid : '', -> unique ID of the token (for revocation purpose)
        //subject : '',
        //noTimestamp : '',
        //headers : ''
    };

    try {
        var token = jwt.sign(
            {
               login : identifiant.login
            },
            TokenKey,
            tokenOption
        );

        if (!token){Promise.reject();
        }
        return Promise.resolve(token);
    }
    catch(err) {
        Promise.reject(err);
    }
};


// =========================================================================
// REQUETE DE CONNEXION : http://localhost:3003/auth/signin
// =========================================================================
auth.signIn = function(data) {
    var userInfo = {};

    return db.oneOrNone("SELECT * FROM public.users WHERE login = '" + data.login+ "'")

        .then(function(res) {
        
            if (res == null) {
                return new Promise.reject("Login does not exist")
            }
            userInfo = res;
            //comparaison
            return new Promise(function(resolve, reject) {
                bcrypt.compare(data.password, userInfo.password, function(err, res) {
                    if(res) {
                    // Passwords match             
                        return resolve(auth.generateToken(userInfo))
                    } else {
                    // Passwords don't match
                    return reject("Login / Password mismatch")
                    } 
                });
            });

        })

        .then(function(token) {
            var toSend = {
                user: {
                    id: userInfo.id,
                },
                token: token
            };
            var sql = "update public.users " +
                "set  nbconnexion = COALESCE(nbconnexion,0)+1 WHERE id = "+userInfo.id;
            db.query(sql);
            return new Promise.resolve(toSend)
        })
        .catch(function(err) {
            console.log(err);
            throw new Errors.HttpForbidden("Login does not exist");
        })
};


// =========================================================================
// REQUETE DE DCONNEXION : http://localhost:3003/auth/signout
// =========================================================================
// auth.signOut = function(data) {

//     var userInfo = {};

//     return db.oneOrNone("SELECT * FROM public.users WHERE login = '" + data.login+ "'")

//         .then(function(res) {
        
//             if (res == null) {
//                 return new Promise.reject("Login does not exist")
//             }
//             userInfo = res;
//             //comparaison
//             return new Promise(function(resolve, reject) {
//                 bcrypt.compare(data.password, userInfo.password, function(err, res) {
//                     if(res) {
//                     // Passwords match             
//                         return resolve(auth.generateToken(userInfo))
//                     } else {
//                     // Passwords don't match
//                     return reject("Login / Password mismatch")
//                     } 
//                 });
//             });

//         })

//         .then(function(token) {
//             var toSend = {
//                 user: {
//                     id: userInfo.id,
//                 },
//                 token: token
//             };
//             var sql = "update public.users " +
//                 "set  nbconnexion = COALESCE(nbconnexion,0)+1 WHERE id = "+userInfo.id;
//             db.query(sql);
//             return new Promise.resolve(toSend)
//         })
//         .catch(function(err) {
//             console.log(err);
//             throw new Errors.HttpForbidden("Login does not exist");
//         })
// };


// =========================================================================
// FONCTION POUR VERIFIER LA VALIDITE D'UN TOKEN
// =========================================================================
auth.checkToken = function (token, tk, issuer) {

    if(tk == null) {
        tk=TokenKey;
    }
    if(issuer == null) {
        issuer= 'siterre.energies-demain.com'
    }
    //{ algorithm: ['HS512'], ignoreExpiration: false, issuer: 'Bonsai-Geek.com'}
    var verifyJWT = function (fulfill, reject) {
        jwt.verify(token, tk, {algorithm: ['HS512', 'XXX'], ignoreExpiration: false /*, issuer: issuer*/}, function(err, decoded) {
            if (err) {
                console.log(err);
                var erreur = new Errors.ClientError();
                erreur.code = '403';
                if (err.message == 'jwt expired') {
                    erreur.message = 'Expired token';
                } else {
                    erreur.message = 'Invalid token';
                }
                return reject(erreur);
            }
            fulfill(decoded);
        });
    };
    return new Promise( verifyJWT );
};

