"use strict";

var async = require('async');
var Errors = require("../utils/custom-errors");
var Promise = require('promise');
var QueryStream = require('pg-query-stream');
var spex = require('spex')(Promise);

var notebookController = {};

// Vérification string entièrement numérique
String.prototype.checkNumericString = function() { return /^\d+$/.test(this); }

module.exports = notebookController;

// **************************************************************
// ******************** GET --- NOTEBOOK/ALL ********************
// **************************************************************
notebookController.getNotebooks = function()
{
    var  users_query  = "SELECT * FROM public.carnet ORDER BY id;";

    return db.query(users_query);
};

// ***********************************************************************
// ******************** GET --- NOTEBOOK/INFORMATIONS ********************
// ***********************************************************************
notebookController.getNotebookInformations = function(notebook)
{
    ErrorManageId(notebook.id, "Notebook", "getNotebookInformations");

    var  users_query  = "SELECT * FROM  public.carnet  WHERE id = '" + notebook.id + "'";

    return db.query(users_query);
};

// *******************************************************************
// ******************** PUT --- NOTEBOOK/TRANSFER ********************
// *******************************************************************
notebookController.putNotebookTransfer = function(notebook)
{
    ErrorManageId(notebook.id_user, "Notebook", "putNotebookTransfer");
    ErrorManageId(notebook.id, "Notebook", "putNotebookTransfer");

    var users_query = "UPDATE public.carnet SET id_user = '" + notebook.id_user + "' WHERE id = " + notebook.id;  

    return db.query(users_query);
};

// ********************************************************************
// ******************** DELETE --- NOTEBOOK/DELETE ********************
// ********************************************************************
notebookController.deleteNotebookDelete = function(notebook)
{
    ErrorManageId(notebook.id, "Notebook", "deleteNotebookDelete");

    var users_query = "DELETE FROM public.carnet WHERE id=" + notebook.id;

    return db.query(users_query);
};

// ****************************************************************
// ******************** PUT --- NOTEBOOK/SHARE ********************
// ****************************************************************
notebookController.putNotebookShare = function(notebook)
{
    ErrorManageId(notebook.id_user_share, "Notebook", "putNotebookShare");

    var users_query =   "INSERT INTO public.carnet " 
                        + "(id, libelle, id_user, bati_id, date_creation) "
                        + "VALUES (default" + ", "
                        + "'" + notebook.libelle + "', "
                        + notebook.id_user_share + ", "
                        + notebook.bat_id + ", "
                        + "NOW()" + ");";

    return db.query(users_query);
};

// *******************************************************************
// *** Name : ErrorManageId
// *** Fonction : Vérification d'un ID
// *** Paramètre : String / Category / Location
// *******************************************************************
function ErrorManageId(string, category, location)
{
    if (string == null || string == 'undefined') // 422
    {
        throw new Errors.IllegalArgumentError("ERROR Method " + location + " : " + category + " Id is required.");
    }
    if (string.checkNumericString() == false) // 422
    {
        throw new Errors.IllegalArgumentError("ERROR Method " + location + " : " + category + " Id must contains only digits.");
    }
}
