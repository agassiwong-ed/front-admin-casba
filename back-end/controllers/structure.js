"use strict";

var async = require('async');
var Errors = require("../utils/custom-errors");
var Promise = require('promise');
var QueryStream = require('pg-query-stream');
var spex = require('spex')(Promise);

var structureController = {};

// Vérification string entièrement numérique
String.prototype.checkNumericString = function() { return /^\d+$/.test(this); }

module.exports = structureController;

// ***************************************************************
// ******************** GET --- STRUCTURE/ALL ********************  
// ***************************************************************
structureController.getStructures = function()
{
    var  users_query  =  "SELECT * FROM public.structure ORDER BY id;";

    return db.query(users_query );
};

// ************************************************************************
// ******************** GET --- STRUCTURE/INFORMATIONS ********************
// ************************************************************************
structureController.getStructureInformations = function(structure)
{
    ErrorManageId(structure.id, "Structure", "getStructureInformations");

    var  users_query  =  "SELECT * FROM  public.structure  WHERE id = '" + structure.id + "'";

    return db.query(users_query );
};

// ****************************************************************
// ******************** POST --- STRUCTURE/NEW ********************
// ****************************************************************
structureController.postStructureNew = function(structure)
{
    ErrorManageString(structure.nom, "Name", "Structure", "putStructureNew");
    ErrorManageString(structure.adresse, "Address", "Structure", "putStructureNew");
    ErrorManageNumeric(structure.code_pays, "Country Code", "Structure", "putStructureNew");
    structure.telephone = CleanString(structure.telephone);
    ErrorManageLength(structure.telephone, "Phone", 10, "Structure", "putStructureNew");
    structure.telephone = FormatPhone(structure.telephone);
    ErrorManageOrganismeRattachement(structure.organisme_rattachement);
    structure.siret = CleanString(structure.siret);
    ErrorManageLength(structure.siret, "Siret", 14, "Structure", "putStructureNew");
    structure.siret = FormatSiret(structure.siret);

    var users_query  =   "INSERT INTO public.structure "
                        +"(id, nom, adresse, code_pays, telephone, organisme_rattachement, date_creation, siret) "
                        +"VALUES (" + "default"
                        +", '" + structure.nom
                        +"', '" + structure.adresse
                        +"', '" + structure.code_pays
                        +"', '" + structure.telephone
                        +"', '" + structure.organisme_rattachement
                        +"', NOW() "
                        +", '" + structure.siret
                        +"'); ";

    return db.query(users_query );
};

// *********************************************************************
// ******************** DELETE --- STRUCTURE/DELETE ********************
// *********************************************************************
structureController.deleteStructureDelete = function(structure)
{
    ErrorManageId(structure.id, "Structure", "deleteStructureDelete");

    var users_query  =   "DELETE FROM public.structure  WHERE id=" + structure.id;

    return db.query(users_query )
};

// ******************************************************************
// ******************** PUT --- STRUCTURE/UPDATE ********************
// ******************************************************************
structureController.putStructureUpdate = function(structure)
{
    var users_query  =   "UPDATE public.structure SET ";
    ErrorManageString(structure.nom, "Name", "Structure", "putStructureNew");
    if (structure.nom) { users_query += " nom = '" + structure.nom +"'"; }
    ErrorManageString(structure.adresse, "Address", "Structure", "putStructureNew");
    if (structure.adresse) { users_query += ", adresse='" + structure.adresse +"'"; }
    ErrorManageNumeric(structure.code_pays, "Country Code", "Structure", "putStructureNew");
    if (structure.code_pays) { users_query += ", code_pays='" + structure.code_pays +"'"; }
    structure.telephone = CleanString(structure.telephone);
    ErrorManageLength(structure.telephone, "Phone", 10, "Structure", "putStructureNew");
    structure.telephone = FormatPhone(structure.telephone);
    if (structure.telephone) { users_query += ", telephone='" + structure.telephone +"'"; }
    ErrorManageOrganismeRattachement(structure.organisme_rattachement);
    if (structure.organisme_rattachement) { users_query += ", organisme_rattachement='" + structure.organisme_rattachement  +"'"; }
    structure.siret = CleanString(structure.siret);
    ErrorManageLength(structure.siret, "Siret", 14, "Structure", "putStructureNew");
    structure.siret = FormatSiret(structure.siret);
    if (structure.siret) { users_query += ", siret='" + structure.siret +"'"; }
    ErrorManageId(structure.id, "Structure", "putStructureUpdate");
    if (structure.id) { users_query += " WHERE id = " + structure.id; }
    return db.query(users_query );
};

// *******************************************************************
// *** Name : ErrorManageId
// *** Fonction : Vérification d'un ID
// *** Paramètre : String / Category / Location
// *******************************************************************
function ErrorManageId(string, category, location)
{
    if (string == null || string == 'undefined') // 422
    {
        throw new Errors.IllegalArgumentError("ERROR Method " + location + " : " + category + " Id is required.");
    }
    if (string.checkNumericString() == false) // 422
    {
        throw new Errors.IllegalArgumentError("ERROR Method " + location + " : " + category + " Id must contains only digits.");
    }
};

// *******************************************************************
// *** Name : ErrorManageLength
// *** Fonction : Vérification de la taille d'un champs numérique
// *** Paramètre : String / String Category / Size / Category / Location
// *******************************************************************
function ErrorManageLength(string, string_category, size, category, location)
{
    if (string.length != size)
    {
        throw new Errors.IllegalArgumentError("ERROR Method " + location + " : " + category + " " + string_category + " must contains " + size + " digits.");
    }
}

// *******************************************************************
// *** Name : FormatPhone
// *** Fonction : Ajout de point séparateur pour les numéros de téléphone
// *** Paramètre : String
// *******************************************************************
function FormatPhone(string)
{
    string = string.replace(/.{0}$/,'.$&');
    string = string.replace(/.{3}$/,'.$&');
    string = string.replace(/.{6}$/,'.$&');
    string = string.replace(/.{9}$/,'.$&');
    string = string.replace(/.{12}$/,'.$&');
    return (string);
}

// *******************************************************************
// *** Name : FormatSiret
// *** Fonction : Ajout de point séparateur pour les numéros SIRET
// *** Paramètre : String
// *******************************************************************
function FormatSiret(string)
{
    string = string.replace(/.{5}$/,' $&');
    return (string)
}

// *******************************************************************
// *** Name : CleanString
// *** Fonction : Suppression des caractères non numériques
// *** Paramètre : String
// *******************************************************************
function CleanString(string)
{
    string = string.replace(/\D/g, "");
    return (string);
}

// *******************************************************************
// *** Name : ErrorManageString
// *** Fonction : Vérification d'un champs chaîne de caractères
// *** Paramètre : String / String Category / Category / Location
// *******************************************************************
function ErrorManageString(string, string_category, category, location)
{
    if (string == null || string == 'undefined' || string == "") // 422
    {
        throw new Errors.IllegalArgumentError("ERROR Method " + location + " : " + category + " " + string_category + " is required.");
    }
}

// *******************************************************************
// *** Name : ErrorManageNumeric
// *** Fonction : Vérification d'un champs s'il est entièrement numérique
// *** Paramètre : String / String Category / Category / Location
// *******************************************************************
function ErrorManageNumeric(string, string_category, category, location)
{
    if (string.checkNumericString() == false) // 422
    {
        throw new Errors.IllegalArgumentError("ERROR Method " + location + " : " + category + " " + string_category + " must contains only digits.");
    }
}

// *******************************************************************
// *** Name : ErrorManageOrganismeRattachement
// *** Fonction : Vérification du champs organisme rattachement (Non Obligatoire)
// *** Paramètre : String
// *******************************************************************
function ErrorManageOrganismeRattachement(string)
{
    if (string && string != "" && string.checkNumericString() == false) // 422
    {
        throw new Errors.IllegalArgumentError("ERROR Method putStructureNew : Structre Organisme Rattachement Id must contains only digits.");
    }
}