"use strict";

var async = require('async');
var Errors = require("../utils/custom-errors");
var Promise = require('promise');
var QueryStream = require('pg-query-stream');
var spex = require('spex')(Promise);

var userController = {};

// Vérification string entièrement numérique
String.prototype.checkNumericString = function() { return /^\d+$/.test(this); }

module.exports = userController;

// **********************************************************
// ******************** GET --- USER/ALL ********************
// **********************************************************
userController.getUsers = function()
{
    var users_query  = "SELECT * FROM public.users ORDER BY id;";

    return db.query(users_query);
};

// *******************************************************************
// ******************** GET --- USER/INFORMATIONS ********************
// *******************************************************************
userController.getUserInformations = function(user)
{
    ErrorManageId(user.id, "User", "getUserInformations");

    var users_query  = "SELECT * FROM  public.users  WHERE id = " + user.id ;

    return db.query(users_query);
};

// ***************************************************************
// ******************** PUT --- USER/ACTIVATE ********************
// ***************************************************************
userController.putUserActivate = function(user)
{
    ErrorManageId(user.id, "putUserActivate");

    var users_query = "UPDATE public.users SET isactivated = 'true' WHERE id = " + user.id;  

    return db.query(users_query);
};

// **************************************************************
// ******************** PUT --- USER/DISABLE ********************
// **************************************************************
userController.putUserDisable = function(user)
{
    ErrorManageId(user.id, "putUserDisable");

    var users_query = "UPDATE public.users SET isactivated = 'false' WHERE id = " + user.id;  

    return db.query(users_query);
};

// *******************************************************************
// *** Name : ErrorManageId
// *** Fonction : Vérification d'un ID
// *** Paramètre : String / Category / Location
// *******************************************************************
function ErrorManageId(string, category, location)
{
    if (string == null || string == 'undefined') // 422
    {
        throw new Errors.IllegalArgumentError("ERROR Method " + location + " : " + category + " Id is required.");
    }
    if (string.checkNumericString() == false) // 422
    {
        throw new Errors.IllegalArgumentError("ERROR Method " + location + " : " + category + " Id must contains only digits.");
    }
}