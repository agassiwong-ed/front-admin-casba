"use strict";

var auth    = require('../controllers/auth');
var Errors  = require("../utils/custom-errors");
var express = require('express');
var Promise = require('promise');
var utils   = require("../utils/express-utils");

var app     = express.Router();

module.exports = app;

// **************************************************************
// ******************** POST --- AUTH/SIGNIN ********************
// **************************************************************
app.post('/signin', function(req, res, next) {
    auth.signIn(req.body)
        .then(function (results) {
            // Handle resuls lists
            res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// *******************************************************************
// ******************** POST --- AUTH/GROUP/TOKEN ********************
// *******************************************************************
app.post('/signin/:group/:token', function(req, res, next) {

    auth.signInFromProsper(req.params.group, req.params.token)
        .then(function (results) {
            // Handle resuls lists
            res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});