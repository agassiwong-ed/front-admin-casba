"use strict";

var utils = require("../utils/express-utils");
var express = require('express');
var router = express.Router();

module.exports = router;

// Load all routes
router.use("/auth", require('./auth'));
router.use("/user", require('./user'));

router.use("/structure", require('./structure'));
router.use("/notebook", require('./notebook'));

router.use("/swagger", require('./swagger'));