var utils = require("../utils/express-utils");
var Errors = require("../utils/custom-errors");

var Promise = require('promise');
var express = require('express');
var app = express.Router();
module.exports = app;

var auth = require('../controllers/auth');
var notebookCtrl = require('../controllers/notebook');

// **************************************************************
// ******************** GET --- NOTEBOOK/ALL ********************
// **************************************************************
app.get('/all', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return notebookCtrl.getNotebooks()
        })
        .then(function(rs) {
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ***********************************************************************
// ******************** GET --- NOTEBOOK/INFORMATIONS ********************
// ***********************************************************************
app.get('/informations', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return notebookCtrl.getNotebookInformations(req.query)
        })
        .then(function(rs) {
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ********************************************************************
// ******************** PUT --- NOPTEBOOK/TRANSFER ********************
// ********************************************************************
app.put('/transfer', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return notebookCtrl.putNotebookTransfer(req.body);
        })
        .then(function(rs) {
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ********************************************************************
// ******************** DELETE --- NOTEBOOK/DELETE ********************
// ********************************************************************
app.delete('/delete', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return notebookCtrl.deleteNotebookDelete(req.body);
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ****************************************************************
// ******************** PUT --- NOTEBOOK/SHARE ********************
// ****************************************************************
app.put('/share', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return notebookCtrl.putNotebookShare(req.body);
        })
        .then(function(rs){
            console.log(rs)
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});