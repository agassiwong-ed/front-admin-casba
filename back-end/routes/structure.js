var utils = require("../utils/express-utils");
var Errors = require("../utils/custom-errors");

var Promise = require('promise');
var express = require('express');
var app = express.Router();
module.exports = app;

var auth = require('../controllers/auth');
var structureCtrl = require('../controllers/structure');

// ***************************************************************
// ******************** GET --- STRUCTURE/ALL ********************
// ***************************************************************
app.get('/all', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return structureCtrl.getStructures()
        })
        .then(function(rs) {
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ************************************************************************
// ******************** GET --- STRUCTURE/INFORMATIONS ********************
// ************************************************************************
app.get('/informations', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return structureCtrl.getStructureInformations(req.query)
        })
        .then(function(rs) {
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ****************************************************************
// ******************** POST --- STRUCTURE/NEW ********************
// ****************************************************************
app.post('/new',  function(req, res, next) {
    var pld=null;
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return structureCtrl.postStructureNew(req.body);
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// *********************************************************************
// ******************** DELETE --- STRUCTURE/DELETE ********************
// *********************************************************************
app.delete('/delete', function(req, res, next) {
    var pld=null;
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return structureCtrl.deleteStructureDelete(req.body);
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ******************************************************************
// ******************** PUT --- STRUCTURE/UPDATE ********************
// ******************************************************************
app.put('/update', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            console.log(req.body)
            return structureCtrl.putStructureUpdate(req.body);
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});