var express = require('express');
var app = express.Router();

module.exports = app;

// API URL PREFIX = http://localhost:3003/

// **********************************************
// ******************** AUTH ********************
// **********************************************
/**
 * @swagger
 * /auth/signin :
 *   post:
 *      tags:
 *          - Auth
 *      summary : Sign In to the Back-End
 *      description: Request to allow you to sign in to the back-end with a valid username and password. Once connected you will recieve a token to put in every request to check the validity of your connection status.
 *      parameters:
 *          -   in: body
 *              name : Account
 *              description: Parameters for the Sign In request.
 *              schema:
 *                  type: object
 *                  required:
 *                      -   login
 *                      -   password
 *                  properties:
 *                      login:
 *                          type: string
 *                      password:
 *                          type: string
 *      responses:
 *          200:
 *              description: Login successful
 *          403:
 *              description: Login does not exist
 */

// **********************************************
// ******************** USER ********************
// **********************************************
/**
 * @swagger
 * /user/all :
 *   get:
 *      tags:
 *          - User
 *      summary : Display the list of all the users from the database.
 *      description: Request to allow you to list all the users present inside the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *      responses:
 *          200:
 *              description: Displaying Users successful.
 *          403:
 *              description: Invalid Token.
 */

/**
 * @swagger
 * /user/informations?id={id} :
 *   get:
 *      tags:
 *          - User
 *      summary : Display the informations of a specific user id from the database.
 *      description: Request to allow you to display the informations of a specific user id from the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *          -   in: path
 *              name: id
 *              type: string
 *              required: true
 *              description: ID of user that needs to be fetched
 *      responses:
 *          200:
 *              description: Displaying Informations successful.
 *          403:
 *              description: Invalid Token.
 *          422:
 *              description: Invalid Id.
 */

/**
 * @swagger
 * /user/activate :
 *   put:
 *      tags:
 *          - User
 *      summary : Activate an user from the database.
 *      description: Request to allow you to activate an user from the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *          -   in: body
 *              name: body
 *              type: string
 *              required: true
 *              description: ID of user that needs to be activated
 *              schema:
 *                  type: object
 *                  required:
 *                      -   id
 *                  properties:
 *                      id:
 *                          type: string
 *      responses:
 *          200:
 *              description: Activating user successful.
 *          403:
 *              description: Invalid Token.
 *          422:
 *              description: Invalid Id.
 */

/**
 * @swagger
 * /user/disable :
 *   put:
 *      tags:
 *          - User
 *      summary : Disable an user from the database.
 *      description: Request to allow you to disable an user from the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *          -   in: body
 *              name: body
 *              type: string
 *              required: true
 *              description: ID of user that needs to be disabled
 *              schema:
 *                  type: object
 *                  required:
 *                      -   id
 *                  properties:
 *                      id:
 *                          type: string
 *      responses:
 *          200:
 *              description: Disabling user successful.
 *          403:
 *              description: Invalid Token.
 *          422:
 *              description: Invalid Id.
 */

// ***************************************************
// ******************** STRUCTURE ********************
// ***************************************************
/**
 * @swagger
 * /structure/all :
 *   get:
 *      tags:
 *          - Structure
 *      summary : Display the list of all the structures from the database.
 *      description: Request to allow you to list all the structures present inside the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *      responses:
 *          200:
 *              description: Displaying Structures successful.
 *          403:
 *              description: Invalid Token.
 */

/**
 * @swagger
 * /structure/informations?id={id} :
 *   get:
 *      tags:
 *          - Structure
 *      summary : Display the informations of a specific structure id from the database.
 *      description: Request to allow you to display the informations of a specific structure id from the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *          -   in: path
 *              name: id
 *              type: string
 *              required: true
 *              description: ID of structure that needs to be fetched
 *      responses:
 *          200:
 *              description: Displaying Informations successful.
 *          403:
 *              description: Invalid Token.
 *          422:
 *              description: Invalid Id.
 */

/**
 * @swagger
 * /structure/new :
 *   post:
 *      tags:
 *          - Structure
 *      summary : Add a new structure to the database.
 *      description: Request to allow you to add a new structure to the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *          -   in: body
 *              name: body
 *              type: string
 *              required: true
 *              description: ID of structure that needs to be added
 *              schema:
 *                  type: object
 *                  required:
 *                      -   nom
 *                      -   adresse
 *                      -   code_pays
 *                      -   telephone
 *                      -   organisme_rattachement
 *                      -   siret
 *                  properties:
 *                      nom:
 *                          type: string
 *                      adresse:
 *                          type: string
 *                      code_pays:
 *                          type: string
 *                      telephone:
 *                          type: string
 *                      organisme_rattachement:
 *                          type: string
 *                      siret:
 *                          type: string
 *      responses:
 *          200:
 *              description: Adding Structure successful.
 *          403:
 *              description: Invalid Token.
 *          422:
 *              description: Invalid Id.
 */

/**
 * @swagger
 * /structure/update :
 *   put:
 *      tags:
 *          - Structure
 *      summary : Update a structure from the database.
 *      description: Request to allow you to update a structure from the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *          -   in: body
 *              name: body
 *              type: string
 *              required: true
 *              description: ID of structure that needs to be updated
 *              schema:
 *                  type: object
 *                  required:
 *                      -   id
 *                      -   nom
 *                      -   adresse
 *                      -   code_pays
 *                      -   telephone
 *                      -   organisme_rattachement
 *                      -   siret
 *                  properties:
 *                      id:
 *                          type: string
 *                      nom:
 *                          type: string
 *                      adresse:
 *                          type: string
 *                      code_pays:
 *                          type: string
 *                      telephone:
 *                          type: string
 *                      organisme_rattachement:
 *                          type: string
 *                      siret:
 *                          type: string
 *      responses:
 *          200:
 *              description: Updating Structure successful.
 *          403:
 *              description: Invalid Token.
 *          422:
 *              description: Invalid Id.
 */

/**
 * @swagger
 * /structure/delete :
 *   delete:
 *      tags:
 *          - Structure
 *      summary : Delete a specific structure id from the database.
 *      description: Request to allow you to delete a specific structure id from the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *          -   in: body
 *              name: body
 *              type: string
 *              required: true
 *              description: ID of structure that needs to be deleted
 *              schema:
 *                  type: object
 *                  required:
 *                      -   id
 *                  properties:
 *                      id:
 *                          type: string
 *      responses:
 *          200:
 *              description: Deleting Structure successful.
 *          403:
 *              description: Invalid Token.
 *          422:
 *              description: Invalid Id.
 */

// **************************************************
// ******************** NOTEBOOK ********************
// **************************************************
/**
 * @swagger
 * /notebook/all :
 *   get:
 *      tags:
 *          - Notebook
 *      summary : Display the list of all the notebooks from the database.
 *      description: Request to allow you to list all the notebooks present inside the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *      responses:
 *          200:
 *              description: Displaying Notebooks successful.
 *          403:
 *              description: Invalid Token.
 */

/**
 * @swagger
 * /notebook/informations?id={id} :
 *   get:
 *      tags:
 *          - Notebook
 *      summary : Display the informations of a specific notebook id from the database.
 *      description: Request to allow you to display the informations of a specific notebook id from the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *          -   in: path
 *              name: id
 *              type: string
 *              required: true
 *              description: ID of notebook that needs to be fetched
 *      responses:
 *          200:
 *              description: Displaying Informations successful.
 *          403:
 *              description: Invalid Token.
 *          422:
 *              description: Invalid Id.
 */

/**
 * @swagger
 * /notebook/transfer :
 *   put:
 *      tags:
 *          - Notebook
 *      summary : Transfer a notebook from the database.
 *      description: Request to allow you to transfer a notebook from the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *          -   in: body
 *              name: body
 *              type: string
 *              required: true
 *              description: ID of notebook that needs to be updated
 *              schema:
 *                  type: object
 *                  required:
 *                      -   id
 *                      -   id_user
 *                  properties:
 *                      id:
 *                          type: string
 *                      id_user:
 *                          type: string
 *      responses:
 *          200:
 *              description: Updating Notebook successful.
 *          403:
 *              description: Invalid Token.
 *          422:
 *              description: Invalid Id.
 */

/**
 * @swagger
 * /notebook/share :
 *   put:
 *      tags:
 *          - Notebook
 *      summary : Share a notebook from the database.
 *      description: Request to allow you to share a notebook from the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *          -   in: body
 *              name: body
 *              type: string
 *              required: true
 *              description: ID of notebook that needs to be updated
 *              schema:
 *                  type: object
 *                  required:
 *                      -   libelle
 *                      -   id_user_share
 *                      -   bat_id
 *                  properties:
 *                      libelle:
 *                          type: string
 *                      id_user_share:
 *                          type: string
 *                      bat_id:
 *                          type: string
 *      responses:
 *          200:
 *              description: Sharing Notebook successful.
 *          403:
 *              description: Invalid Token.
 *          422:
 *              description: Invalid Id.
 */

 /**
 * @swagger
 * /notebook/delete :
 *   delete:
 *      tags:
 *          - Notebook
 *      summary : Delete a specific notebook id from the database.
 *      description: Request to allow you to delete a specific notebook id from the database.
 *      parameters:
 *          -   in: header
 *              name: x-auth-token
 *              type: string
 *              required: true
 *              description: Token you recieved once you signed in
 *          -   in: body
 *              name: body
 *              type: string
 *              required: true
 *              description: ID of notebook that needs to be deleted
 *              schema:
 *                  type: object
 *                  required:
 *                      -   id
 *                  properties:
 *                      id:
 *                          type: string
 *      responses:
 *          200:
 *              description: Deleting Notebook successful.
 *          403:
 *              description: Invalid Token.
 *          422:
 *              description: Invalid Id.
 */
