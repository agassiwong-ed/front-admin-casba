

var utils = require("../utils/express-utils");
var Errors = require("../utils/custom-errors");

var Promise = require('promise');
var express = require('express');
var app = express.Router();
module.exports = app;

var auth = require('../controllers/auth');
var userCtrl = require('../controllers/user');

// **********************************************************
// ******************** GET --- USER/ALL ********************
// **********************************************************
app.get('/all', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return userCtrl.getUsers();
        })
        .then(function(rs) {
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// *******************************************************************
// ******************** GET --- USER/INFORMATIONS ********************
// http://localhost:3003/user/informations?id=1
// *******************************************************************
app.get('/informations', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return userCtrl.getUserInformations(req.query);
        })
        .then(function(rs) {
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ***************************************************************
// ******************** PUT --- USER/ACTIVATE ********************
// ***************************************************************
app.put('/activate', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return userCtrl.putUserActivate(req.body);
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// **************************************************************
// ******************** PUT --- USER/DISABLE ********************
// **************************************************************
app.put('/disable', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return userCtrl.putUserDisable(req.body);
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});