"use strict";

var Errors = require("../utils/custom-errors");
var Promise = require('promise');
var jwt = require('jsonwebtoken');
var utils = require("../utils/express-utils");


var auth = {};
module.exports = auth;

var TokenKey = "1w5Hf$/5&r645gjYms!gHb?/5&r645g45gjYms!gH28ja5Hf$/5&r645gjYms!0oo!";

// ============ Génération du token ==================================================================================== DEF
auth.generateToken = function(identifiant){

    var tokenOption = {
        algorithm: 'HS512',
        expiresIn : '2h',
        issuer : 'siterre.energies-demain.com'
        //audience : '',
        //jwtid : '', -> unique ID of the token (for revocation purpose)
        //subject : '',
        //noTimestamp : '',
        //headers : ''
    };

    try {
        var token = jwt.sign(
            {
                id: identifiant.id,
                role : identifiant.role,
                group : identifiant.groups,
                scale_type_ter : identifiant.scale_type_ter,
                scale_type_ter_int : parseInt(identifiant.scale_type_ter.slice(4)),
                scale_id_ter :  identifiant.scale_id_ter,
                granularity : identifiant.granularity,
                gran_int : parseInt(identifiant.granularity.slice(4))
            },
            TokenKey,
            tokenOption
        );

        if (!token){Promise.reject();}
        return Promise.resolve(token);
    }
    catch(err) {
        Promise.reject(err);
    }
};

// ============ Récupération des infos utilisateur ===================================================================== DEF
auth.getUserInfo = function(data){

    if (!data.login || !data.password) {
        throw new Errors.IllegalArgumentError("Login and password are required.");
    }

    return db.one("SELECT " +
        "u.id as id, " +
        "u.role as role, " +
        "CASE WHEN u.role = 1 then 'ter_99' ELSE g.scale_type_ter END  as scale_type_ter, " +
        "CASE WHEN u.role = 1 then '1' ELSE g.scale_id_ter END as scale_id_ter, " +
        "CASE WHEN u.role = 1 then 'ter_00' ELSE g.granularity END as granularity, " +
        "u.groups " +
        "FROM siterre.admin_users as u " +
        "LEFT JOIN siterre.admin_groups as g " +
        "ON u.groups = g.id " +
        "WHERE u.login=$1 AND u.password=$2", [data.login, data.password])
};

// ============ Connexion  ============================================================================================= DEF
auth.signIn = function(data){

    var userInfo = {};

    return auth.getUserInfo(data)
        .then(function(res){
            userInfo = res;
            return auth.generateToken(userInfo);
        })
        .then(function(res) {
            var toSend = {
                user: {
                    role: userInfo.role,
                    id: userInfo.id,
                    group : {id :userInfo.groups }
                },
                token: res
            };
            var sql = "update siterre.admin_users " +
                "set d_last_con = NOW(), d_nb_con = COALESCE(d_nb_con,0)+1 WHERE id = "+userInfo.id;
            db.query(sql);
            return new Promise.resolve(toSend)
        })
        .catch(function(err){
            console.log(err);
            throw new Errors.HttpForbidden("ID and password missmatch");
        })
};

// ============ Vérif token validity =================================================================================== DEF
auth.checkToken = function (token) {
    //{ algorithm: ['HS512'], ignoreExpiration: false, issuer: 'Bonsai-Geek.com'}
    var verifyJWT = function (fulfill, reject) {
        jwt.verify(token, TokenKey, {algorithm: ['HS512', 'XXX'], ignoreExpiration: false, issuer: 'siterre.energies-demain.com'}, function(err, decoded) {
            if (err){
                console.log(err);
                var erreur = new Errors.ClientError();
                erreur.code = '403';
                if (err.message == 'jwt expired') {
                    erreur.message = 'Expired token';
                } else {
                    erreur.message = 'Invalid token';
                }

                return reject(erreur);
            }
            if(decoded.scale_id_ter.indexOf("'") == -1) {
                decoded.scale_id_ter = "'" + decoded.scale_id_ter + "'";

            }
            fulfill(decoded);
        });
    };
    return new Promise( verifyJWT );
};

// ============ Vérif granularity authorization ======================================================================== DEF
auth.checkAuthGranulatity = function (payload, data, granularityIntAked){

    var check = function(resolve, reject){
        var erreur = new Errors.HttpUnauthorized();
        if (payload.role == 1 ){
            return resolve(payload)
        }
        if (!data[granularityIntAked] || data[granularityIntAked] == 'undefined' || data[granularityIntAked] === null ) {
            return reject(erreur);
        }
        try{
            /*  && parseInt(data[granularityIntAked]) <= parseInt(payload.scale_type_ter_int)*/
            if (parseInt(data[granularityIntAked]) >= parseInt(payload.gran_int)){
                return resolve(payload);
                // return reject(erreur);
            }
            else {
                return reject(erreur);
            }
        }
        catch(err){
            return reject(erreur);
        }
    };
    return new Promise(check);
};

// ============ Vérif scale authorization ============================================================================== DEF
auth.checkAuthScale = function (payload, data, scaleAskedInt,  idTerAsked){

    var check = function(resolve, reject){
        var erreur = new Errors.HttpUnauthorized();

        if (payload.role == 1 ){
            return resolve(payload)
        }


        if (!data[scaleAskedInt] || data[scaleAskedInt] == 'undefined' || data[scaleAskedInt] === null ) {
            return reject(erreur);
        }
        if (!data[idTerAsked] || data[idTerAsked] == 'undefined' || data[idTerAsked] === null ) {
            return reject(erreur);
        }
        if (!data.year || data.year == 'undefined' || data.year === null ) {
            return reject(erreur);
        }
        try{
            /*
            if (parseInt(data[scaleAskedInt]) <= parseInt(payload.scale_type_ter_int)){
*/
            if (parseInt(data[scaleAskedInt]) == parseInt(payload.scale_type_ter_int)  &&  payload.scale_id_ter == data[idTerAsked]) {
                return resolve(payload);
            }
            else {
                if (data[scaleAskedInt].length == 1) {
                    data[scaleAskedInt]="0"+data[scaleAskedInt];
                }


                var table_ter_imb = "";
                if(data[scaleAskedInt] == '01' || data[scaleAskedInt] == '02' || payload.scale_type_ter_int  == '01' || payload.scale_type_ter_int  == '02' ){
                    table_ter_imb = "ter_cr_"+data.year+"_full";
                }
                else {
                    table_ter_imb = "ter_cr_"+data.year;
                }

                if(data[idTerAsked].indexOf("'") == -1) {
                    data[idTerAsked]= "'" + data[idTerAsked] + "'";
                }


                var query = "SELECT ter_"  + data[scaleAskedInt] + "_" + data.year + " " +
                    "FROM siterre." + table_ter_imb + " " +
                    "WHERE ter_" + payload.scale_type_ter_int + "_" + data.year + " in (" + payload.scale_id_ter +") " +
                    "AND ter_"  + data[scaleAskedInt] + "_" + data.year + " in (" + data[idTerAsked] +") " +
                    "GROUP BY ter_" + data[scaleAskedInt] + "_" + data.year;
                db.query(query)
                    .then(function(res){
                        return resolve(payload)
                    })
                    .catch(function(err){
                        return reject(erreur);
                    })

            }
            /*}
            else {
                return reject(erreur);
            }*/
        }
        catch(err){
            return reject(erreur);
        }
    };
    return new Promise(check);
};

// ============ Vérif Payload, granularity, and scale authorization ==================================================== DEF
auth.checkPayloadGranAndScale = function (token, data, chekGran, checkScale, granularityIntAked, scaleAskedInt, idTerAsked){

    var decodedToken = {};

    return auth.checkToken(token)
        .then(function(res){
            decodedToken = res;
            if ( decodedToken.scale_id_ter.indexOf("'") == -1 ) {
                decodedToken.scale_id_ter = "'" + decodedToken.scale_id_ter + "'" ;
            }
            if (chekGran) {
                return auth.checkAuthGranulatity(decodedToken, data, granularityIntAked)
            }
            else {
                return new Promise.resolve(decodedToken)
            }
        })
        .then(function(){
            if (checkScale) {
                return auth.checkAuthScale(decodedToken, data, scaleAskedInt, idTerAsked)
            }
            else {
                return new Promise.resolve(decodedToken)
            }
        })
        .then(function(){
            return new Promise.resolve(decodedToken)

        })
        .catch(function(err){
            return new Promise.reject(err)
        })
};