
"use strict";
var async = require('async');
var Errors = require("../utils/custom-errors");
var Promise = require('promise');
var spex = require('spex')(Promise);
var QueryStream = require('pg-query-stream');
var dbgeo = require('dbgeo');


var ctrl = {};

module.exports = ctrl;

// ============ Get bases    ========================================================================================== DEF
ctrl.getBases = function(payload) {

    var query = '';

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }
    query = " select * from siterre.blthq_descpr_value_tbl "

    return db.query(query);

};

// ============ save base    ========================================================================================== DEF
ctrl.postBase = function(payload, data) {

    var query = '';

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }

    if(data.new == true) {
        query = " INSERT INTO siterre.blthq_descpr_value_tbl(tbl, champ_ter, crrsdc_ter, crrsdc_ter_year_isfixe, crrsdc_ter_year_geo, multi_year, champ_year, year_data)   VALUES ( ";
        query += "'" + data.tbl + "', ";
        query += "'" + data.champ_ter + "', ";
        query += "'" + data.crrsdc_ter + "', ";
        query += data.crrsdc_ter_year_isfixe + ", ";
        query += data.crrsdc_ter_year_geo + ", ";
        query +=  data.multi_year + ", ";
        query +=  "null, ";
        query +=  data.year_data + "); ";
    }
    else{
        query = " UPDATE siterre.blthq_descpr_value_tbl set ";
        query += "champ_ter='" + data.champ_ter + "', ";
        query += "crrsdc_ter='" + data.crrsdc_ter + "', ";
        query += "crrsdc_ter_year_isfixe=" +data.crrsdc_ter_year_isfixe + ", ";
        query += "crrsdc_ter_year_geo=" + data.crrsdc_ter_year_geo + ", ";
        query +=  "multi_year=" + data.multi_year + ", ";
        query +=  "champ_year=null, ";
        query +=  "year_data="+data.year_data + " "
        query += "WHERE tbl = '" + data.tbl + "'; ";
    }

    return db.query(query);

};


// ============ Get vector    ========================================================================================== DEF
ctrl.getBibliothequeIndicator = function(payload, data) {

    var query = '';

    if (payload.role != 1 ) {
        query = 'SELECT i.* FROM siterre.blthq_indicateurs  as i ' +
            'LEFT JOIN siterre.admin_users_indic as g ' +
            'ON  i.id_indicateur = g.id_indicateur ' +
            'WHERE g.id_user=' + parseInt(payload.id);

        if (data.type) {
            query = query + ' AND type="' + data.type+ '"'
        }
    }
    else {
        query = 'SELECT * FROM siterre.blthq_indicateurs';
        if (data.type) {
            query = query + ' WHERE type="' + data.type+ '"'
        }
    }

    return db.query(query)

};

// ============ Get Info About single  indicator =======================================================================  DEF
ctrl.getInfoOnIndicator= function(payload, id_indic) {

    var indicInfo = {};

    if (!id_indic  || id_indic == 'undefined' || id_indic === null ) {
        throw new Errors.IllegalArgumentError("Id Indicator is required.");
    }

    var query = '';
    if (payload.role != 1 ) {
        query = 'SELECT i.* FROM siterre.blthq_indicateurs  as i ' +
            'LEFT JOIN siterre.admin_users_indic as g ' +
            'ON  i.id_indicateur = g.id_indicateur ' +
            'WHERE g.id_user=' + parseInt(payload.id) + ' AND i.id_indicateur = $1';
    }
    else {
        query = 'SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur = $1';
    }

    return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur = $1", [parseInt(id_indic)])
        .then(function(res){
            indicInfo = res;
            indicInfo.secondary_unit_short = '';
            if (res.id_indic_for_filter_defaut !== null && res.id_indic_for_filter_defaut !== ''){
                return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur = $1", [parseInt(res.id_indic_for_filter_defaut)])
                    .then(function(res){
                        indicInfo.secondary_unit_short = res.unit_short;
                        return new Promise.resolve(indicInfo)
                    })
                    .catch(function(err){
                        return new Promise.resolve(indicInfo)
                    })
            }
            else {
                return new Promise.resolve(indicInfo)
            }
        })
        .catch(function(err){
            return new Promise.reject()
        })

};

// ============ Get indicators ordrerd by theme ========================================================================  DEF
ctrl.getArchitechtureThemeIndicator = function(payload){

    var tab_theme = [];

    return db.query("SELECT * FROM siterre.blthq_theme", [])
        .then(function(res){
            tab_theme = res;

            var query = '';

            if (payload.role != 1 ) {

                query = 'SELECT bti.id_theme, st.* ' +
                    'FROM siterre.blthq_theme_indic as bti ' +
                    'LEFT JOIN siterre.blthq_ss_theme as st ' +
                    'ON bti.id_ss_theme = st.id_ss_theme ' +
                    'LEFT JOIN siterre.admin_users_indic as g ' +
                    'ON  bti.id_indicateur = g.id_indicateur ' +
                    'WHERE g.id_user=' + parseInt(payload.id) + ' '+
                    'GROUP BY id_theme, st.id_ss_theme, st.libelle , st.ordre ' +
                    'ORDER BY bti.id_theme';
            }
            else {
                query = 'SELECT bti.id_theme, st.* FROM siterre.blthq_theme_indic as bti LEFT JOIN siterre.blthq_ss_theme as st on bti.id_ss_theme = st.id_ss_theme GROUP BY id_theme, st.id_ss_theme,st.libelle , st.ordre order by bti.id_theme';
            }

            return db.query(query, [])
        })
        .then(function(res){

            var teb_temp = {};
            for (var i = 0 ; i < tab_theme.length; i++){
                tab_theme[i].ss_theme = [];
                for (var j = 0 ; j < res.length; j++){
                    if (res[j].id_theme == tab_theme[i].id_theme){
                        teb_temp = {};
                        for (var param in res[j]){
                            if (param !== 'id_theme'){
                                teb_temp[param] = res[j][param];
                            }
                        }
                        tab_theme[i].ss_theme.push(teb_temp);
                    }
                }
            }

            var query = '';

            if (payload.role != 1 ) {
                query = 'SELECT bti.id_theme, bti.id_ss_theme, bti.ordre, bi.* ' +
                    'FROM siterre.blthq_theme_indic as bti ' +
                    'LEFT JOIN siterre.blthq_indicateurs as bi ' +
                    'ON bi.id_indicateur=bti.id_indicateur ' +
                    'LEFT JOIN siterre.admin_users_indic as g ' +
                    'ON  bti.id_indicateur = g.id_indicateur ' +
                    'WHERE g.id_user=' + parseInt(payload.id) ;
            }
            else {
                query = 'SELECT bti.id_theme, bti.id_ss_theme, bti.ordre, bi.* FROM siterre.blthq_theme_indic as bti left join siterre.blthq_indicateurs as bi on bi.id_indicateur=bti.id_indicateur';
            }

            return db.query(query, [])
        })
        .then(function(res){

            var teb_temp = {};
            for (var i = 0 ; i < tab_theme.length; i++){
                for (var j = 0 ; j < tab_theme[i].ss_theme.length; j++){
                    tab_theme[i].ss_theme[j].indicators = [];
                    for (var k = 0 ; k < res.length; k++){
                        if (res[k].id_theme == tab_theme[i].id_theme && tab_theme[i].ss_theme[j].id_ss_theme == res[k].id_ss_theme ){
                            teb_temp = {};
                            for (var param in res[k]){
                                if (param !== 'id_theme' && param !== 'id_ss_theme'){
                                    teb_temp[param] = res[k][param];
                                }
                            }
                            teb_temp.ordre = parseInt(teb_temp.ordre);
                            tab_theme[i].ss_theme[j].indicators.push(teb_temp);
                        }
                    }
                }
            }

            var tab_result = [];
            var themNotNull = false;

            if (payload.role != 1) {
                for (i = 0 ; i < tab_theme.length; i++){

                themNotNull = false;
                for (j = 0 ; j < tab_theme[i].ss_theme.length; j++){
                    if (tab_theme[i].ss_theme[j].indicators && tab_theme[i].ss_theme[j].indicators.length>0) {
                        themNotNull = true;
                        break;
                    }
                }

                if (themNotNull) {
                    teb_temp = {};
                    for (param in tab_theme[i]){
                        if (param !== 'ss_theme'){
                            teb_temp[param] = tab_theme[i][param];
                        }
                    }

                    teb_temp.ss_theme = [];

                    for (j = 0 ; j < tab_theme[i].ss_theme.length; j++){
                        if (tab_theme[i].ss_theme[j].indicators && tab_theme[i].ss_theme[j].indicators.length>0) {
                            teb_temp.ss_theme.push(tab_theme[i].ss_theme[j]);
                        }
                    }
                    tab_result.push(teb_temp);
                }
            }
            }
            else {
                tab_result = tab_theme;
            }

            return new Promise.resolve(tab_result)
        })
        .catch(function(err){
            return new Promise.reject(err)
        })

};


//=============== Save indicators legend parameters (Super Admin Only !) =============================================== DEF
ctrl.saveDefaultParameters = function(payload, data) {

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }
    if (!data.id_indicator || data.id_indicator == null || data.id_indicator == 'undefined') {
        throw new Errors.IllegalArgumentError("Id Indicator is required.");
    }
    if (!data.nbClasses || data.nbClasses == null || data.id_indicator == 'nbClasses') {
        throw new Errors.IllegalArgumentError("nbClasses is required.");
    }
    if (!data.methodeDistributionId || data.methodeDistributionId == null || data.methodeDistributionId == 'undefined') {
        throw new Errors.IllegalArgumentError("methodeDistributionId is required.");
    }

    var sql = " update siterre.blthq_indicateurs set "
        + "defaut_class_number =  " + data.nbClasses + " , "
        + "defaut_class_method = '" + data.methodeDistributionId + "' "
        + " WHERE id_indicateur = " + data.id_indicator;

    return db.query(sql)
        .then(function (rs) {
            return new Promise.resolve(rs);
        })
        .catch(function (err) {
            return new Promise.reject(err);
        });
};


//=============== Save indicators Color parameters (Super Admin Only !) =============================================== DEF
ctrl.saveColorParameters = function(payload, data) {

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }
    if (!data.id_indicator || data.id_indicator == null || data.id_indicator == 'undefined') {
        throw new Errors.IllegalArgumentError("Id Indicator is required.");
    }

    if (!data.degrade || data.degrade == null || data.degrade == 'undefined') {
        throw new Errors.IllegalArgumentError("degrade is required.");
    }

    var sql = " update siterre.blthq_indicateurs set "
        + "degrade=  '" + data.degrade + "' "
        + " WHERE id_indicateur = " + data.id_indicator;

    return db.query(sql)
        .then(function (rs) {
            return new Promise.resolve(rs);
        })
        .catch(function (err) {
            return new Promise.reject(err);
        });
};


// ============ Get vector    ========================================================================================== DEF
ctrl.getBibliothequeIndicatorReference = function(payload, data) {

    var toSend = {};
    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }

    var query = "select * from siterre.blthq_descpr_value_indic order by categorie, ordre"
    return db.query(query)
        .then(function(rs) {
            toSend = rs;
            return new Promise.resolve(toSend);
        })

};


ctrl.updateIndicator = function(payload, item) {
    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }
    var query = " SELECT column_name FROM information_schema.columns WHERE table_schema = 'siterre' AND table_name = 'blthq_indicateurs' ";

    return db.query(query)
        .then(function(column_name){
            var query = " update siterre.blthq_indicateurs SET ";
            for(var col in item) {
                for (var i = 0; i < column_name.length; i++ ) {
                    if (column_name[i].column_name == col) {

                        if(typeof item[col] == 'string') {

                            if ( item[col].indexOf("'") != -1) {
                                query = query + col + " = '" + item[col].replace("'", "''") + "', "
                            } else {
                                query = query + col + " = '" + item[col] + "', "
                            }
                        }
                        else {
                            query = query + col + " = " + item[col] + ", ";
                        }
                        break;
                    }

                }
            }
            query = query.slice(0,-2) + " WHERE id_indicateur = " + item.id_indicateur
            db.query(query);

        })
        .then(function(rs){
            var toSend = {id_indicateur : item.id_indicateur}
            return new Promise.resolve(toSend)
        });
}


ctrl.updateIndicatorTheme = function(payload, item) {
    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }
    var query = " delete from siterre.blthq_theme_indic where id_indicateur = " + item[0].id_indicateur;
    return db.query(query)
        .then(function(){
            var query = " INSERT INTO siterre.blthq_theme_indic ( id_theme, id_ss_theme, id_indicateur, ordre ) VALUES ";
            for (var line in item) {
                query = query + " (  " + item[line].id_theme + ", " + item[line].id_ss_theme + ", " + item[line].id_indicateur + ", " + item[line].ordre + "), ";
            }
            query = query.slice(0,-2) + ";";
            return db.query(query);

        })
        .then(function(rs){
            return new Promise.resolve(rs)
        });
};



ctrl.addTheme = function(payload, item) {
    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }
    var query = " select max(id_theme)+1 as id_theme, count(*) as ordre from siterre.blthq_theme";
    return db.one(query)
        .then(function(rs){
            var query = " INSERT INTO siterre.blthq_theme ( id_theme, libelle, ordre ) VALUES ";
            query = query + " (  " + rs.id_theme + ", '" + item.libelle + "', " + rs.ordre + ");";

            return db.query(query);

        })
        .then(function(rs){
            return new Promise.resolve(rs)
        });
};


ctrl.addSsTheme = function(payload, item) {
    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }
    var query = " select max(id_ss_theme)+1 as id_ss_theme from siterre.blthq_ss_theme";
    return db.one(query)
        .then(function(rs){
            item.id_ss_theme = rs.id_ss_theme;
            var query = " INSERT INTO siterre.blthq_ss_theme ( id_ss_theme, libelle, ordre ) VALUES ";
            query = query + " (  " + rs.id_ss_theme + ", '" + item.libelle + "', " + item.ordre + ") ";
            return db.query(query);
        })
        .then(function(rs){
            var toSend = {id_ss_theme : item.id_ss_theme}
            return new Promise.resolve(toSend)
        });
};



ctrl.getSsTheme = function(payload, item) {
    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }
    var query = " select id_ss_theme, libelle from siterre.blthq_ss_theme";
    return db.query(query)
        .then(function(rs){
            return new Promise.resolve(rs)
        });
};



ctrl.postIndicator = function(payload, data) {
    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }


    var query = "select max(id_indicateur)+1 as id_indicateur FROM siterre.blthq_indicateurs "
    return db.one(query)
        .then(function(rs) {
            data['id_indicateur'] = rs.id_indicateur;
            query = " SELECT column_name FROM information_schema.columns WHERE table_schema = 'siterre' AND table_name = 'blthq_indicateurs' ";
            return db.query(query)
        })
        .then(function(colname){
            var query = " insert into siterre.blthq_indicateurs ( ";
            for(var col in colname){
                query = query + colname[col].column_name + ", "
            }
            query = query.slice(0,-2) + " ) VALUES ("
            for(var col in colname) {

                if (typeof data[colname[col].column_name] == 'string') {

                    if (data[colname[col].column_name].indexOf("'") != -1) {
                        query = query + "'" + data[colname[col].column_name].replace("'", "''") + "', "
                    } else {
                        query = query + "'" + data[colname[col].column_name] + "', "
                    }
                } else {
                    query = query + data[colname[col].column_name] + ", ";
                }
            }
            query = query.slice(0,-2) + " )" ;
            return db.query(query);

        })
        .then(function(rs){

            var toSend = {id_indicateur : data['id_indicateur']};
            return new Promise.resolve(toSend)
        });
}


ctrl.deleteIndicator= function (payload, data){
    var query = '';
    var scn = null;

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }

    query = "delete from siterre.blthq_indicateurs where id_indicateur = " + data.id_indicateur
    return db.query(query)
        .then(function(rs){
            query = "delete from siterre.blthq_theme_indic where id_indicateur = " + data.id_indicateur
            return db.query(query)
        })
        .then(function(rs){
            query = "delete from siterre.admin_groups_indic where id_indicateur = " + data.id_indicateur
            return db.query(query)
        })
        .then(function(rs){
            query = "select id_scn from siterre.scn_indicateurs where id_indicateur = " + data.id_indicateur
            return db.query(query)
        })
        .then(function(rs) {
            scn = rs;
            return async.forEach(scn, function (item) {
                query = "delete from siterre.scn_indicateurs where id_scn = " + data.id_indicateur
                return db.query(query)
            })
        })
        .then(function(rs) {
            scn = rs;
            return async.forEach(scn, function (item) {
                query = "delete from siterre.scn_gestion where id_scn = " + data.id_indicateur
                return db.query(query)
            })
        })
        .then(function(rs) {
            scn = rs;
            return async.forEach(scn, function (item) {
                query = "delete from siterre.scn_filtres where id_scn = " + data.id_indicateur
                return db.query(query)
            })
        })
        .then(function(rs) {
            scn = rs;
            return async.forEach(scn, function (item) {
                query = "delete from siterre.scn_raster where id_scn = " + data.id_indicateur
                return db.query(query)
            })
        })
        .then(function(rs) {
            scn = rs;
            return async.forEach(scn, function (item) {
                query = "delete from siterre.scn_ter where id_scn = " + data.id_indicateur
                return db.query(query)
            })
        })
        .then(function(rs) {
            scn = rs;
            return async.forEach(scn, function (item) {
                query = "delete from siterre.scn_vectors where id_scn = " + data.id_indicateur
                return db.query(query)
            })
        })
        .then(function(rs){
            return new Promise.resolve(rs);
        })
        .catch(function(err){
            return new Promise.reject(err);
        })

}

// ============ Get vector    ========================================================================================== DEF
ctrl.getBibliothequeFilter = function(payload) {
    var query = '';

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }

    query = "select fi.id_archi_critdter, fi.tbl, fi.nom_archi_critdter, COALESCE(nb_indic,0) as nb_indic, COALESCE(nb_critere,0) as nb_critere " +
        "   FROM siterre.blthq_descpr_value_filter fi" +
        "   LEFT join (select id_archi_critdter, count(*) as nb_critere from siterre.blthq_indic_dyn_critere group by id_archi_critdter ) n on n.id_archi_critdter=fi.id_archi_critdter" +
        "   LEFT JOIN (select id_archi_critdter, count(*) as nb_indic from siterre.blthq_indicateurs group by id_archi_critdter ) i on i.id_archi_critdter=fi.id_archi_critdter "
    return db.query(query).then(function(rs){
        return new Promise.resolve(rs);
    })

};


ctrl.getFilterInfo = function(payload, id_archi_critdter){

    var toSend=[];

    var query = '';

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }


    var getDterParCrit = function (fulfill, reject) {
        async.each(toSend.critere,
            function (item, callback) {
                var sql = "select * from siterre.blthq_indic_dyn_dter where id_critere =" + item.id_critere + " and id_archi_critdter = " +id_archi_critdter; + " order by ordre "
                db.query(sql)
                    .then(function (rows) {
                        for(var i in toSend.critere) {
                            if( toSend.critere[i].id_critere == item.id_critere)
                            {
                                toSend.critere[i].dter = rows;
                                break;
                            }
                        }
                        callback();
                    })
                    .catch(callback);
            },
            function (tasksError) {
                // Function called at first error or after the success of all tasks
                if (tasksError) {
                    console.log(tasksError);// Something went wrong
                    { return reject(tasksError); }
                }
                // Here, everything is done
                return fulfill();
            });
    };


    query =" select * from siterre.blthq_descpr_value_filter where id_archi_critdter = "  + id_archi_critdter;
    return db.one(query)
        .then(function(rs) {
            toSend=rs;
            query = " select * from siterre.blthq_indic_dyn_critere where id_archi_critdter = " + id_archi_critdter +" order by id_critere";
            return db.query(query)
        })
        .then(function(rs) {
            toSend.critere = rs;
            return new Promise(getDterParCrit)
        })
        .then(function(rs){
            return new Promise.resolve(toSend);
        });
}


// ============ Save Filter dter   ========================================================================================== DEF
ctrl.saveArchiFilter = function(payload, data){
    var query = '';

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }

    query = "update siterre.blthq_descpr_value_filter " +
        "set tbl= '" + data.tbl + "', " +
        " nom_archi_critdter= '" + data.nom_archi_critdter + "' " +
        " where id_archi_critdter =" + data.id_archi_critdter;

    return db.query(query)
        .then(function(rs){
            return new Promise.resolve(rs);
        })


}


// ============ Save Filter dter   ========================================================================================== DEF
ctrl.postArchiFilter = function(payload, data){
    var query = '';

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }

    query = "select max(id_archi_critdter)+1 as id_archi_critdter from siterre.blthq_descpr_value_filter"

    return db.one(query)
        .then(function(rs) {
            data.id_archi_critdter = rs.id_archi_critdter;
            query = "insert into siterre.blthq_descpr_value_filter (id_archi_critdter, tbl, nom_archi_critdter) VALUES "
                + "("+ data.id_archi_critdter + ", '"
                + data.tbl.replace("'","''") + "', '"
                + data.nom_archi_critdter.replace("'","''") + "'); "

            return db.query(query)
        })
        .then(function(rs) {
            return async.forEach( data.critere, function(item, index,err){
                item.id_archi_critdter = data.id_archi_critdter;
                item.new=true;
                ctrl.saveFilterCritere(payload, item)

            })

        })
        .then(function(rs) {

            for(var i=0; i < data.critere.length ; i++){
                async.forEach(data.critere[i].dter, function(dter, index, err) {
                    dter.new=true;
                    dter.id_archi_critdter = data.id_archi_critdter;
                    ctrl.saveFilterDter(payload, dter)
                })
            }
            return new Promise.resolve();

        })
        .then(function(rs){
            return new Promise.resolve(rs);
        })


}


// ============ Deletet Filter archi ========================================================================================== DEF
ctrl.deleteArchiFilter = function(payload, data){
    var query = '';

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }

    query =" delete from siterre.blthq_descpr_value_filter where id_archi_critdter= " + data.id_archi_critdter ;

    return db.query(query)
        .then(function(rs) {
            query = " delete from siterre.blthq_indic_dyn_critere where id_archi_critdter= " + data.id_archi_critdter ;
            return db.query(query)
        })
        .then(function(rs) {
            query = " delete from siterre.blthq_indic_dyn_dter where id_archi_critdter= " + data.id_archi_critdter ;
            return db.query(query)
        })
        .then(function(rs){
            return new Promise.resolve(rs)
        })
        .catch(function(err){
            return new Promise.reject(err);
        })
};




// ============ Save Filter dter   ========================================================================================== DEF
ctrl.saveFilterDter = function(payload, data){
    var query = '';

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }

    var query = " SELECT column_name FROM information_schema.columns WHERE table_schema = 'siterre' AND table_name = 'blthq_indic_dyn_dter' ";

    return db.query(query)
        .then(function(colname){

            if(data.new == true){
                query= 'insert into siterre.blthq_indic_dyn_dter ('
                for (var col in colname) {
                    query = query + colname[col].column_name + " ,";
                }
                query = query.slice(0,-2) + ') VALUES (';
                for (var col in colname) {

                    if(typeof data[colname[col].column_name] == 'string') {

                        if ( data[colname[col].column_name].indexOf("'") != -1) {
                            query = query +"'" + data[colname[col].column_name].replace("'", "''") + "', "
                        } else {
                            query = query + "'" + data[colname[col].column_name] + "', "
                        }
                    }
                    else {
                        query = query  + data[colname[col].column_name] + ", ";
                    }

                }

                query = query.slice(0,-2) + ")"
            }
            else {

                query = "update siterre.blthq_indic_dyn_dter set "
                for (var col in colname) {

                    if(typeof data[colname[col].column_name] == 'string') {

                        if ( data[colname[col].column_name].indexOf("'") != -1) {
                            query = query + colname[col].column_name + " = '" + data[colname[col].column_name].replace("'", "''") + "', "
                        } else {
                            query = query + colname[col].column_name + " = '" + data[colname[col].column_name] + "', "
                        }
                    }
                    else {
                        query = query + colname[col].column_name + " = " + data[colname[col].column_name] + ", ";
                    }
                }

                query = query.slice(0,-2) + " WHERE id_archi_critdter = " + data.id_archi_critdter + " and " +
                    " id_critere =" + data.id_critere + " and id_dter = "+  data.id_dter

            }


            return db.query(query);
        })
        .then(function(rs){
            return new Promise.resolve(rs)
        })
        .catch(function(err){
            return new Promise.reject(err);
        })

}

// ============ Deletet Filter dter   ========================================================================================== DEF
ctrl.deleteFilterDter = function(payload, data){
    var query = '';

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }

    query =" delete from siterre.blthq_indic_dyn_dter where id_archi_critdter= " + data.id_archi_critdter + " and " +
        "id_critere= " + data.id_critere + " and id_dter = "+data.id_dter;

    return db.query(query)
        .then(function(rs){
            return new Promise.resolve(rs)
        })
        .catch(function(err){
            return new Promise.reject(err);
        })
}

// ============ Save Filter critere   ========================================================================================== DEF
ctrl.saveFilterCritere = function(payload, data){
    var query = '';

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }

    var query = " SELECT column_name FROM information_schema.columns WHERE table_schema = 'siterre' AND table_name = 'blthq_indic_dyn_critere' ";

    return db.query(query)
        .then(function(colname){

            if(data.new == true){
                query= 'insert into siterre.blthq_indic_dyn_critere ('
                for (var col in colname) {
                    query = query + colname[col].column_name + " ,";
                }
                query = query.slice(0,-2) + ') VALUES (';
                for (var col in colname) {

                    if(typeof data[colname[col].column_name] == 'string') {

                        if ( data[colname[col].column_name].indexOf("'") != -1) {
                            query = query +"'" + data[colname[col].column_name].replace("'", "''") + "', "
                        } else {
                            query = query + "'" + data[colname[col].column_name] + "', "
                        }
                    }
                    else {
                        query = query  + data[colname[col].column_name] + ", ";
                    }

                }

                query = query.slice(0,-2) + ")"
            }
            else {

                query = "update siterre.blthq_indic_dyn_critere set "
                for (var col in colname) {

                    if(typeof data[colname[col].column_name] == 'string') {

                        if ( data[colname[col].column_name].indexOf("'") != -1) {
                            query = query + colname[col].column_name + " = '" + data[colname[col].column_name].replace("'", "''") + "', "
                        } else {
                            query = query + colname[col].column_name + " = '" + data[colname[col].column_name] + "', "
                        }
                    }
                    else {
                        query = query + colname[col].column_name + " = " + data[colname[col].column_name] + ", ";
                    }
                }

                query = query.slice(0,-2) + " WHERE id_archi_critdter = " + data.id_archi_critdter + " and " +
                    " id_critere =" + data.id_critere

            }


            return db.query(query);
        })
        .then(function(rs){
            return new Promise.resolve(rs)
        })
        .catch(function(err){
            return new Promise.reject(err);
        })
}

// ============ Delete Filter critere   ========================================================================================== DEF
ctrl.deleteFilterCritere = function(payload, data){
    var query = '';

    if (payload.role != 1) {
        throw new Errors.IllegalArgumentError("Super Admin Only !.");
    }
    query =" delete from siterre.blthq_indic_dyn_critere where id_archi_critdter= " + data.id_archi_critdter + " and " +
        "id_critere= " + data.id_critere ;

    return db.query(query)
        .then(function(rs) {
            query = " delete from siterre.blthq_indic_dyn_dter where id_archi_critdter= " + data.id_archi_critdter + " and " + "id_critere= " + data.id_critere;
            return db.query(query)
        })
        .then(function(rs){
            return new Promise.resolve(rs)
        })
        .catch(function(err){
            return new Promise.reject(err);
        })

}