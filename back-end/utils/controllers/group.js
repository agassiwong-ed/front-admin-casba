
/**
 * Created by Victor Le Deuff - Energies Demain -  on 10/07/2017.
 */


"use strict";
var async = require('async');
var Errors = require("../utils/custom-errors");
var Promise = require('promise');
var spex = require('spex')(Promise);
var QueryStream = require('pg-query-stream');

var ctrl = {};

module.exports = ctrl;

// ============ Get Group infos by ID    =============================================================================== DEF

ctrl.getGroupID = function(arg) {
    return new Promise (function (resolve, reject)  {
            // Async task : Query all DB informations
            function async(arg, callback) { //Not the same callback
                db.query(arg).then(function (res) {
                    callback(res)
                });
            }

            // Final task : assemble and format datas
            function final() {
                var groups = results[0];
                var users = results[1];
                var indicators = results[2];

                var to_push = [];
                var group = 0;
                var user = [];
                var indicator = [];
                var territoire = {};

                for (var grp = 0; grp < groups.length; grp++) {

                    group = groups[grp];
                    user = [];
                    indicator = [];
                    // User recuperation
                    for (var usr = 0; usr < users.length; usr++) {
                        if (users[usr].id == group.id) {
                            user.push(
                                {
                                    login: users[usr].login,
                                    nom: users[usr].usr_nom,
                                    surname: users[usr].surname,
                                    statut: null, //Quel interet?
                                    mail: users[usr].mail,
                                    role: users[usr].role,
                                    lastcon: users[usr].lastcon

                                });
                        }
                    }
                    //Indicators recuperation
                    for (var indic = 0; indic < indicators.length; indic++) {
                        if (indicators[indic].id_group == group.id) {
                            indicator[indicators[indic].id_indicateur] = indicators[indic].id_indicateur;
                        }
                    }
                    //Territories recuperation

                    territoire = {
                        granularity: {
                            type_ter: group.granularity,
                            lib_granularity_long: group.lib_granularity_long,
                            lib_granularity_short: group.lib_granularity_short

                        },
                        scale: {
                            id_ter: group.scale_id_ter,
                            lib_scale_long: group.lib_scale_long,
                            lib_scale_short: group.lib_scale_short,
                            lib_ter: group.lib_ter,
                            type_ter: group.scale_type_ter
                        }
                    };

                    to_push.push({
                            id: group.id,
                            nom: group.nom,
                            users: user,
                            indicators: indicator,
                            territoires: territoire
                        });
                }

                //callback(to_push);
                if(to_push.length == 1 ){
                    resolve(to_push[0]);
                }
                else{
                    resolve(to_push);
                }
            }

            // A simple async series:


            var groups_query = " select distinct "
                + " grp.id, grp.name as nom, "
                + " scale_id_ter, lib_ter, scale_type_ter,  lib_scale_short, lib_scale_long, "
                + " granularity,  lib_granularity_short, lib_granularity_long "
                + " from siterre.admin_groups grp ";

            var users_query = "select distinct "
                + " grp.id, grp.name as nom, usr.login,  usr.name as usr_nom,  usr.surname, usr.role, usr.mail, usr.d_last_con as lastcon "
                + " from siterre.admin_groups grp "
                + " Join siterre.admin_users usr on usr.groups=grp.id ";

            var indic_query = "select distinct id_group, id_indicateur "
                + " from siterre.admin_groups_indic ";


            if (arg && arg != 'undefined' && arg !== null && arg !='null') {
                groups_query += " WHERE grp.id   = " + arg + " ORDER BY grp.id; ";
                users_query  += " WHERE grp.id   = " + arg + " ORDER BY grp.id; ";
                indic_query  += " WHERE id_group = " + arg + " ORDER BY id_group; ";
            }
            else {
                groups_query += " ORDER BY grp.id; ";
                users_query  += " ORDER BY grp.id; ";
                indic_query  += " ORDER BY id_group; ";
            }

            var items = [groups_query, users_query, indic_query];
            var results = [];

            function series(item) {
                if (item) {
                    async(item, function (result) {
                        if (result) {
                            results.push(result);
                            series(items.shift());
                        }
                        else{
                            reject();
                        }
                    });
                } else {
                     final()
                }
            }

            series(items.shift())



    });
};

// ============ delete  users    ======================================================================================= DEF
ctrl.deleteGroup = function(payload, group) {
    if (!group.id_group  || group.id_group  == 'undefined' || group.id_group  === null ) {
        throw new Errors.IllegalArgumentError("Id group is required.");
    }
    if (payload.role != 1  ) {
        throw new Errors.IllegalArgumentError("Insufficant privileges");
    }

    var sql = "DELETE FROM siterre.admin_groups  where id=" + group.id_group ;

    if(payload.group != null) {
        sql+= "and groups = "+ payload.group;
    }

    return db.query(sql)
        .then( function() {
            return db.query ("DELETE FROM siterre.admin_groups_indic  WHERE id_group ="+group.id_group )
        })
};

// ============ update  group infos    ================================================================================= DEF
ctrl.updateGroup = function(payload, group) {
    if (payload.group != null && payload.role >= 3 ) {
        throw new Errors.IllegalArgumentError("Insuffisant privileges");
    }
    if (!group  || group  == 'undefined' || group  === null ) {
        throw new Errors.IllegalArgumentError("group is required.");
    }
    var sql= null;
    if (group.nom) {
         sql = "UPDATE siterre.admin_groups SET "
                 + " name='" + group.nom + "'"
                 + " WHERE id = " + group.id;
    }
    group.territoires.scale.id_ter = group.territoires.scale.id_ter.replace(/'/g, "''");

    if (group.territoires) {
         sql = "UPDATE siterre.admin_groups SET "
            + "   scale_type_ter='" + group.territoires.scale.type_ter + "'"
            + " , scale_id_ter='"   + group.territoires.scale.id_ter + "'"
            + " , lib_ter='"        + group.territoires.scale.lib_ter + "'"
            + " , lib_scale_short='"+ group.territoires.scale.lib_scale_short + "'"
            + " , lib_scale_long='" + group.territoires.scale.lib_scale_long + "'"
            + " , granularity='"           + group.territoires.granularity.type_ter + "'"
            + " , lib_granularity_short='" + group.territoires.granularity.lib_granularity_short + "'"
            + " , lib_granularity_long='"  + group.territoires.granularity.lib_granularity_long + "'"
            + "    WHERE id = " + group.id;

        //ajouter libelles
    }

    return db.query(sql);
};

// ============ update  group indic    ================================================================================= DEF
ctrl.updateGroupIndicators = function(payload, group) {
    if (payload.group != null && payload.role >= 3 ) {
        throw new Errors.IllegalArgumentError("Insuffisant privileges");
    }
    if (!group  || group  == 'undefined' || group  === null ) {
        throw new Errors.IllegalArgumentError("group is required.");
    }
    return db.query("Delete FROM siterre.admin_groups_indic where id_group="+group.id)
        .then(function(){
            var query ="INSERT INTO siterre.admin_groups_indic( id_group, id_indicateur) VALUES";
            for (var indic in  group.indicators) {
                query += "  ( "+ group.id  +" , " + indic + " ),";
            }
            query = query.substring(0, query.length-1);
            return db.query(query);


        });
};

// ============ new  Group    ========================================================================================== DEF
ctrl.newGroup = function(payload, group) {
    var id_new = null;
    if (payload.group != null && payload.role >= 3 ) {
        throw new Errors.IllegalArgumentError("Insuffisant privileges");
    }
    if (!group  || group  == 'undefined' || group  === null ) {
        throw new Errors.IllegalArgumentError("group is required.");
    }

    return db.one("select nextval('siterre.admin_groups_id_seq') as id_new") //récupération du prochain identifiant libre
        .then(function(res) {
            id_new = res.id_new;

            group.territoires.scale.id_ter = group.territoires.scale.id_ter.replace(/'/g, "''");
            group.territoires.scale.lib_ter = group.territoires.scale.lib_ter.replace(/'/g, "''");
            if(group.territoires.scale.nom)
                group.territoires.scale.nom = group.territoires.scale.nom.replace(/'/g, "''");

            group.nom=group.nom.replace(/'/g, "''");
            var sql = " INSERT INTO siterre.admin_groups( "
                + " id, name, scale_type_ter, scale_id_ter, granularity, lib_scale_short,"
                + " lib_scale_long, lib_granularity_short, lib_granularity_long, lib_ter "
                + ") VALUES ("
                + res.id_new
                + " , '" + group.nom
                + "', '" + group.territoires.scale.type_ter
                + "', '" + group.territoires.scale.id_ter
                + "', '" + group.territoires.granularity.type_ter
                + "', '" + group.territoires.scale.lib_scale_short
                + "', '" + group.territoires.scale.lib_scale_long
                + "', '" + group.territoires.granularity.lib_granularity_short
                + "', '" + group.territoires.granularity.lib_granularity_long
                + "', '" + group.territoires.scale.lib_ter
                + "'); ";

            return db.query(sql)
        })
        .then (function() { //Insertion des indicateurs
            var query ="INSERT INTO siterre.admin_groups_indic( id_group, id_indicateur) VALUES";
             for (var indic in  group.indicators) {
                query += "  ( "+ id_new  +" , " + indic + " ),";
             }
            query = query.substring(0, query.length-1);
            return db.query(query);
        });
};

