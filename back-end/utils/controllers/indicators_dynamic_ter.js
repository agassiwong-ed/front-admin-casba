
"use strict";
var async = require('async');
var Errors = require("../utils/custom-errors");
var Promise = require('promise');
var spex = require('spex')(Promise);
var QueryStream = require('pg-query-stream');

var ctrl = {};

module.exports = ctrl;

// ============ Get Catalogue Critères + deter en fonction de l'indicateur ============================================= DEF
ctrl.getCatalogueCriteresDeterminants = function (idIndic) {

    var tab_critDter = [];
    var tab_crit = [];
    var id_archi_critdter = -1;

    var getDterParCrit = function (fulfill, reject) {
        async.each(tab_crit,
            function (item, callback) {
                db.query("SELECT id_dter, lib_dter, ordre " +
                    "FROM siterre.blthq_indic_dyn_dter " +
                    "WHERE id_archi_critdter = $1 AND id_critere = $2", [id_archi_critdter, item.id_critere ])
                    .then(function (rows) {
                        var toPush = {};
                        toPush = item ;
                        toPush.donnees_dter = [];
                        for (var i = 0; i < rows.length; i++) {
                            toPush.donnees_dter.push(rows[i]);
                        }
                        toPush.donnees_dter.push({id_dter : 'null',
                            lib_dter : 'Non renseigné',
                            ordre: 999});
                        tab_critDter.push(toPush);
                        callback();
                    })
                    .catch(callback);
            },
            function (tasksError) {
                // Function called at first error or after the success of all tasks
                if (tasksError) {
                    { return reject(tasksError); }
                }
                // Here, everything is done
                return fulfill();
            });
    };

    if (!idIndic  || idIndic == 'undefined' || idIndic === null ) {
        throw new Errors.IllegalArgumentError("idIndic is required.");
    }

    // on récupère d'abord les critères.
    return db.one("SELECT id_archi_critdter FROM siterre.blthq_indicateurs WHERE id_indicateur = $1", [idIndic])
        .then(function(res){
            id_archi_critdter = parseInt(res.id_archi_critdter);

            return db.query("SELECT id_critere, lib_critere, champ_associe " +
                "FROM siterre.blthq_indic_dyn_critere " +
                "WHERE id_archi_critdter = $1", [id_archi_critdter])})
        .then(function(rows){
            for (var i = 0; i < rows.length; i++) {
                tab_crit.push(rows[i]);
            }
            return new Promise(getDterParCrit)
        })
        .then(function(rslt) {
            return new Promise.resolve(tab_critDter)
        })
        .catch(function(err){
            return new Promise.reject(err)
        })
};

// ============ Get DTER VALUE For FILTERS  ============================================================================ DEF
//27 000
ctrl.getDterValue_old = function (dataQuery, filter, payload) {

    var varSum = ''; //"nlog";
    var architectureCritDter = [];
    var bdSource = "";
    var infoIndic = {};
    var info_tbl= {};

    var filterTer = {
        joinString : "",
        whereString : null
    };

    if (!dataQuery.id_indicateur  || dataQuery.id_indicateur == 'undefined' || dataQuery.id_indicateur === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!dataQuery.scale  || dataQuery.scale == 'undefined' || dataQuery.scale === null ) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }
    if (!dataQuery.scale_filter  || dataQuery.scale_filter == 'undefined' || dataQuery.scale_filter === null ) {
        throw new Errors.IllegalArgumentError("scale_filter is required.");
    }
    if (!dataQuery.year  || dataQuery.year == 'undefined' || dataQuery.year === null ) {
        throw new Errors.IllegalArgumentError("year is required.");
    }
    if (!dataQuery.id_struct  || dataQuery.id_struct == 'undefined' || dataQuery.id_struct === null ) {
        throw new Errors.IllegalArgumentError("id_struct is required.");
    }

    var idIndicator = parseInt(dataQuery.id_indicateur);
    // var idStruct = parseInt(dataQuery.id_struct);
    var scale = dataQuery.scale;
    var scale_filter = dataQuery.scale_filter;
    var year = parseInt(dataQuery.year);
    var infoIndicDefFilter = {};

    return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur = $1", [idIndicator])
        .then(function(res){
            infoIndic = res;
            return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur = $1", [infoIndic.id_indic_for_filter_defaut])
        })
        .then(function(res){
            infoIndicDefFilter = res;
            varSum = infoIndicDefFilter.champ_p_associe ;
            return getCritDterArchitecture(parseInt(infoIndic.id_archi_critdter))
        })
        .then(function(architecture) {
            architectureCritDter = architecture;
            return db.one("SELECT * FROM siterre.blthq_descpr_value_tbl WHERE tbl = $1 AND (year_data = 9999 OR year_data=$2)", [infoIndic.tbl, year])
        })
        .then(function(res){

            // console.log("info_tbl : ok");
            info_tbl=res;
            var table_ter_imb = "ter_cr_" + year;
            if(info_tbl.crrsdc_ter == 'ter_01' || info_tbl.crrsdc_ter == 'ter_02'  || scale.substr(0,6) == 'ter_01' || scale.substr(0,6) == 'ter_02' || scale_filter.substr(0,6) == 'ter_01' || scale_filter.substr(0,6) == 'ter_02'   || info_tbl.champ_ter.substr(0,6) == 'ter_01' || info_tbl.champ_ter.substr(0,6) == 'ter_02' ){
                 table_ter_imb = "ter_cr_" + year+"_full";

            }
            else {
                 table_ter_imb = "ter_cr_" + year;
            }

            if (scale_filter && scale_filter.indexOf("'") == -1){
                filterTer.joinString =
                    " LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+ scale +" as scl " +
                    "FROM siterre." + table_ter_imb + " " +
                    "WHERE " + scale +" = '"+ scale_filter +"' " +
                    " AND  ter_" + payload.scale_type_ter_int + "_" + year + " in (" + payload.scale_id_ter +") " +
                    "GROUP BY correspondance, scl" +
                    ") as tr " +
                    " ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";

                filterTer.whereString = " scl='" + scale_filter + "'";
            }
            else if(scale_filter && scale_filter.indexOf("'") != -1){
                filterTer.joinString =
                    " LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+ scale +" as scl " +
                    "FROM siterre." + table_ter_imb + " " +
                    "WHERE " + scale +" in ("+ scale_filter +") " +
                    " AND  ter_" + payload.scale_type_ter_int + "_" + year + " in (" + payload.scale_id_ter +") " +
                    "GROUP BY correspondance, scl" +
                    ") as tr " +
                    " ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";

                filterTer.whereString = " scl in (" + scale_filter + ")";
            }


            // if (idIndicator && idIndicator>0) {

            bdSource = "siterre." + infoIndic.tbl;

            return defineAndDoQuery(varSum, architectureCritDter, bdSource, filter, filterTer)
        })
        .then(function(rslt){
            // mise en forme des donn�es.
            var factortoMainUnitStruct = 1;
            if (infoIndicDefFilter.facteur_to_main_unit !== null ){
                factortoMainUnitStruct = Number(infoIndicDefFilter.facteur_to_main_unit)
            }

            var totalStruct = factortoMainUnitStruct * rslt[varSum] ;
            if (totalStruct === 0 ){totalStruct=1}

            for ( var i = 0; i < architectureCritDter.length; i++) {
                for ( var j = 0;  j < architectureCritDter[i].donnees_dter.length; j++) {
                    architectureCritDter[i].donnees_dter[j].dataIndicVal = 1;
                    architectureCritDter[i].donnees_dter[j].dataIndic  = 1 ;
                    architectureCritDter[i].donnees_dter[j].dataStructVal = factortoMainUnitStruct * rslt[architectureCritDter[i].id_critere + "_" + architectureCritDter[i].donnees_dter[j].id_dter] ;
                    if (!totalStruct || totalStruct==0 || totalStruct==undefined){
                        architectureCritDter[i].donnees_dter[j].dataStruct  = 0
                    }
                    else {
                        architectureCritDter[i].donnees_dter[j].dataStruct  = 1 * architectureCritDter[i].donnees_dter[j].dataStructVal / totalStruct;
                    }
                }
            }

            return new Promise.resolve(architectureCritDter) ;
        })
        .catch(function(err){
            console.log(err);
            return new Promise.reject(err)
        })
};


ctrl.getDterValue = function (dataQuery, filter, payload) {

    var varSum = ''; //"nlog";
    var architectureCritDter = [];
    var bdSource = "";
    var infoIndic = {};
    var info_tbl = {};

    var filterTer = {
        joinString: "", whereString: null
    };

    if (!dataQuery.id_indicateur || dataQuery.id_indicateur == 'undefined' || dataQuery.id_indicateur === null) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!dataQuery.scale || dataQuery.scale == 'undefined' || dataQuery.scale === null) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }
    if (!dataQuery.scale_filter || dataQuery.scale_filter == 'undefined' || dataQuery.scale_filter === null) {
        throw new Errors.IllegalArgumentError("scale_filter is required.");
    }
    if (!dataQuery.year || dataQuery.year == 'undefined' || dataQuery.year === null) {
        throw new Errors.IllegalArgumentError("year is required.");
    }
    if (!dataQuery.id_struct || dataQuery.id_struct == 'undefined' || dataQuery.id_struct === null) {
        throw new Errors.IllegalArgumentError("id_struct is required.");
    }

    var idIndicator = parseInt(dataQuery.id_indicateur);
    // var idStruct = parseInt(dataQuery.id_struct);
    var scale = dataQuery.scale;
    var scale_filter = dataQuery.scale_filter;
    var year = parseInt(dataQuery.year);
    var infoIndicDefFilter = {};

    return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur = $1", [idIndicator])
        .then(function (res) {
            infoIndic = res;
            return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur = $1", [infoIndic.id_indic_for_filter_defaut])
        })
        .then(function (res) {
            infoIndicDefFilter = res;
            varSum = infoIndicDefFilter.champ_p_associe;
            return getCritDterArchitecture(parseInt(infoIndic.id_archi_critdter));
        })
        .then(function (architecture) {
            architectureCritDter = architecture;
            return db.one("SELECT * FROM siterre.blthq_descpr_value_tbl WHERE tbl = $1 AND (year_data = 9999 OR year_data=$2)", [infoIndic.tbl, year])
        })
        .then(function (res) {
            // console.log("info_tbl : ok");
            info_tbl=res;
            var table_ter_imb = "ter_cr_" + year;
            if(info_tbl.crrsdc_ter == 'ter_01' || info_tbl.crrsdc_ter == 'ter_02'  || scale.substr(0,6) == 'ter_01' || scale.substr(0,6) == 'ter_02' || scale_filter.substr(0,6) == 'ter_01' || scale_filter.substr(0,6) == 'ter_02'   || info_tbl.champ_ter.substr(0,6) == 'ter_01' || info_tbl.champ_ter.substr(0,6) == 'ter_02' ){
                table_ter_imb = "ter_cr_" + year+"_full";

            }
            else {
                table_ter_imb = "ter_cr_" + year;
            }

            if (scale_filter && scale_filter.indexOf("'") == -1){
                filterTer.joinString =
                    " LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+ scale +" as scl " +
                    "FROM siterre." + table_ter_imb + " " +
                    "WHERE " + scale +" = '"+ scale_filter +"' " +
                    " AND  ter_" + payload.scale_type_ter_int + "_" + year + " in (" + payload.scale_id_ter +") " +
                    "GROUP BY correspondance, scl" +
                    ") as tr " +
                    " ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";

                filterTer.whereString = " scl='" + scale_filter + "'";
            }
            else if(scale_filter && scale_filter.indexOf("'") != -1){
                filterTer.joinString =
                    " LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+ scale +" as scl " +
                    "FROM siterre." + table_ter_imb + " " +
                    "WHERE " + scale +" in ("+ scale_filter +") " +
                    " AND  ter_" + payload.scale_type_ter_int + "_" + year + " in (" + payload.scale_id_ter +") " +
                    "GROUP BY correspondance, scl" +
                    ") as tr " +
                    " ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";

                filterTer.whereString = " scl in (" + scale_filter + ")";
            }


            bdSource = "siterre." + infoIndic.tbl;
            return dterForEachCriter(varSum, architectureCritDter, bdSource, filter, filterTer);
        })
        .then(function(rslt){
            // mise en forme des donn�es.
            var factortoMainUnitStruct = 1;
            if (infoIndicDefFilter.facteur_to_main_unit !== null ){
                factortoMainUnitStruct = Number(infoIndicDefFilter.facteur_to_main_unit)
            }

            var totalStruct = factortoMainUnitStruct * rslt[varSum] ;
            if (totalStruct === 0 ){totalStruct=1}

            //Calcul du total
            for ( var i = 0; i < architectureCritDter.length; i++) {
                totalStruct=0;
                for ( var j = 0;  j < architectureCritDter[i].donnees_dter.length; j++) {
                    if(! rslt[architectureCritDter[i].id_critere + "_" + architectureCritDter[i].donnees_dter[j].id_dter])
                        rslt[architectureCritDter[i].id_critere + "_" + architectureCritDter[i].donnees_dter[j].id_dter] = 0;
                    totalStruct = totalStruct + factortoMainUnitStruct * rslt[architectureCritDter[i].id_critere + "_" + architectureCritDter[i].donnees_dter[j].id_dter];
                      }

                for ( var j = 0;  j < architectureCritDter[i].donnees_dter.length; j++) {
                    architectureCritDter[i].donnees_dter[j].dataIndicVal = 1;
                    architectureCritDter[i].donnees_dter[j].dataIndic  = 1 ;
                    architectureCritDter[i].donnees_dter[j].dataStructVal = factortoMainUnitStruct * rslt[architectureCritDter[i].id_critere + "_" + architectureCritDter[i].donnees_dter[j].id_dter] ;

                    if (!totalStruct || totalStruct==0 || totalStruct==undefined){
                        architectureCritDter[i].donnees_dter[j].dataStruct  = 0
                    }
                    else {
                        architectureCritDter[i].donnees_dter[j].dataStruct  = 1 * architectureCritDter[i].donnees_dter[j].dataStructVal / totalStruct;
                    }
                }

            }

            return new Promise.resolve(architectureCritDter) ;
        })
        .catch(function(err){
            console.log(err);
            return new Promise.reject(err)
        })
};

// ============ Get indic value for map ================================================================================  DEF
ctrl.getValueIndicForMap = function(dataQuery, filter, payload){

    var mainSqlQuery = '';
    var stringSelect = '';
    var stringWhere = '';
    var stringJoinTer = "";
    var stringGroupBy = "";
    var stringFROM = "";

    var datas = {};
    var baseSource = '';

    if (!dataQuery.id  || dataQuery.id == 'undefined' || dataQuery.id === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!dataQuery.scale  || dataQuery.scale == 'undefined' || dataQuery.scale === null ) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }
    if (!dataQuery.scale_filter  || dataQuery.scale_filter == 'undefined' || dataQuery.scale_filter === null ) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }
    if (!dataQuery.year  || dataQuery.year == 'undefined' || dataQuery.year === null ) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }
    if (!dataQuery.granularity  || dataQuery.granularity == 'undefined' || dataQuery.granularity === null ) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }

    var idIndicator = parseInt(dataQuery.id);
    var granularity = dataQuery.granularity;
    var scale = dataQuery.scale;
    var scale_filter = dataQuery.scale_filter;
    var year = parseInt(dataQuery.year);

    // On ramènes les infos relatives à l'indicateur
    var info_indic = {};
    var info_tbl = {};

    // On va chercher les caractéristiques de l'indicateur
    return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur=$1", [idIndicator])
        .then(function(rslt) {
            info_indic = rslt;
            baseSource = "siterre." +  info_indic.tbl;

            return db.one("SELECT * FROM siterre.blthq_descpr_value_tbl WHERE tbl = $1 AND (year_data = 9999 OR year_data=$2)", [info_indic.tbl, year])
        })
        .then(function(res){

            info_tbl = res;

            stringSelect = " tr.grn as id_ter, ";

            if (info_indic.champ_divider !== '' && info_indic.champ_divider !== null) {
                stringSelect =  stringSelect + 'sum('+info_indic.champ_p_associe + ') as valuep, sum(case when '+info_indic.champ_p_associe+' is not null then '+info_indic.champ_divider+' else 0 end) as valued ';
            }
            else {
                stringSelect =  stringSelect + 'sum('+info_indic.champ_p_associe +') as valuep ';
            }

            stringFROM = " FROM siterre." + info_indic.tbl + " as bd ";

            var table_ter_imb = "ter_cr_2015" ;
            if(info_tbl.crrsdc_ter == 'ter_01' || info_tbl.crrsdc_ter == 'ter_02'|| scale.substr(0,6) == 'ter_01' || scale.substr(0,6) == 'ter_02' || granularity.substr(0,6) == 'ter_01' || granularity.substr(0,6) == 'ter_02'|| scale_filter.substr(0,6) == 'ter_01' || scale_filter.substr(0,6) == 'ter_02'){
                table_ter_imb = "ter_cr_2015_full";

            }
            else {
                table_ter_imb = "ter_cr_2015";
            }

            if (scale_filter){
                if(scale_filter.indexOf("'") == -1 )
                    scale_filter = "'" + scale_filter + "'";
                stringJoinTer =
                    "LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+ granularity +" as grn, "+scale +" as scl " +
                    "FROM siterre."+table_ter_imb+ " " +
                    "WHERE " + scale +"  in ("+ scale_filter +") " +
                    " AND  ter_" + payload.scale_type_ter_int + "_2015" + " in (" + payload.scale_id_ter +") " +
                    "GROUP BY correspondance, grn, scl" +
                    ") as tr ";
            }
            else {
                stringJoinTer =
                    "LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+ granularity +" as grn " +
                    "FROM siterre."+ table_ter_imb +" " +
                    " WHERE  ter_" + payload.scale_type_ter_int + "_2015" + " in (" + payload.scale_id_ter +") " +
                    "GROUP BY correspondance, grn" +
                    ") as tr ";
            }

            stringJoinTer = stringJoinTer +
                "ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";

            if (scale_filter){
                stringWhere = "WHERE tr.scl  in(" + scale_filter +") "
            }

            if (info_tbl.multi_year == 1 && year) {
                if (stringWhere !== ''){
                    stringWhere = stringWhere + " AND bd." + info_tbl.champ_year + "=" + year  + " "
                }
                else {
                    stringWhere = " WHERE bd." + info_tbl.champ_year + "=" + year  + " "
                }
            }

            stringGroupBy = "GROUP BY tr.grn ";

            return constructFilter(filter);
        })
        .then(function(rslt){

            // console.log("constructFilter : ok")
            if (rslt !== '') {
                if (stringWhere !== '') {
                    stringWhere = stringWhere + ' AND ' + rslt + ' ';
                }
                else {
                    stringWhere = ' WHERE ' + rslt + ' ';
                }
            }

            mainSqlQuery = "SELECT " + stringSelect + stringFROM + stringJoinTer + stringWhere + stringGroupBy;

            // console.log(mainSqlQuery);
            var factortoMainUnit = 1;
            if (info_indic.facteur_to_main_unit !== null ){
                factortoMainUnit = Number(info_indic.facteur_to_main_unit)
            }

            var qs = new QueryStream(mainSqlQuery, [], { batchSize: 10 });
            return db.stream(qs, function (s) {
                return spex.stream.read(s, function (index, rows) {
                    // Executed for each row (or by batch of rows)
                    for (var i = 0; i < rows.length; i++) {

                        if (!rows[i].valued){
                            datas[rows[i].id_ter] = {
                                id_ter: rows[i].id_ter,
                                value: Number(rows[i].valuep * factortoMainUnit)
                            };
                        } else {
                            datas[rows[i].id_ter] = {
                                id_ter: rows[i].id_ter,
                                value :  Number(rows[i].valuep) / Number(rows[i].valued ) * factortoMainUnit
                            };
                        }
                    }
                })
                    .catch(function(err) {});
            })
        })
        .then(function(){
            return new Promise.resolve(datas);
        })
        .catch(function(err){
            return new Promise.reject(err);
        })
};

// ============ Get sum of indic value =================================================================================  DEF
ctrl.getValueIndicSum= function(dataQuery, filter, payload) {

    var mainSqlQuery = '';
    var stringSelect = '';
    var stringWhere = '';
    var stringJoinTer = "";
    var stringGroupBy = "";
    var stringFROM = "";

    var datas = {};
    var baseSource = '';

    if (!dataQuery.id  || dataQuery.id == 'undefined' || dataQuery.id === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!dataQuery.scale  || dataQuery.scale == 'undefined' || dataQuery.scale === null ) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }
    if (!dataQuery.scale_filter  || dataQuery.scale_filter == 'undefined' || dataQuery.scale_filter === null ) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }
    if (!dataQuery.year  || dataQuery.year == 'undefined' || dataQuery.year === null ) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }

    var idIndicator = parseInt(dataQuery.id);
    var scale = dataQuery.scale;
    var scale_filter = dataQuery.scale_filter;
    var year = parseInt(dataQuery.year);

    // On ramènes les infos relatives à l'indicateur
    var info_indic = {};
    var info_tbl = {};

    // On va chercher les caractéristiques de l'indicateur
    return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur=$1", [idIndicator])
        .then(function(rslt) {
            info_indic = rslt;
            baseSource = "siterre." +  info_indic.tbl;

            return db.one("SELECT * FROM siterre.blthq_descpr_value_tbl WHERE tbl = $1 AND (year_data = 9999 OR year_data=$2)", [info_indic.tbl, year])
        })
        .then(function(res){

            if ( scale_filter && scale_filter.indexOf("'") == -1)
                scale_filter = "'"+scale_filter+"'";


            info_tbl = res;
            stringSelect = "  ";

            if (info_indic.champ_divider !== '' && info_indic.champ_divider !== null) {
                stringSelect =  stringSelect + 'sum('+info_indic.champ_p_associe + ') as valuep, ' +
                    'sum( case when '+info_indic.champ_p_associe + ' is not null then '+info_indic.champ_divider+' else 0 end ) as valued ';
            }
            else {
                stringSelect =  stringSelect + 'sum('+info_indic.champ_p_associe +') as valuep ';
            }

            stringFROM = " FROM siterre." + info_indic.tbl + " as bd ";

            var table_ter_imb = "ter_cr_" + year;

            if(info_tbl.crrsdc_ter == 'ter_01' || info_tbl.crrsdc_ter == 'ter_02' || scale.substr(0,6) == 'ter_01' || scale.substr(0,6) == 'ter_02' || scale_filter.substr(0,6) == 'ter_01' || scale_filter.substr(0,6) == 'ter_02' ){
                table_ter_imb = "ter_cr_" + year +"_full";
            }
            else {
                table_ter_imb = "ter_cr_" + year;
            }


            if (scale_filter){

                stringJoinTer =
                    "LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+ scale +" as scl " +
                    "FROM siterre." + table_ter_imb  + " " +
                    "WHERE " + scale +" in ("+ scale_filter +") " +
                    " AND  ter_" + payload.scale_type_ter_int + "_" + year + " in (" + payload.scale_id_ter +") " +
                    "GROUP BY correspondance, scl" +
                    ") as tr ";
            }
            else {
                stringJoinTer =
                    "LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance " +
                    "FROM siterre." + table_ter_imb  + " " +
                    " WHERE  ter_" + payload.scale_type_ter_int + "_" + year + " in (" + payload.scale_id_ter +") " +
                    "GROUP BY correspondance " +
                    ") as tr ";
            }

            stringJoinTer = stringJoinTer +
                "ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";

            if (scale_filter){
                stringWhere = "WHERE tr.scl in (" + scale_filter +") "
            }

            if (info_tbl.multi_year == 1 && year) {
                if (stringWhere !== ''){
                    stringWhere = stringWhere + " AND bd." + info_tbl.champ_year + "=" + year  + " "
                }
                else {
                    stringWhere = " WHERE bd." + info_tbl.champ_year + "=" + year  + " "
                }
            }

            stringGroupBy = "  ";

            return constructFilter(filter);
        })
        .then(function(rslt){

            if (rslt !== '') {
                if (stringWhere !== '') {
                    stringWhere = stringWhere + ' AND ' + rslt + ' '
                }
                else {
                    stringWhere = ' WHERE ' + rslt + ' '
                }
            }

            mainSqlQuery = "SELECT " + stringSelect + stringFROM + stringJoinTer + stringWhere + stringGroupBy;
            return db.one(mainSqlQuery, [])
        })
        .then(function(res){

            var factortoMainUnit = 1;
            if (info_indic.facteur_to_main_unit !== null ){
                factortoMainUnit = Number(info_indic.facteur_to_main_unit)
            }


            var results = 0;
            if (!res.valued) {
                results = Number(res.valuep) * factortoMainUnit
            }
            else {

                if (Number(res.valued)>0){
                    results = Number(res.valuep) / Number(res.valued ) * factortoMainUnit
                }
                else {
                    results = 0;
                }
            }

            return new Promise.resolve(results);
        })
        .catch(function(err){
            return new Promise.reject(err);
        })
};

// ============ Get CRITER / DETER Architecture (simplfified)   ======================================================== DEF
var getCritDterArchitecture = function(id_archi_critdter) {

    var tab_critDter = [];
    var tab_crit = [];

    var getDterParCrit = function (fulfill, reject) {
        async.each(tab_crit,
            function (item, callback) {
                db.query("SELECT id_dter, ordre " +
                    "FROM siterre.blthq_indic_dyn_dter " +
                    "WHERE id_archi_critdter = $1 AND id_critere = $2 ORDER BY ORDRE", [id_archi_critdter, item.id_critere ])
                    .then(function (rows) {
                        var toPush = {};
                        toPush = item ;
                        toPush.donnees_dter = [];
                        for (var i = 0; i < rows.length; i++) {
                            toPush.donnees_dter.push(rows[i]);
                        }
                        toPush.donnees_dter.push({id_dter : 'null', ordre : 999})
                        tab_critDter.push(toPush);
                        callback();
                    })
                    .catch(callback);
            },
            function (tasksError) {
                // Function called at first error or after the success of all tasks
                if (tasksError) {
                    console.log(tasksError);// Something went wrong
                    { return reject(tasksError); }
                }
                // Here, everything is done
                return fulfill();
            });
    };

    // on r�cup�re d'abord les crit�res.
    return db.query("SELECT id_critere, champ_associe " +
        "FROM siterre.blthq_indic_dyn_critere " +
        "WHERE id_archi_critdter = $1 ", [id_archi_critdter])
        .then(function(rows){
            for (var i = 0; i < rows.length; i++) {
                tab_crit.push(rows[i]);
            }
            return new Promise(getDterParCrit)
        })
        .then(function(rslt) {
            return new Promise.resolve(tab_critDter)
        });

};


//========= Do Query Replacement ===================================================================================== DEF
var dterForEachCriter = function ( varSum, architectureCritDter, bdSource, Objfilter, filterTerInit) {
    var filterTer = cloneByJson(filterTerInit);
    var stringSelect = "sum(" + varSum + ") as data ";
    var stringWhere = "";
    var filter = Objfilter;
    var stringFilter = " ";
    for (var i=0;  i < filter.length; i++) {
        stringFilter = stringFilter + " and " + filter[i];
    }

    var tab_critDter = {};
    var tab_crit = architectureCritDter;

    var getDterValueParCrit = function (fulfill, reject) {
        async.each(tab_crit,
            function (item, callback) {
            var sql = "select coalesce(" +
                    item.champ_associe  + "::character varying, 'null') as id_crit, " +
                    stringSelect +
                    " FROM " + bdSource + " as bd " +
                        filterTer.joinString + " " +
                    " WHERE " + filterTer.whereString + "  " + stringFilter +
                    " group by coalesce(" + item.champ_associe +"::character varying, 'null')"

            db.query(sql)
                    .then(function (rows) {
                        for (var i = 0; i < rows.length; i++) {
                           tab_critDter[item.id_critere + "_" + rows[i].id_crit] = rows[i].data ;
                        }
                        callback();
                    })
                    .catch(callback);
            },
            function (tasksError) {
                // Function called at first error or after the success of all tasks
                if (tasksError) {
                    console.log(tasksError);// Something went wrong
                    { return reject(tasksError); }
                }
                // Here, everything is done
                return fulfill();
            });
    };

    return new Promise(getDterValueParCrit)
        .then(function(rs){
            return new Promise.resolve(tab_critDter);
        });



}

// ============ Do Query  ============================================================================================== DEF
var defineAndDoQuery = function (varSum, architectureCritDter, bdSource, Objfilter, filterTerInit) {

    var filterTer = cloneByJson(filterTerInit);
    var stringSelect = "sum(" + varSum + ") as " + varSum +" ";
    var stringWhere = "";
    var filter = Objfilter;

    if (!varSum || varSum === ""){
        return new Promise.resolve();
    }

    // On cr�e la requ�te.

    for (var i = 0; i < architectureCritDter.length; i++) {
        for (var j = 0;  j < architectureCritDter[i].donnees_dter.length; j++) {
            stringSelect = stringSelect + ', '
                + 'sum (case when ' + architectureCritDter[i].champ_associe + '=' + architectureCritDter[i].donnees_dter[j].id_dter
                + ' then ' + varSum + ' else 0 end) as "'
                + architectureCritDter[i].id_critere + '_' + architectureCritDter[i].donnees_dter[j].id_dter + '" ';
        }
    }

    if (filter.length > 0) {
        stringWhere = "WHERE " + filter[0];
        for (i = 1 ; i <filter.length; i++ ) {
            stringWhere = stringWhere + " AND " + filter[i];
        }

    } else {
        stringWhere = ""
    }


    if (filterTer.whereString) {
        if (stringWhere == ''  ){
            stringWhere = ' WHERE ';  //Ajout de Vic
        } else {
            filterTer.whereString = ' AND ' + filterTer.whereString + ' ';
        }
    }

    return db.one("SELECT " + stringSelect + "FROM "+ bdSource + " as bd "+ filterTer.joinString + " " + stringWhere + filterTer.whereString, [])
        .then(function(rslt) {
            return new Promise.resolve(rslt);
        })
        .catch(function(err) {
            if (!varSum || varSum === ""){
                return new Promise.resolve();
            } else {
                return new Promise.reject(err);
            }
        });
};

//============ Construction filtres   ================================================================================== DEF
var constructFilter = function (filter) {
    var stringWhere = "";
    try{
        if (filter && filter.length > 0) {
            stringWhere = " " + filter[0];
            for (var i = 1 ; i <filter.length; i++ ) {
                stringWhere = stringWhere + " AND " + filter[i];
            }
        } else {
            stringWhere = ""
        }
    }
    catch(err){
        console.log(err);
        stringWhere = ""
    }
    return new Promise.resolve(stringWhere);

};

function cloneByJson(a) {
    return JSON.parse(JSON.stringify(a));
}