
"use strict";
var async = require('async');
var Errors = require("../utils/custom-errors");
var Promise = require('promise');
var spex = require('spex')(Promise);
var QueryStream = require('pg-query-stream');
var dbgeo = require('dbgeo');


var ctrl = {};

module.exports = ctrl;

// ============ Get value for map (Static indicator)     =============================================================== DEF
ctrl.getStaticIndicatorValue = function(payload, data) {


    // data :  { id: '7',
    //     scale: 'ter_50_2015',
    //     scale_filter: '75',
    //     granularity: 'ter_20_2015',
    //     filter: ''
    // }

    if (!data.id  || data.id == 'undefined' || data.id === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!data.scale  || data.scale == 'undefined' || data.scale === null ) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }
    if (!data.scale_filter  || data.scale_filter == 'undefined' || data.scale_filter === null ) {
        throw new Errors.IllegalArgumentError("scale_filter is required.");
    }
    if (!data.granularity || data.granularity == 'undefined' || data.granularity === null ) {
        throw new Errors.IllegalArgumentError("Granularity is required.");
    }
    if (!data.granularity_int || data.granularity_int == 'undefined' || data.granularity_int === null ) {
        throw new Errors.IllegalArgumentError("Granularity is required.");
    }


    var query ='';

    // On ramènes les infos relatives à l'indicateur
    var info_indic = {};
    var info_tbl = {};

    return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur = $1", [parseInt(data.id)])
        .then(function(res){

            info_indic = res;
            // console.log(info_indic)
            return db.one("SELECT * FROM siterre.blthq_descpr_value_tbl WHERE tbl = $1 AND (year_data = 9999 OR year_data=$2)", [info_indic.tbl,  2015])
        })
        .then(function(res){
            info_tbl = res;
            // console.log(info_tbl)

            if (data.scale_filter && data.scale_filter.indexOf("'") == -1 ){
                data.scale_filter = "'" + data.scale_filter + "'";
            }

            query = "SELECT tr.grn as id_ter, ";

            if (!info_indic.champ_divider) {
                query = query +
                    "SUM(" + info_indic.champ_p_associe + ") as value ";
            }
            else {
                query = query +
                    "SUM(" + info_indic.champ_p_associe + ") as value, " +
                    "SUM( CASE WHEN " + info_indic.champ_p_associe+ " is not null then "+ info_indic.champ_divider + " else 0 end) as value_d ";
            }

            query = query +
                "FROM siterre." + info_indic.tbl + " as bd ";


            var table_ter_imb = "ter_cr_2015" ;
            if(info_tbl.crrsdc_ter == 'ter_01' || info_tbl.crrsdc_ter == 'ter_02' || data.granularity.substr(0,6) == 'ter_01' || data.granularity.substr(0,6) == 'ter_02'  || data.scale.substr(0,6) == 'ter_01' || data.scale.substr(0,6) == 'ter_02'){
                table_ter_imb = "ter_cr_2015_full";

            }
            else {
                table_ter_imb = "ter_cr_2015";
            }


            if (data.scale_filter){

                query = query + "" +
                    "LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+ data.granularity +" as grn, "+data.scale +" as scl " +
                    "FROM siterre."+table_ter_imb+" " +
                    "WHERE " + data.scale +" in ("+ data.scale_filter +") " +
                    "AND ter_" + payload.scale_type_ter_int + "_2015" + " in (" + payload.scale_id_ter +") " +
                    "GROUP BY correspondance, grn, scl" +
                    ") as tr ";
            }
            else {
                query = query + "" +
                    "LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+ data.granularity +" as grn " +
                    "FROM siterre."+table_ter_imb+" " +
                    "WHERE ter_" + payload.scale_type_ter_int + "_2015" + " in (" + payload.scale_id_ter +") " +
                    "GROUP BY correspondance, grn" +
                    ") as tr ";
            }

            query = query +
                "ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";

            if (data.scale_filter){
                query = query + "WHERE tr.scl in (" + data.scale_filter +") "
            }


            if (info_tbl.multi_year == 1 && data.year) {

                if (!data.scale_filter){
                    query = query + " WHERE bd." + info_tbl.champ_year + "=" + data.year  + " "
                }
                else {
                    query = query + " AND bd." + info_tbl.champ_year + "=" + data.year  + " "
                }
            }

            query = query + "GROUP BY tr.grn;";

            // console.log(query)

            return db.query(query)

        })
        .then(function(res){

            var toReturn = {};
            for (var i = 0 ; i < res.length; i++){
                if (!info_indic.champ_divider) {
                    toReturn[res[i].id_ter] ={value : parseFloat(res[i].value) *  parseFloat(info_indic.facteur_to_main_unit)};
                }
                else {

                    try{
                        toReturn[res[i].id_ter] ={value : parseFloat(info_indic.facteur_to_main_unit)*parseFloat(res[i].value)/ parseFloat(res[i].value_d)};
                    }
                    catch(err){
                        toReturn[res[i].id_ter] ={value : null};
                    }
                }
            }

            // On converti dans la bonne unité.
            return new Promise.resolve(toReturn);


        })
        .catch(function(err){
            return new Promise.reject(err)
        })

};

// ============ Get single value for one specific terr ================================================================= DEF
ctrl.getStaticIndicatorSingleValue = function (data, payload){


    if (!data.id  || data.id == 'undefined' || data.id === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!data.scale  || data.scale == 'undefined' || data.scale === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!data.scale_init  || data.scale_init == 'undefined' || data.scale_init === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!data.scale_filter_init || data.scale_filter_init == 'undefined' || data.scale_filter_init === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }

    // On ramènes les infos relatives à l'indicateur
    var query ='';
    var info_indic = {};
    var info_tbl = {};

    return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur = $1", [parseInt(data.id)])
        .then(function(res){
            info_indic = res;

            return db.one("SELECT * FROM siterre.blthq_descpr_value_tbl WHERE tbl = $1 AND (year_data = 9999 OR year_data=$2)", [info_indic.tbl,  2015])
        })
        .then(function(res){
            info_tbl = res;

            var table_ter_imb = "ter_cr_2015" ;
            if( data.scale.substr(0,6) == 'ter_01' || data.scale.substr(0,6) == 'ter_02' || data.scale_init.substr(0,6) == 'ter_01' || data.scale_init.substr(0,6) == 'ter_02'  || data.scale_filter_init.substr(0,6) == 'ter_01' || data.scale_filter_init.substr(0,6) == 'ter_02' ){
                table_ter_imb = "ter_cr_2015_full";

            }
            else {
                table_ter_imb = "ter_cr_2015";
            }

            // ON va rercher l'identifiant du territoire que l'on cherche
            query = "SELECT " + data.scale + " as scl FROM siterre."+table_ter_imb+" " +
                "WHERE " + data.scale_init +" in ("+ data.scale_filter_init +") " +
                "AND ter_" + payload.scale_type_ter_int + "_2015" + " in (" + payload.scale_id_ter +") " +
                "GROUP BY scl";

            // console.log(query)
            return db.one(query, [])

        })
        .then(function(res){

            var id_ter = res.scl;

            query = "SELECT ";

            if (!info_indic.champ_divider) {
                query = query +
                    "SUM(" + info_indic.champ_p_associe + ") as value ";
            }
            else {
                query = query +
                    "SUM(" + info_indic.champ_p_associe + ") as value, " +
                    "SUM( CASE WHEN " + info_indic.champ_p_associe + " is not null then "+ info_indic.champ_divider + " else 0 end) as value_d ";
            }

            query = query +
                "FROM siterre." + info_indic.tbl + " as bd ";

            var table_ter_imb = "ter_cr_2015" ;
            if(info_tbl.crrsdc_ter == 'ter_01' || info_tbl.crrsdc_ter == 'ter_02' || data.scale.substr(0,6) == 'ter_01' || data.scale.substr(0,6) == 'ter_02' || payload.scale_type_ter_int == '01' || payload.scale_type_ter_in == '02' ){
                table_ter_imb = "ter_cr_2015_full";

            }
            else {
                table_ter_imb = "ter_cr_2015";
            }

            query = query + "" +
                "LEFT JOIN (" +
                "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+data.scale +" as scl " +
                "FROM siterre." +table_ter_imb+ " " +
                "WHERE " + data.scale +" = '"+ id_ter +"' " +
                "AND ter_" + payload.scale_type_ter_int + "_2015" + " in (" + payload.scale_id_ter +") " +
                "GROUP BY correspondance, scl" +
                ") as tr ";

            query = query +
                "ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";


            query = query + "WHERE tr.scl in (" + id_ter +") ";

            if (info_tbl.multi_year == 1 && data.year) {
                query = query + " AND bd." + info_tbl.champ_year + "='" + data.year  + "' "
            }
            // console.log(query)
            return db.oneOrNone(query)

        })
        .then(function(res){

            var toReturn = {};

            if (!info_indic.champ_divider) {
                toReturn ={value : parseFloat(res.value) *  parseFloat(info_indic.facteur_to_main_unit)};
            }
            else {
                try{
                    toReturn ={value : parseFloat(info_indic.facteur_to_main_unit)*parseFloat(res.value)/ parseFloat(res.value_d)};
                }
                catch(err){
                    toReturn ={value : null};
                }
            }
            return new Promise.resolve(toReturn);
        })
        .catch(function(err){
            return new Promise.reject(err)
        })
};

// ============ Get moyenne for one specific terr ====================================================================== DEF
ctrl.getStaticIndicatorMoyenne = function (data, payload){


    if (!data.id  || data.id == 'undefined' || data.id === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!data.scale  || data.scale == 'undefined' || data.scale === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!data.scale_init  || data.scale_init == 'undefined' || data.scale_init === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!data.scale_filter_init || data.scale_filter_init == 'undefined' || data.scale_filter_init === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }

    // On ramènes les infos relatives à l'indicateur
    var query ='';
    var info_indic = {};
    var info_tbl = {};
    var nb_ter = 1;
    var id_ter = "";

    return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur = $1", [parseInt(data.id)])
        .then(function(res){
            info_indic = res;

            return db.one("SELECT * FROM siterre.blthq_descpr_value_tbl WHERE tbl = $1 AND (year_data = 9999 OR year_data=$2)", [info_indic.tbl,  2015])
        })
        .then(function(res){
            info_tbl = res;


            var table_ter_imb = "ter_cr_2015";
            if( data.scale_init.substr(0,6) == 'ter_01' || data.scale_init.substr(0,6) == 'ter_02'  || data.scale.substr(0,6) == 'ter_01' || data.scale.substr(0,6) == 'ter_02' ){
                table_ter_imb = "ter_cr_2015_full";

            }
            else {
                table_ter_imb = "ter_cr_2015";
            }

            if (data.scale_filter_init.indexOf("'") == -1 ){
                data.scale_filter_init = "'" + data.scale_filter_init +"'";
            }
            // ON va rercher l'identifiant du territoire que l'on cherche
            query = "SELECT " + data.scale + " as scl FROM siterre."+table_ter_imb+" " +
                "WHERE " + data.scale_init +" in ("+ data.scale_filter_init +") " +
                "AND ter_" + payload.scale_type_ter_int + "_2015" + " in (" + payload.scale_id_ter +") " +
                "GROUP BY scl";

            // console.log(query)
            return db.query(query, [])

        })
        .then(function(res){

            if ( res.constructor == Array) {
                id_ter = "'"+res[0].scl+"'"
                for (var i=1 ; i< res.length ; i++){
                    id_ter = id_ter + ", '"+res[i].scl + "'"
                }
            }
            else{
                id_ter = "'" + res.scl + "'"
            }

            // ON va chercher le nb de territoire correspondant au territoire cherché
            var table_ter_imb = "ter_cr_2015";
            if( data.granularity.substr(0,6) == 'ter_01' || data.granularity.substr(0,6) == 'ter_02' || data.scale.substr(0,6) == 'ter_01' || data.scale.substr(0,6) == 'ter_02' ){
                table_ter_imb = "ter_cr_2015_full";
            }
            else {
                table_ter_imb = "ter_cr_2015" ;
            }

            var subQuery = "(SELECT " + data.granularity + " as gran FROM siterre."+table_ter_imb+" " +
                "WHERE " + data.scale +" in ("+ id_ter +") AND " +  data.granularity + " IS NOT NULL " +
                "AND ter_" + payload.scale_type_ter_int + "_2015" + " in (" + payload.scale_id_ter +") " +
                "GROUP BY gran) as req";

            var mainQuery = "SELECT count(*) as nb FROM " + subQuery;

            // console.log(mainQuery);
            return db.one(mainQuery, [])

        })
        .then(function(res){

            nb_ter = res.nb;
            // console.log(res.nb)

            query = "SELECT ";

            if (!info_indic.champ_divider) {
                query = query +
                    "SUM(" + info_indic.champ_p_associe + ") as value ";
            }
            else {
                query = query +
                    "SUM(" + info_indic.champ_p_associe + ") as value, " +
                    "SUM(case when " + info_indic.champ_p_associe + " is not null " +
                    " then "+ info_indic.champ_divider + " else 0 end ) as value_d ";
            }

            query = query +
                "FROM siterre." + info_indic.tbl + " as bd ";

            var table_ter_imb = "ter_cr_2015" ;
            if(info_tbl.crrsdc_ter == 'ter_01' || info_tbl.crrsdc_ter == 'ter_02' || data.scale.substr(0,6) == 'ter_01' || data.scale.substr(0,6) == 'ter_02' || id_ter.substr(0,6) == 'ter_01' || id_ter.substr(0,6) == 'ter_02' ){
                table_ter_imb = "ter_cr_2015_full";
            }
            else {
                table_ter_imb = "ter_cr_2015";
            }

            query = query + "" +
                "LEFT JOIN (" +
                "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+data.scale +" as scl " +
                "FROM siterre."+table_ter_imb+" " +
                "WHERE " + data.scale +" in ("+ id_ter +") " +
                "AND ter_" + payload.scale_type_ter_int + "_2015" + " in (" + payload.scale_id_ter +") " +
                "GROUP BY correspondance, scl" +
                ") as tr ";

            query = query +
                "ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";


            query = query + "WHERE tr.scl in (" + id_ter +") ";

            if (info_tbl.multi_year == 1 && data.year) {
                query = query + " AND bd." + info_tbl.champ_year + "=" + data.year  + " "
            }
            // console.log(query)
            return db.oneOrNone(query) //ICI bug

        })
        .then(function(res){

            var toReturn = {};

            if (!info_indic.champ_divider) {
                toReturn ={value : parseFloat(res.value) *  parseFloat(info_indic.facteur_to_main_unit)};
            }
            else {
                try{
                    toReturn ={value : parseFloat(info_indic.facteur_to_main_unit)*parseFloat(res.value)/ parseFloat(res.value_d)};
                }
                catch(err){
                    toReturn ={value : null};
                }
            }

            if (!info_indic.champ_divider && toReturn.value) {
                if (parseFloat(nb_ter)> 0){
                    toReturn ={value : toReturn.value/parseFloat(nb_ter)};
                }
                else {
                    toReturn ={value : null};
                }
            }

            return new Promise.resolve(toReturn);
        })
        .catch(function(err){
            return new Promise.reject(err)
        })
};


// ============ Get All datas for one specific terr ====================================================================== DEF
ctrl.getDataSingleTer = function (payload, data, filters) {
/*
     var data = {
     id : 520,
     scale : "ter_50_2015",
     scale_filter_init : "71",
     scale_init : "ter_50_2015",
     year : "2015"

     };
*/


    if (!data.id  || data.id == 'undefined' || data.id === null ) {
        throw new Errors.IllegalArgumentError("Id indicator is required.");
    }
    if (!data.scale  || data.scale == 'undefined' || data.scale === null ) {
        throw new Errors.IllegalArgumentError("Id scale is required.");
    }
    if (!data.scale_init  || data.scale_init == 'undefined' || data.scale_init === null ) {
        throw new Errors.IllegalArgumentError("Id scale_init is required.");
    }
    if (!data.scale_filter_init || data.scale_filter_init == 'undefined' || data.scale_filter_init === null ) {
        throw new Errors.IllegalArgumentError("Id scale_filter_init is required.");
    }

    // On ramènes les infos relatives à l'indicateur
    var query ='';
    var info_indic = {};
    var info_indic_all = {};
    var info_tbl = {};
    var column_to_calculate = []

    return db.one("SELECT tbl FROM siterre.blthq_indicateurs WHERE id_indicateur = $1", [parseInt(data.id)])
        .then(function(res) {
            info_indic = res;


            return db.query("SELECT distinct b.tbl, b.id_indicateur as id, b.champ_p_associe, b.champ_divider, b.facteur_to_main_unit, b.decimal_count, " +
                " b.libelle_indic_short, b.unit_short " +
                "FROM siterre.blthq_indicateurs b  " +
                "LEFT JOIN siterre.admin_groups_indic a on a.id_indicateur=b.id_indicateur " +
               // "LEFT JOIN siterre.blthq_theme_indic c on c.id_indicateur=b.id_indicateur " +
                //"WHERE c.id_theme = $1 and  ( 1 = $2 or a.id_group = $3  )", [data.id_theme, payload.role, payload.group])
                "WHERE b.tbl = $1 and  ( 1 = $2 or a.id_group = $3  )", [info_indic.tbl, payload.role, payload.group])
        })
        .then(function(res){
            info_indic_all = res;
            // Je recupère les colonnes que je vais sommer
            for (var i = 0; i < info_indic_all.length ; i++ ){


                var toPush = {}
                if (info_indic_all[i].champ_p_associe != null && info_indic_all[i].champ_p_associe != undefined  && info_indic_all[i].champ_p_associe != "")
                {
                    toPush={
                            lib  : '"'+info_indic_all[i].id + '_p"',
                            champ :info_indic_all[i].champ_p_associe
                    };
                }
                if ( info_indic_all[i].champ_divider != null && info_indic_all[i].champ_divider != undefined && info_indic_all[i].champ_divider != "")
                {
                    toPush.lib_d = '"'+info_indic_all[i].id + '_d"';
                    toPush.champ_d = info_indic_all[i].champ_divider;
                }

                if (toPush != {} ){
                    column_to_calculate.push(toPush);
                }
            }



            return db.one("SELECT * FROM siterre.blthq_descpr_value_tbl WHERE tbl = $1 AND (year_data = 9999 OR year_data=$2)", [info_indic.tbl,  2015])
        })
        .then(function(res){
            info_tbl = res;

            query = "SELECT ";

            //Generation des sum
            query = query + "SUM( " + column_to_calculate[0].champ+ " ) as " + column_to_calculate[0].lib;
            if(column_to_calculate[0].lib_d != null) {
                query = query + ",SUM( CASE WHEN  " + column_to_calculate[0].champ+ " IS NOT NULL THEN " + column_to_calculate[0].champ_d+ " ELSE 0 END ) as " + column_to_calculate[0].lib_d;
            }

            for (var i = 1; i < column_to_calculate.length ; i++) {
                query = query + ",SUM( " + column_to_calculate[i].champ + " ) as " + column_to_calculate[i].lib;
                if (column_to_calculate[i].lib_d != null) {
                    query = query + ",SUM(CASE WHEN  " + column_to_calculate[i].champ + " IS NOT NULL THEN " + column_to_calculate[i].champ_d + " ELSE 0 END) as " + column_to_calculate[i].lib_d;
                }
            }


            query = query +
                " FROM siterre." + info_indic.tbl + " as bd ";

            var table_ter_imb = "ter_cr_2015" ;
            if(info_tbl.crrsdc_ter == 'ter_01' || info_tbl.crrsdc_ter == 'ter_02' || data.scale.substr(0,6) == 'ter_01' || data.scale.substr(0,6) == 'ter_02' || payload.scale_type_ter_int == '01' || payload.scale_type_ter_in == '02' ){
                table_ter_imb = "ter_cr_2015_full";

            }
            else {
                table_ter_imb = "ter_cr_2015";
            }

            if (data.scale_filter_init.indexOf("'") == -1){
                data.scale_filter_init ="'" + data.scale_filter_init + "'"
            }

            query = query + "" +
                "LEFT JOIN (" +
                "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+data.scale +" as scl " +
                "FROM siterre." +table_ter_imb+ " " +
                "WHERE " + data.scale +" in ("+ data.scale_filter_init +") " +
                "AND ter_" + payload.scale_type_ter_int + "_2015" + " in (" + payload.scale_id_ter +") " +
                "GROUP BY correspondance, scl" +
                ") as tr ";

            query = query +
                "ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";


            query = query + "WHERE tr.scl in (" + data.scale_filter_init +") ";

            if (info_tbl.multi_year == 1 && data.year) {
                query = query + " AND bd." + info_tbl.champ_year + "='" + data.year  + "' "
            }
            // console.log(query)

            if (data.filters != 'undefined') {
                query = query + ' and ' + data.filters;
            }

            return db.oneOrNone(query)

        })
        .then(function(res) {

            for (var i=0; i < info_indic_all.length ; i ++) {

                if (!info_indic_all[i].champ_divider) {
                    info_indic_all[i].value = parseFloat(res[info_indic_all[i].id+'_p']) * parseFloat(info_indic_all[i].facteur_to_main_unit);
                }
                else {
                    try {
                        info_indic_all[i].value =parseFloat(info_indic_all[i].facteur_to_main_unit) * parseFloat(res[info_indic_all[i].id+'_p']) / parseFloat(res[info_indic_all[i].id+'_d']);
                    }
                    catch (err) {
                        info_indic_all[i].value =null;
                    }
                }

            }

            return info_indic_all;
        });

} ;






// #####################################################################################################################
// #####################################################################################################################
// #####################################################################################################################
// #####################################################################################################################
// #####################################################################################################################
