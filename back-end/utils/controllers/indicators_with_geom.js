
"use strict";
var async = require('async');
var Errors = require("../utils/custom-errors");
var Promise = require('promise');
var spex = require('spex')(Promise);
var QueryStream = require('pg-query-stream');
var dbgeo = require('dbgeo');


var ctrl = {};

module.exports = ctrl;


// ============ Get Vectors for self provided vector indicator    ====================================================== DEF
ctrl.getSelfProvidedVectors = function(dataQuery){

    var mainSqlQuery = '';
    var stringSelect = '';
    var stringWhere = '';
    var stringJoinTer = "";
    var stringGroupBy = "";
    var stringFROM = "";
    var stringSelectInfo = "";

    var datas = [];
    var baseSource = '';

    if (!dataQuery.id  || dataQuery.id == 'undefined' || dataQuery.id === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!dataQuery.scale  || dataQuery.scale == 'undefined' || dataQuery.scale === null ) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }
    if (!dataQuery.scale_filter  || dataQuery.scale_filter == 'undefined' || dataQuery.scale_filter === null ) {
        throw new Errors.IllegalArgumentError("scale_filter is required.");
    }


    var idIndicator = parseInt(dataQuery.id);
    var scale = dataQuery.scale;
    var scale_filter = dataQuery.scale_filter;


    var info_indic = {};
    var info_tbl = {};

    var champ_info = [];


    // On va chercher les caractéristiques de l'indicateur
    return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur=$1", [idIndicator])
        .then(function(rslt) {
            info_indic = rslt;
            baseSource = "siterre." +  info_indic.tbl;
            // console.log("info_indic : ok")
            return db.one("SELECT * FROM siterre.blthq_descpr_value_tbl WHERE tbl = $1 AND (year_data = 9999 OR year_data=$2)", [info_indic.tbl, 2015])
        })
        .then(function(res){

            // console.log("info_tbl : ok");
            info_tbl = res;
            champ_info = JSON.parse(info_indic.champ_info);
            // console.log(champ_info)

            // console.log(info_indic)

            for (var param in champ_info){
                stringSelectInfo = stringSelectInfo + " bd." + champ_info[param] + " as " + param + " , ";
            }

            stringSelect = stringSelectInfo + " bd." + info_indic.vector_id_field +" as id_ter, ST_AsGeoJSON(bd." + info_indic.vector_field + ") as geom ";
            stringFROM = " FROM siterre." + info_indic.tbl + " as bd ";

            var table_ter_imb = "ter_cr_2015" ;
            if(info_tbl.crrsdc_ter == 'ter_01' || info_tbl.crrsdc_ter == 'ter_02' || scale.substr(0,6) == 'ter_01' || scale.substr(0,6) == 'ter_02' ){
                table_ter_imb = "ter_cr_2015_full";
                console.log(scale.substr(0,6));
            }
            else {
                table_ter_imb = "ter_cr_2015";
            }

            if (scale_filter){
                stringJoinTer =
                    "LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+scale +" as scl " +
                    "FROM siterre."+table_ter_imb+" " +
                    "WHERE " + scale +" in ("+ scale_filter +") " +
                    "GROUP BY correspondance, scl " +
                    ") as tr ";
            }
            else {
                stringJoinTer =
                    "LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance " +
                    "FROM siterre."+table_ter_imb+" " +
                    "GROUP BY correspondance " +
                    ") as tr ";
            }

            stringJoinTer = stringJoinTer +
                "ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";

            if (scale_filter){
                stringWhere = "WHERE tr.scl in ( " + scale_filter +" ) "
            }

            if (info_tbl.multi_year == 1 && year) {
                if (stringWhere !== ''){
                    stringWhere = stringWhere + " AND bd." + info_tbl.champ_year + "=" + year  + " "
                }
                else {
                    stringWhere = " WHERE bd." + info_tbl.champ_year + "=" + year  + " "
                }
            }

            stringGroupBy = " ";

            return new Promise.resolve();
        })
        .then(function(rslt){

            mainSqlQuery = "SELECT " + stringSelect + stringFROM + stringJoinTer + stringWhere + stringGroupBy;

            // console.log(mainSqlQuery);
            var qs = new QueryStream(mainSqlQuery, [], { batchSize: 10 });
            return db.stream(qs, function (s) {
                return spex.stream.read(s, function (index, rows) {
                    // Executed for each row (or by batch of rows)
                    for (var i = 0; i < rows.length; i++) {
                        datas.push(rows[i]);
                    }
                })
                    .catch(function(err) {
                        console.log(err)
                    });
            })
        })
        .then(function(){
            return parseToProperGeoJSON(datas);
        })
        .catch(function(err){
            return new Promise.reject(err);
        })


};

// ============ Get values for self provided vector indicator    ======================================================= DEF
ctrl.getStaticIndicValues = function(dataQuery){

    var mainSqlQuery = '';
    var stringSelect = '';
    var stringWhere = '';
    var stringJoinTer = "";
    var stringGroupBy = "";
    var stringFROM = "";

    var datas = [];
    var baseSource = '';

    if (!dataQuery.granularity  || dataQuery.granularity == 'undefined' || dataQuery.granularity === null ) {
        throw new Errors.IllegalArgumentError("Id dndicator is required.");
    }
    if (!dataQuery.scale  || dataQuery.scale == 'undefined' || dataQuery.scale === null ) {
        throw new Errors.IllegalArgumentError("scale is required.");
    }
    if (!dataQuery.year  || dataQuery.year == 'undefined' || dataQuery.year === null ) {
        throw new Errors.IllegalArgumentError("scale_filter is required.");
    }

    var idIndicator = parseInt(dataQuery.id);
    var scale = dataQuery.scale;
    var scale_filter = dataQuery.scale_filter;
    var year = dataQuery.year;

    var info_indic = {};
    var info_tbl = {};



    // On va chercher les caractéristiques de l'indicateur
    return db.one("SELECT * FROM siterre.blthq_indicateurs WHERE id_indicateur=$1", [idIndicator])
        .then(function(rslt) {
            info_indic = rslt;
            baseSource = "siterre." +  info_indic.tbl;
            // console.log("info_indic : ok")

            return db.one("SELECT * FROM siterre.blthq_descpr_value_tbl WHERE tbl = $1 AND (year_data = 9999 OR year_data=$2)", [info_indic.tbl, year])
        })
        .then(function(res){

            // console.log("info_tbl : ok")

            info_tbl = res;

            stringSelect = " bd." + info_indic.vector_id_field + " as id_ter, SUM(bd."+info_indic.champ_p_associe+") as value ";

            stringFROM = " FROM siterre." + info_indic.tbl + " as bd ";

            var table_ter_imb = "ter_cr_"+year  ;
            if(info_tbl.crrsdc_ter == 'ter_01' || info_tbl.crrsdc_ter == 'ter_02' || scale.substr(0,6) == 'ter_01' || scale.substr(0,6) == 'ter_02' || scale_filter.substr(0,6) == 'ter_01' || scale_filter.substr(0,6) == 'ter_02' ){
                table_ter_imb = "ter_cr_" + year+"_full";
                console.log(scale.substr(0,6));
            }
            else {
                table_ter_imb = "ter_cr_" + year;
            }

            if (scale_filter){
                stringJoinTer =
                    "LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance, "+scale +" as scl " +
                    "FROM siterre."+table_ter_imb + " " +
                    "WHERE " + scale +" in ("+ scale_filter +") " +
                    "GROUP BY correspondance, scl " +
                    ") as tr ";
            }
            else {
                stringJoinTer =
                    "LEFT JOIN (" +
                    "SELECT " +info_tbl.crrsdc_ter + "_" +  info_tbl.crrsdc_ter_year_geo +" as correspondance " +
                    "FROM siterre."+table_ter_imb + " " +
                    "GROUP BY correspondance " +
                    ") as tr ";
            }

            stringJoinTer = stringJoinTer +
                "ON bd." + info_tbl.champ_ter  +"= tr.correspondance ";

            if (scale_filter){
                stringWhere = "WHERE tr.scl in (" + scale_filter +") "
            }

            if (info_tbl.multi_year == 1 && year) {
                if (stringWhere !== ''){
                    stringWhere = stringWhere + " AND bd." + info_tbl.champ_year + "=" + year  + " "
                }
                else {
                    stringWhere = " WHERE bd." + info_tbl.champ_year + "=" + year  + " "
                }
            }

            stringGroupBy = " GROUP BY id_ter ";

            return new Promise.resolve()
        })
        .then(function(rslt){
            mainSqlQuery = "SELECT " + stringSelect + stringFROM + stringJoinTer + stringWhere + stringGroupBy;

            // console.log(mainSqlQuery);
            var qs = new QueryStream(mainSqlQuery, [], { batchSize: 10 });
            return db.stream(qs, function (s) {
                return spex.stream.read(s, function (index, rows) {
                    // Executed for each row (or by batch of rows)
                    for (var i = 0; i < rows.length; i++) {
                        datas.push(rows[i])
                    }
                })
                    .catch(function(err) {
                        console.log(err)
                    });
            })
        })
        .then(function(){
            var resTosend = {};
            for (var i = 0 ; i < datas.length ; i++ ) {
                try{
                    resTosend[datas[i].id_ter] = {value : parseFloat(datas[i].value)};
                }
                catch(err){
                    resTosend[datas[i].id_ter] = {value : null};
                }
            }
            return new Promise.resolve(resTosend);
        })
        .catch(function(err){
            return new Promise.reject(err);
        })

};

// #####################################################################################################################
// #####################################################################################################################
// #####################################################################################################################
// #####################################################################################################################
// #####################################################################################################################

// ======= Make clean GeoJSON out of postgis query =====================================================================
var parseToProperGeoJSON = function(data){
    var doIt = function(fullfill, reject){
        dbgeo.parse(data, {
            outputFormat: 'geojson',
            geometryType: 'geojson',
            geometryColumn: 'geom'
        }, function(error, result) {
            // This will log a valid GeoJSON FeatureCollection
            if (error){
                reject(error)
            }
            fullfill(result)
        });
    };

    return new Promise(doIt)
};