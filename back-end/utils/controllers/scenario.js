/**
 * Created by Johan Schram - KOSMEO ANALITICS - https://kosmeo-a.com on 10/07/2017.
 */


"use strict";
var async = require('async');
var Errors = require("../utils/custom-errors");
var utils = require("../utils/express-utils");

var Promise = require('promise');
var spex = require('spex')(Promise);
var QueryStream = require('pg-query-stream');

var ctrl = {};

module.exports = ctrl;

// ============ Get Scenario list    =================================================================================== DEF
ctrl.getListScenarios = function(user){

    // Récupèration des scénarios de l'utilisateurs + scénario du group et lecture et lecture écriture
    if (user.role === 1 ){
        return db.query("SELECT " +
            "s.id as id, " +
            "s.name as name, " +
            "s.date_creation as date_creation, " +
            "s.date_last_modif as date_last_modif, " +
            "s.user_crea as created_by, " +
            "u.surname || ' ' || u.name as created_by_lib, " +
            "u2.surname || ' ' || u2.name as last_modif_by_lib, " +
            "s.user_mod as last_modif_by, " +
            "s.type as type " +
            "FROM siterre.scn_gestion as s " +
            "LEFT JOIN  siterre.admin_users as u on u.id = s.user_crea " +
            "LEFT JOIN siterre.admin_users as u2 on u2.id = s.user_mod ", [])
    }
    else{
        return db.query("SELECT " +
            "s.id as id, " +
            "s.name as name, " +
            "s.date_creation as date_creation, " +
            "s.date_last_modif as date_last_modif, " +
            "s.user_crea as created_by, " +
            "u.surname || ' ' || u.name as created_by_lib, " +
            "u2.surname || ' ' || u2.name as last_modif_by_lib, " +
            "s.user_mod as last_modif_by, " +
            "s.type as type " +
            "FROM siterre.scn_gestion as s " +
            "LEFT JOIN  siterre.admin_users as u on u.id = s.user_crea " +
            "LEFT JOIN siterre.admin_users as u2 on u2.id = s.user_mod " +
            "WHERE u.groups = $2 AND (s.user_crea = $1 OR s.TYPE=2 OR s.TYPE=3);   ", [user.id, user.group])
    }


};

// ============ Get Scenario =========================================================================================== DEF
ctrl.getScenario = function(user, data){

    if(!data.id || data.id == 'undefined' || !(parseInt(data.id) > 0) ){
        throw new Errors.IllegalArgumentError("Id is required.");
    }
    var id_scn = parseInt(data.id);

    var dataScn = {
        id : id_scn,
        data : {
            filtre : [],
            raster : 0,
            indicateurs : [],
            territorry : {},
            vecteurs : {}
        }
    };

    // Vérification de l'accès en lecture au scenario.
    return verificationDroitLectureScenario(user, id_scn)
        .then(function(){

           var a  = function(){
               return getDataFiltre(id_scn)
                    .then(function(res){
                        dataScn.data.filtre = res;
                        return new Promise.resolve();
                    })
                    .catch(function(){
                        return new Promise.reject();
                    })
           };
            var b  = function(){
               return getDataIndicateurs(id_scn)
                   .then(function(res){
                       dataScn.data.indicateurs = res;
                       return new Promise.resolve();
                   })
                   .catch(function(err){
                       return new Promise.reject();
                   })
            };
            var c  = function(){
               return getDataTer(id_scn)
                   .then(function(res){
                       // console.log(res);
                       dataScn.data.territorry = res;
                       return new Promise.resolve();
                   })
                   .catch(function(err){
                       return new Promise.reject();
                   })
           };
            var d  = function(){
               return getDataVecteur(id_scn)
                   .then(function(res){
                       dataScn.data.vecteurs = res;
                       return new Promise.resolve();
                   })
                   .catch(function(err){
                       return new Promise.reject();
                   })
           };
            var e  = function(){
               return getDataRaster(id_scn)
                   .then(function(res){
                       dataScn.data.raster = res;
                       return new Promise.resolve();
                   })
                   .catch(function(err){
                       return new Promise.reject();
                   })
           };

            return Promise.all([a(),b(), c(), d(), e()])
        })
        .then(function(){
            return new Promise.resolve(dataScn)
        })
        .catch(function(err){
            return new Promise.reject(err)
        })

};

// ============ Get Scenario list    =================================================================================== DEF
ctrl.editScenario = function(user, data){


    if(!data.name || data.name == 'undefined' || data.name == ''){
        throw new Errors.IllegalArgumentError("Name is required.");

    }
    if(!data.type || data.type == 'undefined' ||  (data.type !== 1 &&  data.type !== 2 && data.type !== 3)) {
        throw new Errors.IllegalArgumentError("Type is required.");
    }

    if(!data.id || data.id == 'undefined' || !(data.id > 0) ){
        throw new Errors.IllegalArgumentError("Id is required.");
    }

    return verificationDroitEcritureScenario(user, data.id)
        .then(function(){
            return db.result("UPDATE siterre.scn_gestion set name=$1, type=$2, date_last_modif=current_timestamp, user_mod=$3 WHERE id=$4", [data.name, data.type, user.id, data.id])
        })
        .catch(function(err){
            if (err && err.code){
                return new Promise.reject(err);
            }
            else{
                return new Promise.reject(err);
            }
        })

};

// ============ Delete Scenario     =================================================================================== DEF
ctrl.deleteScenario = function(user, data){

    if(!data.id || data.id == 'undefined' || !(parseInt(data.id) > 0) ){
        throw new Errors.IllegalArgumentError("Id is required.");
    }
    var id_scn = parseInt(data.id);

    return verificationDroitEcritureScenario(user, id_scn)
        .then(function(){
            return deleteAllDataFromScn(id_scn, true)
        })
        .catch(function(err){
            if (err && err.code){
                return new Promise.reject(err);
            }
            else{
                return new Promise.reject(err);
            }
        })

};

// ============ save Scenario  as  ===================================================================================== DEF
ctrl.saveScenarioAs = function(user, data){

    if(!data.name || data.name == 'undefined' || data.name == ''){
        throw new Errors.IllegalArgumentError("Name is required.");

    }
    if(!data.type || data.type == 'undefined' ||  (data.type !== 1 &&  data.type !== 2 && data.type !== 3)) {
        throw new Errors.IllegalArgumentError("Type is required.");
    }

    var id_scn = 0;

    return db.one("INSERT INTO siterre.scn_gestion (name, type, date_creation, date_last_modif, user_crea, user_mod) " +
        "VALUES ($1, $2, current_timestamp, current_timestamp, $3, $3) RETURNING id", [data.name, data.type, user.id ])
        .then(function(res){
            id_scn = parseInt(res.id);

            return Promise.all([
                insertDataFiltre(id_scn, data.data.filtre),
                insertDataIndicateurs(id_scn, data.data.indicateurs),
                insertDataVecteurs(id_scn, data.data.vecteurs),
                insertDataRaster(id_scn, data.data.raster),
                insertDataTer(id_scn, data.data.territorry)
            ])
        })
        .then(function(res){
            return new Promise.resolve({id : id_scn, name : data.name})
        })
        .catch(function(err){
            console.log(err);

            if (id_scn> 0 ){
                return deleteAllDataFromScn(id_scn, true)
                    .then(function(res){
                        return new Promise.reject()
                    })
                    .catch(function(err){
                        return new Promise.reject()
                    })
            }
            else {
                return new Promise.reject()
            }
        });
};

// ============ save existing scenario    ============================================================================== DEF
ctrl.saveScenario = function(user, data){

    if(!data.id || data.id == 'undefined' || !(parseInt(data.id) > 0) ){
        throw new Errors.IllegalArgumentError("Id is required.");
    }
    var id_scn = parseInt(data.id);

    return verificationDroitEcritureScenario(user, id_scn)
        .then(function(){
            return db.result("UPDATE siterre.scn_gestion SET date_last_modif = current_timestamp, user_mod = $1 where id = $2", [ user.id, id_scn ])
        })
        .then(function(){
            return deleteAllDataFromScn(id_scn, false)
        })
        .then(function(){
            return Promise.all([
                insertDataFiltre(id_scn, data.data.filtre),
                insertDataIndicateurs(id_scn, data.data.indicateurs),
                insertDataVecteurs(id_scn, data.data.vecteurs),
                insertDataRaster(id_scn, data.data.raster),
                insertDataTer(id_scn, data.data.territorry)
            ])
        })
        .catch(function(err){
            if (id_scn> 0 ){
                deleteAllDataFromScn(id_scn, false)
            }
            if (err && err.code){
                return new Promise.reject(err);
            }
            else{
                return new Promise.reject(err);
            }
        })
};

// ============= SCENARIO  = > DATA FILTER ============================================================================= DEF
var insertDataFiltre = function(id_scn, data){
    var insert = function(resolve, reject){
        async.each(data,
            function (item, callback) {
                db.query("INSERT INTO siterre.scn_filtres (id_scn, id_crit, id_dter) VALUES ($1, $2, $3)",
                    [id_scn, item.id_critere, item.id_dter])
                    .then(function (rows) {
                        callback();
                    })
                    .catch(callback);
            },
            function (tasksError) {
                // Function called at first error or after the success of all tasks
                if (tasksError) {
                    { return reject(tasksError); }
                }
                // Here, everything is done
                return resolve();
            });
    };

    return new Promise(insert)

};

var deleteFromDataFiltre = function(id_scn){
    return db.result("delete FROM siterre.scn_filtres where id_scn = $1 ", [id_scn])
};

var getDataFiltre = function(id_scn){

    var results = [];
    return db.query("select * FROM siterre.scn_filtres where id_scn = $1 ", [id_scn])
        .then(function(res){
            for (var i = 0 ; i < res.length; i++){
                results.push({
                    id_critere : parseInt(res[i].id_crit),
                    id_dter : parseInt(res[i].id_dter)
                })
            }

            return new Promise.resolve(results)

        })
        .catch(function(err){
            console.log(err);
            return new Promise.reject()
        })


};

// ============= SCENARIO  = > DATA INDICATEUR ========================================================================= DEF
var insertDataIndicateurs = function(id_scn, data){
    var insert = function(resolve, reject){
        async.each(data,
            function (item, callback) {
                db.query("INSERT INTO siterre.scn_indicateurs (id_scn, id_indicateur, colorid, opacity, nbclasses, methodedistributionid, form) VALUES ($1, $2, $3, $4, $5, $6, $7)",
                    [id_scn, item.id_indicateur, item.colorId, item.opacity, item.nbclasses, item.methodeDistributionId, item.form])
                    .then(function (rows) {
                        callback();
                    })
                    .catch(callback);
            },
            function (tasksError) {
                // Function called at first error or after the success of all tasks
                if (tasksError) {
                    { return reject(tasksError); }
                }
                // Here, everything is done
                return resolve();
            });
    };

    return new Promise(insert)

};

var deleteFromDataIndicateurs = function(id_scn){
    return db.result("delete FROM siterre.scn_indicateurs where id_scn = $1 ", [id_scn])
};

var getDataIndicateurs = function(id_scn){
    var results = [];
    return db.query("select s.*, i.static_dynamic as static_dynamic FROM siterre.scn_indicateurs as s LEFT JOIN siterre.blthq_indicateurs as i on i.id_indicateur = s.id_indicateur where id_scn = $1", [id_scn])
        .then(function(res){
            for (var i = 0 ; i < res.length; i++){
                results.push({
                    id_indicateur : parseInt(res[i].id_indicateur),
                    colorId : parseInt(res[i].colorid),
                    opacity : parseFloat(res[i].opacity),
                    nbclasses : parseInt(res[i].nbclasses),
                    methodeDistributionId :  res[i].methodedistributionid,
                    static_dynamic :  res[i].static_dynamic,
                    form : res[i].form
                })
            }

            return new Promise.resolve(results)

        })
        .catch(function(err){
            console.log(err);
            return new Promise.reject()
        })

};

// ============= SCENARIO  = > DATA INDICATEUR ========================================================================= DEF
var insertDataVecteurs = function(id_scn, data){

    var insert = function(resolve, reject){
        async.each(data,
            function (item, callback) {
                db.query("INSERT INTO siterre.scn_vectors (id_scn, id_vector) VALUES ($1, $2)", [id_scn, item])
                    .then(function (rows) {
                        callback();
                    })
                    .catch(callback);
            },
            function (tasksError) {
                // Function called at first error or after the success of all tasks
                if (tasksError) {
                    { return reject(tasksError); }
                }
                // Here, everything is done
                return resolve();
            });
    };

    return new Promise(insert)

};

var deleteFromDataVecteurs = function(id_scn){
    return db.result("delete FROM siterre.scn_vectors where id_scn = $1 ", [id_scn])
};

var getDataVecteur = function(id_scn){
    var results = [];
    return db.query("select id_vector FROM siterre.scn_vectors where id_scn = $1 ", [id_scn])
        .then(function(res){
            for (var i = 0 ; i < res.length; i++){
                results.push(parseInt(res[i].id_vector))
            }
            return new Promise.resolve(results)

        })
        .catch(function(err){
            console.log(err);
            return new Promise.reject()
        })

};

// ============= SCENARIO  = > DATA TER================================================================================= DEF
var insertDataTer = function(id_scn, data){
    return db.result("INSERT INTO siterre.scn_ter " +
        "(id_scn, " +
        "ter_scale_id_ter, " +
        "ter_scale_type_ter, " +
        "ter_scale_year, " +
        "ter_gran_type_ter, " +
        "ter_gran_year) " +
        "VALUES ($1, $2, $3, $4, $5, $6)", [id_scn, data.scale.id_ter, data.scale.type_ter,data.scale.year, data.granularity.type_ter, data.granularity.year])
};

var deleteFromDataTer = function(id_scn){
    return db.result("delete FROM siterre.scn_ter where id_scn = $1 ", [id_scn])
};

var getDataTer = function(id_scn){

    var results = {
        scale : {
            id_ter : '',
                type_ter : '',
                year : null
        },
        granularity : {
            type_ter : '',
                year : null
        }
    };

    return db.one("select * FROM siterre.scn_ter where id_scn = $1 ", [id_scn])
        .then(function(res){

            results.scale.id_ter = res.ter_scale_id_ter;
            results.scale.type_ter = res.ter_scale_type_ter;
            results.scale.year = parseInt(res.ter_scale_year);
            results.granularity.type_ter = res.ter_gran_type_ter;
            results.scale.year = parseInt(res.ter_gran_year);

            return new Promise.resolve(results)
        })
        .catch(function(err){
            console.log(err);
            return new Promise.reject()
        })


};

// ============= SCENARIO  = > DATA RASTER============================================================================== DEF
var insertDataRaster = function(id_scn, raster){
    return db.result("INSERT INTO siterre.scn_rast " +
        "(id_scn, " +
        "raster) " +
        "VALUES ($1, $2)", [id_scn, raster])
};

var deleteFromDataRaster = function(id_scn){
    return db.result("delete FROM siterre.scn_rast where id_scn = $1 ", [id_scn])
};

var getDataRaster = function(id_scn){
    var results = 0;
    return db.one("select * FROM siterre.scn_rast where id_scn = $1 ", [id_scn])
        .then(function(res){
            results = parseInt(res.raster);
            return new Promise.resolve(results)
        })
        .catch(function(err){
            console.log(err);
            return new Promise.reject()
        })
};

// ============= SCENARIO  = > GESTION ================================================================================= DEF
var deleteFromScnGestion = function(id_scn){
    return db.result("delete FROM siterre.scn_gestion where id = $1 ", [id_scn])
};

// ============= SCENARIO  = > DELETE ALL DATA =========================================================================  DEF
var deleteAllDataFromScn = function(id_scn, all){
    if (all) {
        return Promise.all([
            deleteFromScnGestion(id_scn),
            deleteFromDataRaster(id_scn),
            deleteFromDataTer(id_scn),
            deleteFromDataVecteurs(id_scn),
            deleteFromDataIndicateurs(id_scn),
            deleteFromDataFiltre(id_scn)
        ])
    }
    else {
        return Promise.all([
            deleteFromDataRaster(id_scn),
            deleteFromDataTer(id_scn),
            deleteFromDataVecteurs(id_scn),
            deleteFromDataIndicateurs(id_scn),
            deleteFromDataFiltre(id_scn)
        ])
    }
};

// ==== Verification du droit d'écriture sur un scénario ===============================================================  DEF
var verificationDroitEcritureScenario = function (user, id_scn) {

    if (user.role === 1 ){
        return new Promise.resolve()
    }
    else {
        return db.one("SELECT s.id FROM siterre.scn_gestion as s LEFT JOIN siterre.admin_users as u on u.id = s.user_crea WHERE u.groups = $2 AND s.id = $3 AND (s.user_crea = $1 OR s.TYPE=3)", [user.id, user.group, id_scn])
            .then(function(res){
                return new Promise.resolve();
            })
            .catch(function(err){
                throw new Errors.IllegalArgumentError("Droits insuffisants");
            })
    }
};

// ==== Verification du droit de lecture sur un scénario ===============================================================  DEF
var verificationDroitLectureScenario = function (user, id_scn) {

    if (user.role === 1 ){
        return new Promise.resolve()
    }
    else {
        return db.one("SELECT s.id FROM siterre.scn_gestion as s LEFT JOIN siterre.admin_users as u on u.id = s.user_crea WHERE u.groups = $2 AND s.id = $3 AND (s.user_crea = $1 OR s.TYPE=3 OR s.TYPE=2)", [user.id, user.group, id_scn])
            .then(function(res){
                return new Promise.resolve();
            })
            .catch(function(err){
                throw new Errors.IllegalArgumentError("Droits insuffisants");
            })
    }
};

