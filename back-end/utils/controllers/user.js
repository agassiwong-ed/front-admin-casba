/**
 * Created by Johan Schram - KOSMEO ANALITICS - https://kosmeo-a.com on 01/05/2017.
 */


"use strict";
var async = require('async');
var Errors = require("../utils/custom-errors");
var Promise = require('promise');
var spex = require('spex')(Promise);
var QueryStream = require('pg-query-stream');

var ctrl = {};

module.exports = ctrl;

// ============ Get vector    ========================================================================================== DEF
ctrl.getUserPref = function(payload) {
    var res  = {};
    if (payload.role != 1 ) {
        res  = {
            gran_int : payload.gran_int,
            granularity : payload.granularity,
            scale_id_ter: payload.scale_id_ter,
            scale_type_ter: payload.scale_type_ter,
            scale_type_ter_int: payload.scale_type_ter_int
        };
    }
    else {
        res  = {
            gran_int : 0,
            granularity : "ter_00",
            scale_id_ter:"1",
            scale_type_ter:"ter_99",
            scale_type_ter_int:99
        }; //USELESS?
    }
    return new Promise.resolve(payload)
};

// ============ Get group users    ========================================================================================== DEF
ctrl.getUsers = function(group_id) {
    var  users_query  = "select distinct "
        +" usr.id as id, usr.login, usr.name, usr.surname, usr.mail, usr.role, grp.id as group_id, grp.name as group_name, usr.d_last_con as lastcon, COALESCE(usr.d_nb_con,0)::integer as d_nb_con "
        +" from siterre.admin_groups grp "
        +" Join siterre.admin_users usr on usr.groups=grp.id ";

    if( group_id !== null  ) {
        users_query += " where grp.id = " + group_id;
    }
    users_query += " ORDER BY grp.id; ";

    return db.query(users_query);
};

// ============ Get  users    ========================================================================================== DEF
ctrl.getUserInfo = function(id_group, id_user) {

    if (!id_user  || id_user  == 'undefined' || id_user  === null ) {
        throw new Errors.IllegalArgumentError("Id user is required.");
    }

    var  user_query  = "select * from  siterre.admin_users  where id ="+id_user

    if( id_group !== null  ) {
        user_query += " and groups = " + id_group;
    }

    return db.one(user_query);
};

// ============ delete  users    ========================================================================================== DEF
ctrl.deleteUser = function(payload, user_d) {
    if (!user_d.id_user  || user_d.id_user  == 'undefined' || user_d.id_user  === null ) {
        throw new Errors.IllegalArgumentError("Id user is required.");
    }

    var sql = "DELETE FROM siterre.admin_users  where id=" + user_d.id_user + " and  role >="+payload.role;

    if(payload.group != null) {
        sql+= "and groups = "+ payload.group;
    }

    return db.query(sql)
};

// ============ update  users    ========================================================================================== DEF
ctrl.updateUser = function(payload, user) {
    if (payload.group != null && payload.role >= 3 ) {
        throw new Errors.IllegalArgumentError("Insufficient privileges");
    }
    if (!user  || user  == 'undefined' || user  === null ) {
        throw new Errors.IllegalArgumentError("user is required.");
    }
    var sql = "UPDATE siterre.admin_users SET ";

    //Modif userInfo
    if (user.login)   {sql+= "  login='"    + user.login    +"'"}
    if (user.name)    {sql+= ", name='"     + user.name     +"'"}
    if (user.surname) {sql+= ", surname='"  + user.surname  +"'"}
    if (user.mail)    {sql+= ", mail='"     + user.mail     +"'"}
    if (user.password){sql+= ", password='" + user.password +"'"}

    //Modification role/group
    if (user.group) {sql += " groups='" + user.group.id+"'"}
    if (user.role)  {sql += ", role='"  + user.role+"'" }

    sql+= "    WHERE id = " + user.id;

    return db.query(sql);
};

// ============ new  users    ========================================================================================== DEF
ctrl.newUser = function(payload, user) {
    if (payload.group != null && payload.role >= 3 ) {
        throw new Errors.IllegalArgumentError("Insufficient privileges");
    }
    if (!user  || user  == 'undefined' || user  === null ) {
        throw new Errors.IllegalArgumentError("user is required.");
    }
    user.name = user.name.replace(/'/g, "''");
    user.surname = user.surname.replace(/'/g, "''");

    var sql = " INSERT INTO siterre.admin_users "
        +" ( login, name, surname, mail, password, role, groups, d_created) "
        +" VALUES ('"+user.login+"', '"+user.name+"', '"+user.surname+"', '"+user.mail+"', '"+user.password+"', "+user.role+","
        +  user.group.id+", NOW() ); ";

    return db.query(sql);
};

