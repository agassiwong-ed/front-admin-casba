﻿"use strict";

var Errors = require("./custom-errors");
var _und = require("underscore");

var utils = {};
module.exports = utils;

utils.safeErrorHandler = function (err, req, res, next) {
    /* We log the error internaly */
    console.log(err);
    
    var error = { message: err.message };
    
    // We don't want users to see this at the production env
    if (req.app.get('env') == 'development') {
        error.stack = err.stack;
    }
    
    for (var prop in err) error[prop] = err[prop];
    
    /* Finaly respond to the request */
    if (err instanceof Errors.ClientError) {
        return res.status(err.code || 500)
            .json(error);
    } else {
        return res.status(err.status || 500)
                  .json(error);
    }
};

utils.safeHandleError = function (req, res, next) {
    return function (err) {
        var error = err && err instanceof Error ?  err : new Error(err);

        if (!next || !_und.isFunction(next)) {
            console.log("Next not defined so return error: " + err);
            utils.safeErrorHandler(error, req, res, next);
        }
       else {
            next(error);
        }
    };
};
