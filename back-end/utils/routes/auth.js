"use strict";

var utils = require("../utils/express-utils");
var Errors = require("../utils/custom-errors");

var Promise = require('promise');
var express = require('express');
var app = express.Router();
module.exports = app;

var auth = require('../controllers/auth');


// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.post('/signin', function(req, res, next) {

    auth.signIn(req.body)
        .then(function (results) {
            // Handle resuls lists
            res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});