"use strict";

var utils = require("../utils/express-utils");
var Errors = require("../utils/custom-errors");

var Promise = require('promise');
var express = require('express');
var app = express.Router();
module.exports = app;

var bibliothequeCtrl = require('../controllers/bibliotheque');
var auth = require('../controllers/auth');

//######################################################################################################################
//######################################################################################################################
//######################################################################################################################

// ----------- get catalogue bases -------------------------------------------------------------------------------  DEF
app.get('/base/infos', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.getBases(payload)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});
// ----------- post  base -------------------------------------------------------------------------------  DEF
app.post('/base/post', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.postBase(payload, req.body)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});



// ----------- get catalogue Indicateurs -------------------------------------------------------------------------------  DEF
app.get('/indicator/library', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.getBibliothequeIndicator(payload, req.query)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ----------- get info for single indic -------------------------------------------------------------------------------  DEF
app.get('/indicator/info/:id_indicator', function(req, res, next) {

    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.getInfoOnIndicator(payload, req.params.id_indicator)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- get indic ordered by theme ------------------------------------------------------------------------------  DEF
app.get('/indicator/librarybytheme', function(req, res, next) {

    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.getArchitechtureThemeIndicator(payload)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- save parameters ------------------------------------------------------------------------------  DEF
app.get('/indicator/saveDefaultParameters', function(req, res, next) {

    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.saveDefaultParameters(payload, req.query)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- save parameters ------------------------------------------------------------------------------  DEF
app.get('/indicator/saveColorParameters', function(req, res, next) {

    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.saveColorParameters(payload, req.query)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- save new indicator---------------------------------------------------------------------------  DEF
app.post('/indicator/post', function(req, res, next) {

    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.postIndicator(payload, req.body)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- delete indic -----------------------------------------------------------------  DEF
app.delete('/indicator/delete', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.deleteIndicator(payload, req.query)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- get catalogue Indicateurs -------------------------------------------------------------------------------  DEF
app.get('/indicator/reference', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.getBibliothequeIndicatorReference(payload)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.put('/indicator/update',  function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.updateIndicator(payload, req.body)
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});


// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.put('/indicator/theme',  function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.updateIndicatorTheme(payload, req.body)
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.post('/theme/post',  function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.addTheme(payload, req.body)
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.post('/sstheme/post',  function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.addSsTheme(payload, req.body)
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.get('/sstheme/get',  function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.getSsTheme(payload, req.body)
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});


// ----------- get catalogue Filters -------------------------------------------------------------------------------  DEF
app.get('/filter/library', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.getBibliothequeFilter(payload)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});



// ----------- get filter info  ------------------------------------------------------------------------------------  DEF
app.get('/filter/infos', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.getFilterInfo(payload, req.query.id_archi_critdter)
        })
        .then(function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});



// ----------- get filter info  ------------------------------------------------------------------------------------  DEF
app.put('/filter/save', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.saveArchiFilter(payload, req.body);
        })
        .then(function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});




// ----------- get filter info  ------------------------------------------------------------------------------------  DEF
app.post('/filter/post', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.postArchiFilter(payload, req.body);
        })
        .then(function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});




// ----------- delete archi filter ----------------------------------------------------------------------------------  DEF
app.delete('/filter/delete', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.deleteArchiFilter(payload, req.query);
        })
        .then(function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});




// ----------- Save Filter critere  ----------------------------------------------------------------------------  DEF
app.put('/filter/critere/save', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.saveFilterCritere(payload, req.body)
        })
        .then(function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});


// ----------- Delete filter critere & dter-----------------------------------------------------------------------  DEF
app.delete('/filter/critere/delete', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.deleteFilterCritere(payload, req.query);
        })
        .then(function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});



// ----------- Save Filter dter  ----------------------------------------------------------------------------  DEF
app.put('/filter/dter/save', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.saveFilterDter(payload, req.body)
        })
        .then(function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});


// ----------- Delete dter critere & dter-----------------------------------------------------------------------  DEF
app.delete('/filter/dter/delete', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return bibliothequeCtrl.deleteFilterDter(payload, req.query)
        })
        .then(function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});

