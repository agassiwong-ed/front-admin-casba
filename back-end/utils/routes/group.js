/**
 * Created by Johan Schram - KOSMEO ANALITICS - https://kosmeo-a.com on 01/05/2017.
 */


var utils = require("../utils/express-utils");
var Errors = require("../utils/custom-errors");

var Promise = require('promise');
var express = require('express');
var app = express.Router();
module.exports = app;

var groupCtrl = require('../controllers/group');
var auth = require('../controllers/auth');

//######################################################################################################################
//######################################################################################################################
//######################################################################################################################


// ----------- get all groups  ------------------------------------------------------------------------------------  DEF
app.get('/group/usersGroups', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            if (payload.role != 1  ) {
                throw new Errors.IllegalArgumentError("Insufficient privileges");
            }
            return groupCtrl.getGroupID(payload.group)
        })
        .then (function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});



// ----------- get one group info  ------------------------------------------------------------------------------------  DEF
app.get('/group/infos', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            var id_group=req.query.id_group;
            if (( !id_group  || id_group  == 'undefined' || id_group  === null ) && payload.group != null )  {
                throw new Errors.IllegalArgumentError("Id group is required.");
            }

            return groupCtrl.getGroupID(id_group)
        })
        .then (function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});



// ----------- get group info  ------------------------------------------------------------------------------------  DEF
app.get('/group/id', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            var id_group=req.query.id_group;
            if ( !id_group  || id_group  == 'undefined' || id_group  === null || id_group == 'null' ) {
                id_group=payload.group;
            }
            if ((!id_group  || id_group  == 'undefined' || id_group  === null ) && payload.group != null) {
                throw new Errors.IllegalArgumentError("Id group is required.");
            }
            return groupCtrl.getGroupID(id_group)
        })
        .then (function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});




// ----------- delete group ------------------------------------------------------------------------------------  DEF
// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.get('/group/delete', function(req, res, next) {
    var pld = null;
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            pld=payload;
            return groupCtrl.deleteGroup(payload, req.query)
        })
        .then(function () {
            return groupCtrl.getGroupID();
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});


// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.put('/group/update',  function(req, res, next) {
    var pld;
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            pld=payload;
            if (req.body.territoires || req.body.nom) {
                return groupCtrl.updateGroup(payload, req.body)
            }
            else if(req.body.indicators){
                return groupCtrl.updateGroupIndicators(payload, req.body)
            }

        }).then(function(res) {
            return groupCtrl.getGroupID(req.body.id,  function (result) {res.send(result)});
    })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});


// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.put('/group/new',  function(req, res, next) {
    var pld=null;
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            pld=payload;
            return groupCtrl.newGroup(pld, req.body)
        })
        .then(function(res) {
            return groupCtrl.getGroupID(pld.group,  function (result) {res.send(result)});
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});






