"use strict";

var utils = require("../utils/express-utils");
var express = require('express');
var router = express.Router();

module.exports = router;

// Load all routes
// router.use("/geolib", require('./auth'));
router.use("/siterre", require('./bibliotheque'));
router.use("/siterre", require('./indicators_static_ter'));
router.use("/siterre", require('./indicators_dynamic_ter'));
router.use("/siterre", require('./indicators_with_geom'));
router.use("/siterre", require('./auth'));
router.use("/siterre", require('./user'));
router.use("/siterre", require('./group'));
router.use("/siterre", require('./r_scenario'));
