"use strict";

var utils = require("../utils/express-utils");
var Errors = require("../utils/custom-errors");

var Promise = require('promise');
var express = require('express');
var app = express.Router();
module.exports = app;

var indicDynCtrl = require('../controllers/indicators_dynamic_ter');
var auth = require('../controllers/auth');

//######################################################################################################################
//######################################################################################################################
//######################################################################################################################

// ----------- get catalogue criteres + deter -------------------------------------------------------------------------- DEF
app.get('/di/critdter/:id_indic', function(req, res, next) {

    auth.checkToken(req.headers['x-auth-token'])
        .then(function() {
            return indicDynCtrl.getCatalogueCriteresDeterminants(req.params.id_indic)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- get value of  dter (indicators + structural (WTK) ------------------------------------------------------- DEF
app.post('/di/dtervalue?', function(req, res, next) {

    // console.log(req.query)

    auth.checkPayloadGranAndScale(req.headers['x-auth-token'], req.query, false, true, 'granularity_int', 'scale_int',  'scale_filter' )
        .then(function(payload) {
            return indicDynCtrl.getDterValue(req.query, req.body, payload)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ----------- get indicator values for map   -------------------------------------------------------------------------- DEF
app.post('/di/indicValueMap', function(req, res, next) {

    auth.checkPayloadGranAndScale(req.headers['x-auth-token'], req.query, true, true, 'granularity_int', 'scale_int',  'scale_filter' )
        .then(function(payload) {
            return indicDynCtrl.getValueIndicForMap(req.query, req.body, payload)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- get value of  indicators (WTK) -------------------------------------------------------------------------- DEF
app.post('/di/indicValueSum', function(req, res, next) {

    auth.checkPayloadGranAndScale(req.headers['x-auth-token'], req.query, false, true, 'granularity_int', 'scale_int',  'scale_filter' )
        .then(function(payload) {
            return indicDynCtrl.getValueIndicSum(req.query, req.body,payload);
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});



