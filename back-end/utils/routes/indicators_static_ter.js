"use strict";

var utils = require("../utils/express-utils");
var Errors = require("../utils/custom-errors");

var Promise = require('promise');
var express = require('express');
var app = express.Router();
module.exports = app;

var indicCtrl = require('../controllers/indicators_static_ter');
var auth = require('../controllers/auth');

//######################################################################################################################
//######################################################################################################################
//######################################################################################################################


// ----------- get catalogue Indicateurs (WTK) ------------------------------------------------------------------------- DEF
app.get('/si/value', function(req, res, next) {

    auth.checkPayloadGranAndScale(req.headers['x-auth-token'], req.query, true, true, 'granularity_int', 'scale_int',  'scale_filter' )
        .then(function(payload) {
            return indicCtrl.getStaticIndicatorValue(payload, req.query)
        })
        .then(function (results) {
            // Handle resuls lists
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- get catalogue Indicateurs (WTK) ------------------------------------------------------------------------- DEF
app.get('/si/singlevalue', function(req, res, next) {

    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return indicCtrl.getStaticIndicatorSingleValue(req.query, payload)
        })
        .then(function (results) {
            // Handle resuls lists
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- get catalogue Indicateurs (WTK) ------------------------------------------------------------------------- DEF
app.get('/si/moyenne', function(req, res, next) {

    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return indicCtrl.getStaticIndicatorMoyenne(req.query, payload)
        })
        .then(function (results) {
            // Handle resuls lists
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});


//--------------Get data of id_ter for all indicators --------------------
app.get('/si/datasingleter', function(req, res, next ) {

    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return indicCtrl.getDataSingleTer(payload, req.query)
        })
        .then(function (results) {
            // Handle resuls lists
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});







