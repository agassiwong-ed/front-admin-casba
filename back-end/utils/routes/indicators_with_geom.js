"use strict";

var utils = require("../utils/express-utils");
var Errors = require("../utils/custom-errors");

var Promise = require('promise');
var express = require('express');
var app = express.Router();
module.exports = app;

var indicCtrl = require('../controllers/indicators_with_geom');
var auth = require('../controllers/auth');

//######################################################################################################################
//######################################################################################################################
//######################################################################################################################


// ----------- get vectors for self provided vector indicator ----------------------------------------------------------- DEF
app.get('/iv/vectors', function(req, res, next) {

    auth.checkPayloadGranAndScale(req.headers['x-auth-token'], req.query, false, true, 'granularity_int', 'scale_int',  'scale_filter' )
        .then(function() {
            return indicCtrl.getSelfProvidedVectors(req.query, [])
        })
        .then(function (results) {
            // Handle resuls lists
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- get value for self provided vector indicator ------------------------------------------------------------ DEF
app.get('/iv/values', function(req, res, next) {

    auth.checkPayloadGranAndScale(req.headers['x-auth-token'], req.query, false, true, 'granularity_int', 'scale_int',  'scale_filter' )
        .then(function() {
            return indicCtrl.getStaticIndicValues(req.query, [])
        })
        .then(function (results) {
            // Handle resuls lists
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));

});











