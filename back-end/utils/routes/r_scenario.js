

"use strict";

var utils = require("../utils/express-utils");
var Errors = require("../utils/custom-errors");

var Promise = require('promise');
var express = require('express');
var app = express.Router();
module.exports = app;

var scnCtrl = require('../controllers/scenario');
var auth = require('../controllers/auth');

//######################################################################################################################
//######################################################################################################################
//######################################################################################################################

// ----------- get scenario list  --------------------------------------------------------------------------------------  DEF
app.get('/scn/list', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            // console.log(payload)
            return scnCtrl.getListScenarios(payload)
        }).then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ----------- get scenario  -------------------------------------------------------------------------------------------  DEF
app.get('/scn/load', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            // console.log(payload)
            return scnCtrl.getScenario(payload, req.query)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ----------- del scenario  -------------------------------------------------------------------------------------------  DEF
app.get('/scn/del', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            // console.log(payload)
            return scnCtrl.deleteScenario(payload, req.query)
        }).then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});

// ----------- edit scenario  ------------------------------------------------------------------------------------------  DEF
app.post('/scn/edit', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            // console.log(payload)
            return scnCtrl.editScenario(payload, req.body)
        }).then(function (results) {
        return res.json(results);
    })
        .catch(utils.safeHandleError(req, res, next));
});


// ----------- save  scenario as new  ----------------------------------------------------------------------------------  DEF
app.post('/scn/saveas', function(req, res, next) {

    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            // console.log(payload)
            return scnCtrl.saveScenarioAs(payload, req.body)
        }).then(function (results) {
        return res.json(results);
    })
        .catch(utils.safeHandleError(req, res, next));

});

// ----------- save  scenario as existing  -----------------------------------------------------------------------------  DEF
app.post('/scn/save', function(req, res, next) {

    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            // console.log(payload)
            return scnCtrl.saveScenario(payload, req.body)
        }).then(function (results) {
        return res.json(results);
    })
        .catch(utils.safeHandleError(req, res, next));

});


