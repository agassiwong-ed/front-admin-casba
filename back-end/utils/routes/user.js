/**
 * Created by Johan Schram - KOSMEO ANALITICS - https://kosmeo-a.com on 01/05/2017.
 */


var utils = require("../utils/express-utils");
var Errors = require("../utils/custom-errors");

var Promise = require('promise');
var express = require('express');
var app = express.Router();
module.exports = app;

var userCtrl = require('../controllers/user');
var auth = require('../controllers/auth');

//######################################################################################################################
//######################################################################################################################
//######################################################################################################################

// ----------- get User préférences ------------------------------------------------------------------------------------  DEF
app.get('/user/preference', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return userCtrl.getUserPref(payload)
        })
        .then(function (results) {
            return res.json(results);
        })
        .catch(utils.safeHandleError(req, res, next));
});


// ----------- get Users ------------------------------------------------------------------------------------  DEF

app.get('/user/all', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return userCtrl.getUsers(payload.group)
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});


// ----------- get user info  ------------------------------------------------------------------------------------  DEF
app.get('/user/infos', function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
           return userCtrl.getUserInfo(payload.group, req.query.id_user)
        })
        .then(function(rs) {
            return res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});




// ----------- delete User ------------------------------------------------------------------------------------  DEF
// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.get('/user/delete', function(req, res, next) {
    var pld = null;
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            pld=payload;
            return userCtrl.deleteUser(payload, req.query)
        })
        .then(function (res) {
            console.log(res);
            return userCtrl.getUsers(pld.group)
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});


// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.put('/user/update',  function(req, res, next) {
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            return userCtrl.updateUser(payload, req.body)
        }).then(function() {
            return userCtrl.getUserInfo(null, req.body.id);
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});


// ----------- Connection "With Token" ---------------------------------------------------------------------------------
app.put('/user/new',  function(req, res, next) {
    var pld=null;
    auth.checkToken(req.headers['x-auth-token'])
        .then(function(payload) {
            pld=payload;
            return userCtrl.newUser(pld, req.body)
        })
        .then(function() {
             return userCtrl.getUsers(pld.id);
        })
        .then(function(rs){
            res.send(rs);
        })
        .catch(utils.safeHandleError(req, res, next));
});
