import React 		from 'react';
import { Link } 	from 'react-router-dom';

import casbaLight 	from '../../assets/img/logo-light.png';

import '../../assets/vendor/bootstrap/css/bootstrap.min.scss';
import '../../assets/vendor/linearicons/style.scss';
import '../../assets/vendor/chartist/css/chartist-custom.scss';
import '../../assets/css/main.scss';
import '../../assets/css/demo.scss';
import '../../assets/css/custom.scss';

var statutSidebar = true;

function displaySidebar() {
    if (statutSidebar === true)
    {
        document.getElementById("sidebar-nav").style.left = "-14%";
        document.getElementById("principal").style.width = "100%";
        statutSidebar = false;
    }
    else
    {
        document.getElementById("sidebar-nav").style.left = "0%";
        document.getElementById("principal").style.width = "87%";
        statutSidebar = true;
    }
}

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
	render() {
		return (
			<nav className="navbar navbar-default navbar-fixed-top">
				<Link to="/home">
					<div className="brand" style={{backgroundColor: '#475a9a !important'}}>
						<a href=""><img src={casbaLight} alt="Logo Casba Light" className="img-responsive logo" /></a>
					</div>
				</Link>
				<div className="container-fluid" style={{backgroundColor: '#475a9a !important'}}>
					<div className="navbar-btn"></div>
					<div id="navbar-menu">
						<ul className="nav navbar-nav navbar-left">
							<li><div onClick={displaySidebar}><i className="lnr lnr-menu white" /><span className="white">Menu</span></div></li>
						</ul>
						<ul className="nav navbar-nav navbar-right">
							<li><div><i className="lnr lnr-user white" /><span className="white">Invité</span></div></li>
							{/* <li><a href=""><i className="lnr lnr-user white" /><span className="white">Prénom Nom - Chef de territoire</span></a></li> */}
						</ul>
					</div>
				</div>
			</nav>
		);
	}
}

export default Header;
