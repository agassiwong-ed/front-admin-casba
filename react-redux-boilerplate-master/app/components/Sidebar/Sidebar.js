import React from 'react';
import { Link } from 'react-router-dom';

import '../../assets/vendor/bootstrap/css/bootstrap.min.scss';
import '../../assets/vendor/linearicons/style.scss';
import '../../assets/vendor/chartist/css/chartist-custom.scss';
import '../../assets/css/main.scss';
import '../../assets/css/demo.scss';
import '../../assets/css/custom.scss';



class Sidebar extends React.Component { // eslint-disable-line react/prefer-stateless-function eslint-disable
	render() {
		return (
            <div id="wrapper">
                <div id="sidebar-nav" className="sidebar" style={{}}>
                    <div className="sidebar-scroll">
                        <nav>
                            <ul className="nav">
                                <li><Link to="/home"><i className="lnr lnr-home white" /> <span>Accueil</span></Link></li>
                                <li><Link to="/structure"><i className="lnr lnr-apartment white" /> <span>Gérer mes structures</span></Link></li>
                                <li><Link to="/user"><i className="lnr lnr-user white" /> <span>Gérer mes utilisateurs</span></Link></li>
                                <li><Link to="/notebook"><i className="lnr lnr-book white" /> <span>Gérer mes carnets</span></Link></li>
                                <li><Link to="/account"><i className="lnr lnr-briefcase white" /> <span>Mon compte</span></Link></li>
                                <li><Link to="/setting"><i className="lnr lnr-cog white" /> <span>Mes paramètres</span></Link></li>
                                <li><Link to="/disconnect"><i className="lnr lnr-power-switch white" /> <span>Me déconnecter</span></Link></li>

                                {/* <li><a href="/" className="active"><i className="lnr lnr-lock white" /> <span className="white">Se connecter</span></Link></li> */}
                                {/* <li><a href="javascript:close_window();"><i className="lnr lnr-exit white" /> <span>Quitter</span></Link></li> */}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        );
	}
}

export default Sidebar;
