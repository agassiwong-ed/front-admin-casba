// REACT DEPENDENCIES
import React 				from 'react';
import { Helmet } 			from 'react-helmet';
import { Switch, Route }	from 'react-router-dom';

// CONTAINERS
import LoginPage 			from 'containers/LoginPage/LoginPage'
import StructurePage 		from 'containers/StructurePage/Loadable'
import NotebookPage 		from 'containers/NotebookPage/Loadable'
import UserPage 			from 'containers/UserPage/Loadable'
import HomePage 			from 'containers/HomePage/Loadable';
import FeaturePage 			from 'containers/FeaturePage/Loadable';
import NotFoundPage 		from 'containers/NotFoundPage/Loadable';

// COMPONENTS
import Header 				from 'components/Header';
import Sidebar 				from 'components/Sidebar';

// STYLES
import '../../assets/vendor/bootstrap/css/bootstrap.min.scss';
import '../../assets/vendor/linearicons/style.scss';
import '../../assets/vendor/chartist/css/chartist-custom.scss';
import '../../assets/css/main.scss';
import '../../assets/css/demo.scss';
import '../../assets/css/custom.scss';

const App = () => (
	
	<div>
		<Header/>
		<Sidebar/>
		<Switch>
			<Route exact path="/" 		component={LoginPage} />
			<Route path="/structure" 	component={StructurePage} />
			<Route path="/notebook" 	component={NotebookPage} />
			<Route path="/user" 		component={UserPage} />
			<Route path="/home" 		component={HomePage} />
			<Route path="/features" 	component={FeaturePage} />
			<Route path="" 				component={NotFoundPage} />
		</Switch>
	</div>
);

export default App;
