/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import ReposList from 'components/ReposList';

// import './style.scss';
import '../../assets/vendor/bootstrap/css/bootstrap.min.scss';
import '../../assets/vendor/linearicons/style.scss';
import '../../assets/vendor/chartist/css/chartist-custom.scss';
import '../../assets/css/main.scss';
import '../../assets/css/demo.scss';
import '../../assets/css/custom.scss';

export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function


  componentDidMount() {
    
  }

  render() {
    const {
      loading, error, repos
    } = this.props;
    const reposListProps = {
      loading,
      error,
      repos
    };
    return (
		<div id="wrapper">
			<div id="principal" className="main">
				<div className="main-content">
					<div className="container-fluid">
						<div>
							<div className="vertical-align-wrap">
								PAGE D'ACCUEIL
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    );
  }
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool])
};
