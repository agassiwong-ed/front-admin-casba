import React from 'react';
import PropTypes from 'prop-types';

import casbaDark from '../../assets/img/Logo_Casba_gris.png';

import '../../assets/vendor/bootstrap/css/bootstrap.min.scss';
import '../../assets/vendor/linearicons/style.scss';
import '../../assets/vendor/chartist/css/chartist-custom.scss';
import '../../assets/css/main.scss';
import '../../assets/css/demo.scss';
import '../../assets/css/custom.scss';

import { setUserInfos } from './actions'

// import { saga }  from  './saga'

import { UserConsumer, UserProvider } from '../App/UserContext';

export default class LoginPage extends React.Component {
  // eslint-disable-line react/prefer-stateless-function

  // Since state and props are static,
  // there's no need to re-render this component
  	// shouldComponentUpdate() {
    // 	return false;
	// }
    
	constructor(props) {
        super(props)
        this.state = {
            login: 'demo',
            password: 'demo4ed;'
        };
    }

	changeHandlerForLogin = event => {
        this.setState({
            login: event.target.value
        });
    }

    changeHandlerForPassword = event => {
        this.setState({
            password: event.target.value
        });
    }

    handleSubmit = () => {
        this.props.signin({ login:this.state.login, password:this.state.password});

        //this.SignIn();
    }
      
    SignIn = ()  => {
        fetch("http://localhost:3003/auth/signin", {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
            }),
            body: "login=" + this.state.login + "&password=" + this.state.password // <-- Post parameters
        })
            .then((response) =>
                response.text())
            .then((responseText) => {
                this.ResponseParserAuthSignIn(responseText);
            })
            .catch((error) => {
                console.error(error);
            });
    }
    
    GetUser = ()  => {
        fetch("http://localhost:3003/user/informations?id=" + this.state.id, {
            method: 'GET',
            headers: new Headers({
                'x-auth-token': this.state.token, // <-- Specifying the Content-Type
            })
        })
            .then((response) =>
                response.text())
            .then((responseText) => {
                responseText = responseText.substring(1);
                responseText = responseText.slice(0, -1);
                this.setState({
                    response: responseText
                })
                this.ResponseParserUserInformations(this.state.response);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    ResponseParserAuthSignIn = (responseText) => {
        var recievedResponse = JSON.parse(responseText);
        if (recievedResponse.code)
        {
            console.log("CONNECTION FAILURE - START");
            console.log("MESSAGE = "        +   recievedResponse.message);
            console.log("STACK = "          +   recievedResponse.stack);
            console.log("NAME = "           +   recievedResponse.name);
            console.log("CODE = "           +   recievedResponse.code);
            console.log("CONNECTION FAILURE - END");
        }
        else
        {
            console.log("CONNECTION SUCCESS - START");
            console.log("ID = "             +   recievedResponse.user.id);
            console.log("TOKKEN = "         +   recievedResponse.token);
            console.log("CONNECTION SUCCESS - END");
            setUserInfos(recievedResponse)
            // this.setState({
            //     id:             recievedResponse.user.id,
            //     token:          recievedResponse.token,
            //     logged:         true,
            // })
            this.GetUser();
        }
    }

    ResponseParserUserInformations = (responseText) => {
        var recievedResponse = JSON.parse(responseText);
        if (recievedResponse.code)
        {
            console.log("USER INFORMATIONS FAILURE - START");
            console.log("MESSAGE = "        +   recievedResponse.message);
            console.log("STACK = "          +   recievedResponse.stack);
            console.log("NAME = "           +   recievedResponse.name);
            console.log("CODE = "           +   recievedResponse.code);
            console.log("USER INFORMATIONS FAILURE - END");
        }
        else
        {
            console.log("USER INFORMATIONS SUCCESS - START");
            console.log("ID = "             +   recievedResponse.id);
            console.log("PRENOM = "         +   recievedResponse.prenom);
            console.log("NOM = "            +   recievedResponse.nom);
            console.log("TYPE COMPTE = "    +   recievedResponse.type_compte);
            console.log("USER INFORMATIONS SUCCESS - END");
            this.setState({
                id:             recievedResponse.id,
                prenom:         recievedResponse.prenom,
                nom:            recievedResponse.nom,
                type_compte:    recievedResponse.type_compte,
                logged:         true,
                page:           "accueil",
            })
            window.location.href="/home";
        }
    }

	render() {
        const {signin} = this.props;
		return (
            <div id="wrapper">
                <div id="principal" className="main">
                    <div className="main-content">
                        <div className="container-fluid">
                            <div>
                                <div className="vertical-align-wrap">
                                    <div className="vertical-align-middle">
                                        <div className="auth-box ">
                                            <div className="left">
                                                <div className="content">
                                                    <div className="header">
                                                        <div className="logo text-center"><img src={casbaDark} alt="Logo Casba Dark" style={{width: '25%'}} /></div>
                                                        <p className="lead">PAGE DE CONNEXION</p>
                                                    </div>
                                                    <form className="form-auth-small" onSubmit={this.handleSubmit}>
                                                        <div className="form-group">
                                                            <input type="text" value={this.state.login} onChange={this.changeHandlerForLogin} className="form-control" id="signin-email" placeholder="Nom de compte" />
                                                        </div>
                                                        <div className="form-group">
                                                            <input type="password" value={this.state.password} onChange={this.changeHandlerForPassword} className="form-control" id="signin-password" placeholder="Mot de passe" />
                                                        </div>
                                                        <div className="form-group clearfix">
                                                            <label className="fancy-checkbox element-left">
                                                            <input type="checkbox" />
                                                            </label>
                                                        </div>
                                                        <div className="btn btn-primary btn-lg btn-block" onClick={signin}>SE CONNECTER</div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		);
	}
}

LoginPage.propTypes = {
    signin: PropTypes.func,
};