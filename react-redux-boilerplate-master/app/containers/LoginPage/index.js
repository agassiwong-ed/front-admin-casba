

import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,

} from 'containers/App/selectors';
import { loadRepos } from '../App/actions';
 import {  setUserInfos} from './actions';
 import {  makeSelectUserInfos,selectUserInfos} from './selectors';
import reducer from './reducer';
import saga from './saga';


import LoginPage from './LoginPage';

// const mapDispatchToProps = (dispatch) => ({
//   onChangeUserInfos: (evt) => dispatch(setUserInfos(evt.target.value)),
//   setUserInfos: (evt) => {
//     if (evt !== undefined && evt.preventDefault) evt.preventDefault();
//     dispatch(selectUserInfos());
//   }
// });

const mapDispatchToProps = (dispatch) => ({
 // return {
    // dispatching plain actions
    signin: () => {dispatch({type:LOGIN_REQUEST,  payload:{login:this.state.login, password: this.state.password} }) }
  //}
  });

const mapStateToProps = createStructuredSelector({
  //isConnected: makeIsConnected(),
  // repos: makeSelectRepos(),
  userInfos: makeSelectUserInfos(),
  loading: makeSelectLoading(),
  error: makeSelectError()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'login', reducer });
const withSaga = injectSaga({ key: 'login', saga });

export default compose(withReducer, withSaga, withConnect)(LoginPage); // export { default } from './LoginPage';
export { mapDispatchToProps };


