import { SET_USERINFOS, LOGIN_REQUEST } from './constants';

// The initial state of the App
const initialState = {
  userInfos :{
    login: 'demo',
    password: 'demo4ed;',
    id: '',
    logged: false,
    nom: '',
    prenom: '',
    response: '',
    token: '',
    type_compte: ''
  }
};

function LoginReducer(state = initialState, action) {
  switch (action.type) {
    case SET_USERINFOS:
      // Delete prefixed '@' from the github username
      return { ...state, userInfos: action };
    case LOGIN_REQUEST:
      return {...state, userInfos: action };
    default:
      return state;
  }
}

export default LoginReducer;
