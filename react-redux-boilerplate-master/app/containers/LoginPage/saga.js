import {
  call, put, select, takeLatest
} from 'redux-saga/effects';
import { LOGIN_REQUEST } from './constants';
import { setUserInfos } from './actions';

import request from 'utils/request';

export function* authSignin(login, password) {
  //TODO
  const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;

  try {
    // Call our request helper (see 'utils/request')
    const repos = yield call(request, requestURL);
    yield put(reposLoaded(repos, username));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}

export function* SignIn(payload)  {

  const requestURL="http://localhost:3003/auth/signin"

  try {
    // Call our request helper (see 'utils/request')
    const body= "login=" + payload.login + "&password=" + payload.password ; // <-- Post parameters
    const user = yield call(request, requestURL, {method: 'POST', body });
    localStorage.setItem('x-auth-token', user.token )
    yield put(setUserInfos(user));
  } catch (err) {
   // yield put(repoLoadingError(err));
  }

 
}


export function* getUser (id_user)   {
  const requestURL="http://localhost:3003/user/informations?id=" + id_user
  try {
    const user = yield call(request, requestURL, {method: 'GET' });
    localStorage.setItem('x-auth-token', user.token )
    yield put(setUserInfos(user));
  } catch (err) {
   // yield put(repoLoadingError(err));
  }

 
}


/**
 * Github repos request/response handler
 */
export function* getRepos() {
  reposLoaded(null,null);
  
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* loginSaga() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOGIN_REQUEST, SignIn);
}
