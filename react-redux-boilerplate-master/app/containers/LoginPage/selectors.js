import { createSelector } from 'reselect';

const selectUserInfos = (state) => state.userInfos;

const makeSelectUserInfos = () => createSelector(
  selectUserInfos,
  (loginState) => loginState.userInfos
);


export {
  selectUserInfos,
  makeSelectUserInfos
};
