import React from 'react';
import { shallow } from 'enzyme';

import LoginPage from '../index';

describe('<LoginPage />', () => {
  it('should render its heading', () => {
    const renderedComponent = shallow(<LoginPage />);
    expect(renderedComponent.contains(<h1>Logins</h1>)).toBe(true);
  });

  it('should never re-render the component', () => {
    const renderedComponent = shallow(<LoginPage />);
    const inst = renderedComponent.instance();
    expect(inst.shouldComponentUpdate()).toBe(false);
  });
});
