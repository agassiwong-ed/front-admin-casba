/*
 * NotebookPage
 *
 * Display Notebook Page
 */
import React from 'react';
import { Helmet } from 'react-helmet';
// import './style.scss';

import casbaDark from '../../assets/img/Logo_Casba_gris.png';

import '../../assets/vendor/bootstrap/css/bootstrap.min.scss';
import '../../assets/vendor/linearicons/style.scss';
import '../../assets/vendor/chartist/css/chartist-custom.scss';
import '../../assets/css/main.scss';
import '../../assets/css/demo.scss';
import '../../assets/css/custom.scss';

export default class NotebookPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
	render() {
		return (
			<div id="wrapper">
				<div id="principal" className="main">
					<div className="main-content">
						<div className="container-fluid">
							<h3 className="page-title bold">Gérer mes carnets</h3>
							<div className="row">
								<div className="col-md-12">
									<div className="panel">
										<div className="panel-body">
											<table className="table table-striped">
												<thead>
													<tr>
														<th className="bold text-center" style={{ width: "5%" }} >
															#
														</th>
														<th className="bold text-center" style={{ width: "10%" }} >
															Nom du carnet
														</th>
														<th className="bold text-center" style={{ width: "20%" }} >
															Adresse
														</th>
														<th className="bold text-center" style={{ width: "10%" }} >
															Date de création
														</th>
														<th className="bold text-center" style={{ width: "10%" }} >
															Nombre de comptes rattachés
														</th>
														<th className="bold text-center" style={{ width: "45%" }} >
															<table style={{ width: "100%" }} border={0}>
																<tbody>
																	<tr>
																		<td>Actions</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center">
															<a href="ficheCarnetVisualiser.html">
																Carnet Magique
															</a>
														</th>
														<th className="vert-align text-center">
															8 Rue Martel, 75010 Paris
														</th>
														<th className="vert-align text-center">
															30/02/2009
														</th>
														<th className="vert-align text-center">42</th>
														<th className="vert-align text-center">
															<table style={{ width: "100%" }} className="text-center" border={0} >
																<tbody>
																	<tr>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-partager" style={{ width: "95%" }} >
																				<i className="lnr lnr-link white" /> &nbsp;Partager
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-transferer" style={{ width: "95%" }} >
																				<i className="lnr lnr-exit white" /> &nbsp;Transférer
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-supprimer" style={{ width: "95%" }} >
																				<i className="lnr lnr-trash white" /> &nbsp;Supprimer
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-modifier" style={{ width: "95%" }} >
																				<i className="lnr lnr-pencil white" /> &nbsp;Modifier
																			</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
													<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center">
															<a href="ficheCarnetVisualiser.html">
																Carnet Magique
															</a>
														</th>
														<th className="vert-align text-center">
															8 Rue Martel, 75010 Paris
														</th>
														<th className="vert-align text-center">
															30/02/2009
														</th>
														<th className="vert-align text-center">42</th>
														<th className="vert-align text-center">
															<table style={{ width: "100%" }} className="text-center" border={0} >
																<tbody>
																	<tr>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-partager" style={{ width: "95%" }} >
																				<i className="lnr lnr-link white" /> &nbsp;Partager
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-transferer" style={{ width: "95%" }} >
																				<i className="lnr lnr-exit white" /> &nbsp;Transférer
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-supprimer" style={{ width: "95%" }} >
																				<i className="lnr lnr-trash white" /> &nbsp;Supprimer
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-modifier" style={{ width: "95%" }} >
																				<i className="lnr lnr-pencil white" /> &nbsp;Modifier
																			</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
													<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center">
															<a href="ficheCarnetVisualiser.html">
																Carnet Magique
															</a>
														</th>
														<th className="vert-align text-center">
															8 Rue Martel, 75010 Paris
														</th>
														<th className="vert-align text-center">
															30/02/2009
														</th>
														<th className="vert-align text-center">42</th>
														<th className="vert-align text-center">
															<table style={{ width: "100%" }} className="text-center" border={0} >
																<tbody>
																	<tr>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-partager" style={{ width: "95%" }} >
																				<i className="lnr lnr-link white" /> &nbsp;Partager
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-transferer" style={{ width: "95%" }} >
																				<i className="lnr lnr-exit white" /> &nbsp;Transférer
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-supprimer" style={{ width: "95%" }} >
																				<i className="lnr lnr-trash white" /> &nbsp;Supprimer
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-modifier" style={{ width: "95%" }} >
																				<i className="lnr lnr-pencil white" /> &nbsp;Modifier
																			</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
													<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center">
															<a href="ficheCarnetVisualiser.html">
																Carnet Magique
															</a>
														</th>
														<th className="vert-align text-center">
															8 Rue Martel, 75010 Paris
														</th>
														<th className="vert-align text-center">
															30/02/2009
														</th>
														<th className="vert-align text-center">42</th>
														<th className="vert-align text-center">
															<table style={{ width: "100%" }} className="text-center" border={0} >
																<tbody>
																	<tr>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-partager" style={{ width: "95%" }} >
																				<i className="lnr lnr-link white" /> &nbsp;Partager
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-transferer" style={{ width: "95%" }} >
																				<i className="lnr lnr-exit white" /> &nbsp;Transférer
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-supprimer" style={{ width: "95%" }} >
																				<i className="lnr lnr-trash white" /> &nbsp;Supprimer
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-modifier" style={{ width: "95%" }} >
																				<i className="lnr lnr-pencil white" /> &nbsp;Modifier
																			</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
													<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center">
															<a href="ficheCarnetVisualiser.html">
																Carnet Magique
															</a>
														</th>
														<th className="vert-align text-center">
															8 Rue Martel, 75010 Paris
														</th>
														<th className="vert-align text-center">
															30/02/2009
														</th>
														<th className="vert-align text-center">42</th>
														<th className="vert-align text-center">
															<table style={{ width: "100%" }} className="text-center" border={0} >
																<tbody>
																	<tr>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-partager" style={{ width: "95%" }} >
																				<i className="lnr lnr-link white" /> &nbsp;Partager
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-transferer" style={{ width: "95%" }} >
																				<i className="lnr lnr-exit white" /> &nbsp;Transférer
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-supprimer" style={{ width: "95%" }} >
																				<i className="lnr lnr-trash white" /> &nbsp;Supprimer
																			</button>
																		</td>
																		<td style={{ width: "25%" }}>
																			<button type="button" className="btn bouton-medium bouton-modifier" style={{ width: "95%" }} >
																				<i className="lnr lnr-pencil white" /> &nbsp;Modifier
																			</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
