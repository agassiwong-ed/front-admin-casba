import React from 'react';
import { shallow } from 'enzyme';

import NotebookPage from '../index';

describe('<NotebookPage />', () => {
  it('should render its heading', () => {
    const renderedComponent = shallow(<NotebookPage />);
    expect(renderedComponent.contains(<h1>Notebooks</h1>)).toBe(true);
  });

  it('should never re-render the component', () => {
    const renderedComponent = shallow(<NotebookPage />);
    const inst = renderedComponent.instance();
    expect(inst.shouldComponentUpdate()).toBe(false);
  });
});
