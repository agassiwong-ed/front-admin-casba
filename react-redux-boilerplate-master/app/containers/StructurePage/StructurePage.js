/*
 * StructurePage
 *
 * Display structure Page
 */
import React from 'react';
import { Helmet } from 'react-helmet';
// import './style.scss';

import casbaDark from '../../assets/img/Logo_Casba_gris.png';

import '../../assets/vendor/bootstrap/css/bootstrap.min.scss';
import '../../assets/vendor/linearicons/style.scss';
import '../../assets/vendor/chartist/css/chartist-custom.scss';
import '../../assets/css/main.scss';
import '../../assets/css/demo.scss';
import '../../assets/css/custom.scss';

export default class StructurePage extends React.Component { // eslint-disable-line react/prefer-stateless-function
	render() {
		return (
			<div id="wrapper">
				<div id="principal" className="main">
					<div className="main-content">
						<div className="container-fluid">
							<h3 className="page-title bold">Gérer mes structures</h3>
							<div className="row">
								<div className="col-md-12">
									<div className="panel">
										<div className="panel-heading">
											<table style={{width: '95%'}} border={0}>
											<tbody>
												<tr>
													<td style={{width: '100%'}} className="text-right">
													<button onClick="location.href='/wordpress/ficheStructureAjouter.html'" className="fiche-btn-custom-medium fiche-structure-ajouter" type="button"><i className="lnr lnr-plus-circle white" /> &nbsp;Ajouter une nouvelle structure</button>
													</td>
												</tr>
											</tbody>
											</table>
										</div>
										<div className="panel-body">
											<table className="table table-striped">
												<thead>
													<tr>
														<th className="bold text-center" style={{width: '5%'}}>#</th>
														<th className="bold text-center" style={{width: '20%'}}>Nom de la structure</th>
														<th className="bold text-center" style={{width: '10%'}}>Nombre d'utilisateurs</th>
														<th className="bold text-center" style={{width: '15%'}}>Numéro SIRET</th>
														<th className="bold text-center" style={{width: '15%'}}>Date de création</th>
														<th className="bold text-center" style={{width: '35%'}}>
															<table style={{width: '100%'}} border={0}>
																<tbody>
																	<tr>
																		<td>Actions</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center"><a href="ficheStructureVisualiser.html">Nom_de_structure_exemple</a></th>
														<th className="vert-align text-center">999</th>
														<th className="vert-align text-center">362521879 00034</th>
														<th className="vert-align text-center">30/02/2009</th>
														<th className="vert-align text-center">
															<table style={{width: '100%'}} className="text-center" border={0}>
																<tbody>
																	<tr>
																		<td style={{width: '49%'}}>
																			<button onclick="location.href='/wordpress/ficheStructureModifier.html'" type="button" className="btn bouton-medium bouton-modifier" style={{width: '100%'}}><i className="lnr lnr-pencil white" /> &nbsp;Modifier</button>
																		</td>
																		<td style={{width: '2%'}}></td>
																		<td style={{width: '49%'}}>
																			<button type="button" className="btn bouton-medium bouton-supprimer" style={{width: '100%'}}><i className="lnr lnr-trash white" /> &nbsp;Supprimer</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
													
												</tbody>
											</table>
										</div>
										<div className="panel-heading">
											<table style={{width: '95%'}} border={0}>
											<tbody>
												<tr>
													<td style={{width: '100%'}} className="text-right">
													<button onclick="location.href='/wordpress/ficheStructureAjouter.html'" className="fiche-btn-custom-medium fiche-structure-ajouter" type="button"><i className="lnr lnr-plus-circle white" /> &nbsp;Ajouter une nouvelle structure</button>
													</td>
												</tr>
											</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
