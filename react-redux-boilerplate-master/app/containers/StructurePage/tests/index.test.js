import React from 'react';
import { shallow } from 'enzyme';

import StructurePage from '../index';

describe('<StructurePage />', () => {
  it('should render its heading', () => {
    const renderedComponent = shallow(<StructurePage />);
    expect(renderedComponent.contains(<h1>structures</h1>)).toBe(true);
  });

  it('should never re-render the component', () => {
    const renderedComponent = shallow(<StructurePage />);
    const inst = renderedComponent.instance();
    expect(inst.shouldComponentUpdate()).toBe(false);
  });
});
