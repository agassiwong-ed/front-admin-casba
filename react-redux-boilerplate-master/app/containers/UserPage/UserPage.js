/*
 * UserPage
 *
 * Display User Page
 */
import React from 'react';
import { Helmet } from 'react-helmet';
// import './style.scss';

import casbaDark from '../../assets/img/Logo_Casba_gris.png';

import '../../assets/vendor/bootstrap/css/bootstrap.min.scss';
import '../../assets/vendor/linearicons/style.scss';
import '../../assets/vendor/chartist/css/chartist-custom.scss';
import '../../assets/css/main.scss';
import '../../assets/css/demo.scss';
import '../../assets/css/custom.scss';

export default class UserPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
	render() {
		return (
			<div id="wrapper">
				<div id="principal" className="main">
					<div className="main-content">
						<div className="container-fluid">
							<h3 className="page-title bold">Gérer mes utilisateurs</h3>
							<div className="row">
								<div className="col-md-12">
									<div className="panel">
										<div className="panel-body">
											<table className="table table-striped">
												<thead>
													<tr>
														<th className="bold text-center" style={{ width: "5%" }} >
															#
														</th>
														<th className="bold text-center" style={{ width: "20%" }} >
															Nom Prénom
														</th>
														<th className="bold text-center" style={{ width: "15%" }} >
															Structure
														</th>
														<th className="bold text-center" style={{ width: "10%" }} >
															Date de création
														</th>
														<th className="bold text-center" style={{ width: "50%" }} >
															<table style={{ width: "100%" }} border={0}>
																<tbody>
																	<tr>
																		<td>Actions</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
												</thead>
												<tbody>
												<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center">
															<a href="ficheUtilisateurVisualiser.html">
																John Doe
															</a>
														</th>
														<th className="vert-align text-center">
															Structure Magique
														</th>
														<th className="vert-align text-center">
															30/02/2009
														</th>
														<th className="vert-align text-center">
															<table style={{ width: "100%" }} className="text-center" border={0} >
																<tbody>
																	<tr>
																		<td style={{ width: "33%" }}>
																			<button type="button" className="btn bouton bouton-activer" >
																				<i className="lnr lnr-sun white" />
																				&nbsp;Activé
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="admin" type="button" className="btn bouton bouton-administrateur" >
																				<i className="lnr lnr-users white" />
																				&nbsp;Adminstrateur
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="chef" type="button" className="btn bouton bouton-chef" >
																				<i className="lnr lnr-star white" />
																				&nbsp;Chef d'Équipe
																			</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
													<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center">
															<a href="ficheUtilisateurVisualiser.html">
																John Doe
															</a>
														</th>
														<th className="vert-align text-center">
															Structure Magique
														</th>
														<th className="vert-align text-center">
															30/02/2009
														</th>
														<th className="vert-align text-center">
															<table style={{ width: "100%" }} className="text-center" border={0} >
																<tbody>
																	<tr>
																		<td style={{ width: "33%" }}>
																			<button type="button" className="btn bouton bouton-activer" >
																				<i className="lnr lnr-sun white" />
																				&nbsp;Activé
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="admin" type="button" className="btn bouton bouton-administrateur" >
																				<i className="lnr lnr-users white" />
																				&nbsp;Adminstrateur
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="chef" type="button" className="btn bouton bouton-chef" >
																				<i className="lnr lnr-star white" />
																				&nbsp;Chef d'Équipe
																			</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
													<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center">
															<a href="ficheUtilisateurVisualiser.html">
																John Doe
															</a>
														</th>
														<th className="vert-align text-center">
															Structure Magique
														</th>
														<th className="vert-align text-center">
															30/02/2009
														</th>
														<th className="vert-align text-center">
															<table style={{ width: "100%" }} className="text-center" border={0} >
																<tbody>
																	<tr>
																		<td style={{ width: "33%" }}>
																			<button type="button" className="btn bouton bouton-activer" >
																				<i className="lnr lnr-sun white" />
																				&nbsp;Activé
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="admin" type="button" className="btn bouton bouton-administrateur" >
																				<i className="lnr lnr-users white" />
																				&nbsp;Adminstrateur
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="chef" type="button" className="btn bouton bouton-chef" >
																				<i className="lnr lnr-star white" />
																				&nbsp;Chef d'Équipe
																			</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
													<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center">
															<a href="ficheUtilisateurVisualiser.html">
																John Doe
															</a>
														</th>
														<th className="vert-align text-center">
															Structure Magique
														</th>
														<th className="vert-align text-center">
															30/02/2009
														</th>
														<th className="vert-align text-center">
															<table style={{ width: "100%" }} className="text-center" border={0} >
																<tbody>
																	<tr>
																		<td style={{ width: "33%" }}>
																			<button type="button" className="btn bouton bouton-activer" >
																				<i className="lnr lnr-sun white" />
																				&nbsp;Activé
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="admin" type="button" className="btn bouton bouton-administrateur" >
																				<i className="lnr lnr-users white" />
																				&nbsp;Adminstrateur
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="chef" type="button" className="btn bouton bouton-chef" >
																				<i className="lnr lnr-star white" />
																				&nbsp;Chef d'Équipe
																			</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
													<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center">
															<a href="ficheUtilisateurVisualiser.html">
																John Doe
															</a>
														</th>
														<th className="vert-align text-center">
															Structure Magique
														</th>
														<th className="vert-align text-center">
															30/02/2009
														</th>
														<th className="vert-align text-center">
															<table style={{ width: "100%" }} className="text-center" border={0} >
																<tbody>
																	<tr>
																		<td style={{ width: "33%" }}>
																			<button type="button" className="btn bouton bouton-activer" >
																				<i className="lnr lnr-sun white" />
																				&nbsp;Activé
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="admin" type="button" className="btn bouton bouton-administrateur" >
																				<i className="lnr lnr-users white" />
																				&nbsp;Adminstrateur
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="chef" type="button" className="btn bouton bouton-chef" >
																				<i className="lnr lnr-star white" />
																				&nbsp;Chef d'Équipe
																			</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
													<tr>
														<th className="vert-align text-center">1</th>
														<th className="vert-align text-center">
															<a href="ficheUtilisateurVisualiser.html">
																John Doe
															</a>
														</th>
														<th className="vert-align text-center">
															Structure Magique
														</th>
														<th className="vert-align text-center">
															30/02/2009
														</th>
														<th className="vert-align text-center">
															<table style={{ width: "100%" }} className="text-center" border={0} >
																<tbody>
																	<tr>
																		<td style={{ width: "33%" }}>
																			<button type="button" className="btn bouton bouton-desactiver" >
																				<i className="lnr lnr-moon white" />
																				&nbsp;Désactivé
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="admin" type="button" className="btn bouton bouton-administrateur" >
																				<i className="lnr lnr-users white" />
																				&nbsp;Adminstrateur
																			</button>
																		</td>
																		<td style={{ width: "33%" }}>
																			<button id="chef" type="button" className="btn bouton bouton-chef" >
																				<i className="lnr lnr-star white" />
																				&nbsp;Chef d'Équipe
																			</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</th>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
