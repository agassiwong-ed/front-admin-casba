import React from 'react';
import { shallow } from 'enzyme';

import UserPage from '../index';

describe('<UserPage />', () => {
  it('should render its heading', () => {
    const renderedComponent = shallow(<UserPage />);
    expect(renderedComponent.contains(<h1>Users</h1>)).toBe(true);
  });

  it('should never re-render the component', () => {
    const renderedComponent = shallow(<UserPage />);
    const inst = renderedComponent.instance();
    expect(inst.shouldComponentUpdate()).toBe(false);
  });
});
